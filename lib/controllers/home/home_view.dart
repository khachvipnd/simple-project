import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/colors.dart';
import 'package:yourpti/controllers/base/base_screen.dart';
import 'package:yourpti/controllers/base/base_state.dart';
import 'package:yourpti/controllers/home/nhap/inputView.dart';
import '../../constants/icons.dart';
import '../base/app_controller.dart';
import 'home_main/home_main_view.dart';

class HomeView extends BaseScreen {
  const HomeView({Key? key}) : super(key: key);

  @override
  State<HomeView> createState() => _HomeViewState();
}

class _HomeViewState extends BaseState<HomeView> with BasicState {
  // final LoginController _loginController = Get.put(LoginController());
  final AppController _appController = Get.put(AppController());
  int _selectedIndex = 0;
  double sizeIcon = 20;

  List<Widget> widgetOptions = [];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  bool get isSafeArea => false;

  @override
  Widget? createBottomBar() {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: SvgPicture.asset(iconHome, height: sizeIcon, width: sizeIcon),
          label: 'home'.tr,
          backgroundColor: Colors.red,
          activeIcon: SvgPicture.asset(iconHomeSelect,
              height: sizeIcon, width: sizeIcon),
        ),
        BottomNavigationBarItem(
          icon:
              SvgPicture.asset(iconSupport, height: sizeIcon, width: sizeIcon),
          label: 'input'.tr,
          backgroundColor: Colors.purple,
          activeIcon:
              SvgPicture.asset(headphone, height: sizeIcon, width: sizeIcon),
        ),
      ],
      currentIndex: _selectedIndex,
      unselectedItemColor: neutral_400,
      selectedItemColor: primary,
      onTap: _onItemTapped,
    );
  }

  @override
  PreferredSizeWidget? createAppBar() {
    return null;
  }

  @override
  void initState() {
    super.initState();

    widgetOptions = <Widget>[
      HomeMainView(
        callback: ((data) {
          _selectedIndex = widgetOptions.length - 1;
          setState(() {});
        }),
      ),
      const InputView(),
    ];
  }

  @override
  Widget createBody() {
    return IndexedStack(
      index: _selectedIndex,
      children: widgetOptions,
    );
  }
}
