import 'package:flutter/material.dart';
import 'package:yourpti/controllers/base/base_controller.dart';

class InformationPersonalController extends BaseController {
  TextEditingController? nameController;
  TextEditingController? bodController;
  TextEditingController? sexController;
  TextEditingController? idController;
  TextEditingController? phoneController;
  TextEditingController? cardNumberController;
  TextEditingController? taxCodeController;
  TextEditingController? emailController;
  TextEditingController? addressController;

  @override
  void onInit() {
    nameController = TextEditingController(text: "");
    bodController = TextEditingController(text: "");
    sexController = TextEditingController(text: "");
    idController = TextEditingController(text: "");
    phoneController = TextEditingController(text: "");
    cardNumberController = TextEditingController(text: "");
    taxCodeController = TextEditingController(text: "");
    emailController = TextEditingController(text: "");
    addressController = TextEditingController(text: "");
    super.onInit();
  }
}
