import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/colors.dart';
import 'package:yourpti/controllers/base/app_controller.dart';
import 'package:yourpti/controllers/base/baseview.dart';
import 'package:yourpti/extensions/date.dart';
import 'package:yourpti/extensions/string_extension.dart';
import '../../../../../widgets/base/button_widget.dart';
import '../../../../../widgets/base/date_picker/date_picker_view.dart';
import '../../../../../widgets/base/dropdown_search/drop_down_search.dart';
import '../../../../../widgets/base/dropdowns/drop_list_model.dart';
import '../../../../../widgets/base/text_form_widget.dart';
import '../../../../../widgets/base/view_shadow.dart';
import 'inputController.dart';

class InputView extends BaseScreen {
  const InputView({Key? key}) : super(key: key);

  @override
  _inputState createState() => _inputState();
}

class _inputState extends BaseState<InputView> with BasicState {
  final InformationPersonalController _informationPersonalController =
      Get.put(InformationPersonalController());
  AppController? appController;
  final _formKey = GlobalKey<FormState>();
  String bodStr = "";
  String dDateTime = "";
  int gioiTinh = 0;
  @override
  void initState() {
    super.initState();
  }

  @override
  PreferredSizeWidget? createAppBar() {
    return headerView(
      title: "infomation",
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget createBody() {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Column(
          children: [
            Expanded(
                child: SingleChildScrollView(
              child: Form(
                  key: _formKey,
                  child: Container(
                    padding:
                        const EdgeInsets.only(left: 16, right: 16, top: 16),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          // const SizedBox(
                          //   height: 10,
                          // ),
                          TextFormWidget(
                            title: "name",
                            controller:
                                _informationPersonalController.nameController,
                            isRequail: true,
                            textValidate: "namenotempty",
                            hintText: "entername",
                          ),
                          // const SizedBox(
                          //   height: 10,
                          // ),
                          DatePickerView(
                            title: "birdthday",
                            textValidate: "birdthdaynotempty",
                            //initDate: "2009-12-08T00:00:00",
                            initDate: _informationPersonalController
                                    .bodController?.text
                                    .converStringToDate(
                                        dateFormat: "dd/MM/yyyy")
                                    ?.formatDateToString(
                                        format: "yyyy-MM-ddTHH:MM:ss") ??
                                "",
                            isRequail: true,
                            callback: (date, dateString) {
                              bodStr = date.formatDateToStringYYYYMMDD();
                              dDateTime = dateString;
                            },
                          ),
                          // const SizedBox(
                          //   height: 10,
                          // ),
                          DropdownSearch(
                              hindText: "selectsex",
                              isSearch: false,
                              heightView: 300,
                              title: "sextitle",
                              titleDrop: "selectsex",
                              isRequail: true,
                              items: [
                                OptionItem(id: "0", title: "male".tr),
                                OptionItem(id: "1", title: "female".tr),
                                OptionItem(id: "2", title: "other".tr)
                              ],
                              callback: (data) {
                                gioiTinh = int.parse(data.id);
                              }),
                          // const SizedBox(
                          //   height: 10,
                          // ),
                          InkWell(
                            onTap: () {
                              showAlertDialogMessage(
                                  content: "cannotChangeCMTCCCDHC".tr);
                            },
                            child: TextFormWidget(
                              title: "cmnd",
                              enabled: (_informationPersonalController
                                              .idController?.text ??
                                          "")
                                      .isEmpty
                                  ? true
                                  : false,
                              controller:
                                  _informationPersonalController.idController,
                              isRequail: true,
                              textValidate: "cmndnotempty",
                              hintText: "entercmnd",
                            ),
                          ),
                          // const SizedBox(
                          //   height: 10,
                          // ),
                          TextFormWidget(
                            controller:
                                _informationPersonalController.phoneController,
                            title: "phone",
                            keyboardType: TextInputType.phone,
                            isRequail: true,
                            textValidate: "phonenotempty",
                            hintText: "enterphone",
                          ),
                          // const SizedBox(
                          //   height: 10,
                          // ),
                          TextFormWidget(
                            title: "taxCode",
                            controller: _informationPersonalController
                                .taxCodeController,
                            keyboardType: TextInputType.text,
                            hintText: "enterTaxCode",
                          ),
                          // const SizedBox(
                          //   height: 10,
                          // ),
                          TextFormWidget(
                            title: "email",
                            isRequail: true,
                            keyboardType: TextInputType.emailAddress,
                            textValidate: "emailnotempty",
                            controller:
                                _informationPersonalController.emailController,
                            hintText: "enteremail",
                          ),
                          // const SizedBox(
                          //   height: 10,
                          // ),
                          TextFormWidget(
                            title: "address",
                            controller: _informationPersonalController
                                .addressController,
                            keyboardType: TextInputType.text,
                            isRequail: true,
                            textValidate: "addressNoEmpty",
                            hintText: "enterAddress",
                          ),
                          const SizedBox(
                            height: 20,
                          ),
                        ]),
                  )),
            )),
            ViewShadow(views: [
              Expanded(
                child: ButtonWidget(
                  name: "saveInfor",
                  width: Get.width,
                  color: secondary,
                  callback: () {
                    if (_formKey.currentState!.validate()) {}
                  },
                ),
              )
            ])
          ],
        ));
  }

  setInitItemSex(String i) {
    switch (i) {
      case "0":
        {
          return OptionItem(id: "0", title: "male".tr);
        }
      case "1":
        {
          return OptionItem(id: "1", title: "female".tr);
        }
      case "2":
        {
          return OptionItem(id: "2", title: "other".tr);
        }
    }
  }
}
