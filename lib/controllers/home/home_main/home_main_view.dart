import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/widgets/pull_to_refresh/pull_to_refresh.dart';

import '../../base/app_controller.dart';
import '../../base/baseview.dart';
import '../../base/header.dart';

class HomeMainView extends BaseScreen {
  const HomeMainView({Key? key, this.callback}) : super(key: key);
  final CallbackData? callback;
  @override
  State<StatefulWidget> createState() => _HomeMainView();
}

class _HomeMainView extends BaseState<HomeMainView> with BasicState {
  AppController? _appController;

  final RefreshController _refreshController = RefreshController();

  @override
  bool get isTopSafeArea => false;
  @override
  void initState() {
    super.initState();
    _appController = Get.find<AppController>();
  }

  @override
  Widget createBody() {
    return Container(
        color: Colors.white,
        child: Column(
          children: [
            Header(
              appController: _appController,
              callback: ((data) {
                if (widget.callback != null) {
                  widget.callback!(data);
                }
              }),
            ),
            const Expanded(
                child: Center(
              child: Text(
                "Hello world",
                textAlign: TextAlign.center,
                style: boldStyle,
              ),
            )),
          ],
        ));
  }
}
