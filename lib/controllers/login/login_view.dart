import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:local_auth/local_auth.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/controllers/base/base_screen.dart';
import 'package:yourpti/controllers/base/base_state.dart';
import 'package:yourpti/controllers/login/login_controller.dart';
import 'package:yourpti/screens.dart';
import 'package:yourpti/widgets/base/base.dart';

import '../../constants/colors.dart';
import '../../constants/icons.dart';
import '../../constants/size.dart';
import '../../widgets/base/dropdowns/drop_list_model.dart';
import '../../widgets/support/support_widget.dart';

class LoginView extends BaseScreen {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends BaseState<LoginView> with BasicState {
  final LoginController _loginController = Get.put(LoginController());
  List<OptionItem> options = [];
  OptionItem? selectImage;
  final _formKey = GlobalKey<FormState>();
  bool userHasTouchId = false;
  final LocalAuthentication auth = LocalAuthentication();
  String authorized = 'Not Authorized';
  bool isAuthenticating = false;
  bool rememberLogin = false;
  @override
  PreferredSizeWidget? createAppBar() {
    return null;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  bool get isSafeArea => false;
  @override
  Color get colorSafeArea => primary;

  @override
  Widget createBody() {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Container(
          color: primary,
          child: Stack(
            children: [
              Positioned(
                  top: 50,
                  left: 0,
                  right: 0,
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: SvgPicture.asset(
                      iconHome,
                      width: 100,
                      // height: 200,
                    ),
                  )),
              Positioned(
                top: 200,
                bottom: 0,
                left: 0,
                right: 0,
                child: viewInfor(),
              )
            ],
          ),
        ));
  }

  Widget viewInfor() {
    return Container(
        padding: paddingLeftRight.copyWith(top: 42),
        decoration: const BoxDecoration(
            color: white,
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(40), topRight: Radius.circular(40))),
        child: Column(
          children: [
            Expanded(
                child: SingleChildScrollView(
              child: bodyView(),
            )),
            Obx(() => Visibility(
                visible: !_loginController.isShowKey.value,
                child: SupportWidget(
                  callback: () {
                    Get.toNamed(loginView, arguments: true);
                  },
                ))),
            const SizedBox(
              height: 20,
            )
          ],
        ));
  }

  Widget bodyView() {
    return Form(
      key: _formKey,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(
            "titlewelcome".tr,
            style: boldStyle.copyWith(fontSize: fontSize20, color: neutral_500),
          ),
          const SizedBox(height: 10),
          Text(
            "welcome".tr,
            textAlign: TextAlign.center,
            style: normalStyle.copyWith(
                height: 1.7, fontWeight: FontWeight.w400, color: neutral_400),
          ),
          const SizedBox(height: 48),
          TextFormBorderWidget(
              icon: username,
              textValidate: "usernamenotempty",
              isRequail: true,
              controller: _loginController.emailTextController,
              iconAction: infor,
              callbackAction: () {
                showAlertDialogMessage(
                    content: "inforlogin".tr, maxHeight: 140);
              },
              hintText: "username"),
          const SizedBox(height: 10),
          TextFormBorderWidget(
              icon: password,
              controller: _loginController.passwordTextController,
              // icon: Icons.key,
              isRequail: true,
              isLogin: true,
              textValidate: "passnotempty",
              obscureText: true,
              hintText: "password"),

          Row(
            children: [
              Expanded(
                flex: 1,
                child: CheckBoxWidget(
                  isCheck: rememberLogin,
                  title: "savePass",
                  callback: (isCheck) {
                    rememberLogin = !rememberLogin;
                  },
                ),
              ),
              Expanded(
                  flex: 1,
                  child: InkWell(
                    child: Text(
                      "forgetpassword".tr,
                      textAlign: TextAlign.right,
                      style: normalStyle.copyWith(
                          color: secondary, fontWeight: FontWeight.w400),
                    ),
                    onTap: () {
                      Get.toNamed(loginView);
                    },
                  ))
            ],
          ),

          const SizedBox(height: 30),
          loginWidget(),
          const SizedBox(height: 20),
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  if (userHasTouchId) {
                  } else {
                    showAlertDialogMessage(
                        content:
                            "Login first, Need to setup FingerPrint or FaceId in Setting of App");
                  }
                },
                child: Image.asset(
                  iconFingerPrint,
                  width: 40,
                  height: 40,
                ),
              ),
            ],
          ),

          const SizedBox(height: 20),

          // languageWidget()
        ],
      ),
    );
  }

  Widget loginWidget() {
    return ButtonWidget(
      name: 'login',
      color: secondary,
      callback: () {
        if (_formKey.currentState!.validate()) {
          Get.toNamed(homeView);
        }
      },
    );
  }
}
