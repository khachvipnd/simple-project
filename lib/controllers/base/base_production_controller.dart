import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:yourpti/controllers/base/baseview.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';
import 'package:yourpti/models/PageObject.dart';
import 'package:yourpti/utils/common.dart';
import 'package:yourpti/widgets/base/button_ocr_form.dart';
import 'package:yourpti/widgets/base/checkbox_form.dart';
import 'package:yourpti/widgets/base/date_picker/date_picker_view_production.dart';
import 'package:yourpti/widgets/base/dropdown_search_production/drop_down_search_production.dart';
import 'package:yourpti/widgets/base/line_form.dart';
import 'package:yourpti/widgets/base/text_field_form.dart';
import 'package:yourpti/widgets/base/text_form.dart';

import '../../constants/callbacks.dart';
import '../../constants/colors.dart';
import '../../constants/strings.dart';
import '../../manager/manager.dart';
import '../../widgets/base/button_upload_image.dart';
import '../../widgets/base/ky_phi_form.dart';
import '../../widgets/base/button_form.dart';
import 'app_controller.dart';

class BaseProductionController extends BaseController {
  List<PageControl> lsPages = [];
  List<Widget> lsRow = [];
  Map<String, dynamic>? formKD;
  int currentPage = 0;
  int? maxPageShow;
  AppController? appController;
  RxBool isOverViewPage = false.obs; // hiển thị page cuối cùng  nhé.
  RxBool isPrepareNextPage = false.obs;
  bool isEditFormOverView = false;

  dynamic upperCaseKeyData(List<dynamic> ls) {
    List<dynamic> lsNew = [];
    for (var element in ls) {
      Map<String, dynamic> data = Map.from(element);
      Map<String, dynamic> dataNews = <String, dynamic>{};
      data.forEach((key, value) {
        dataNews[key.toUpperCase()] = value;
      });
      lsNew.add(dataNews);
    }
    return lsNew;
  }

  dynamic lowerCaseKeyData(List<dynamic> ls) {
    List<dynamic> lsNew = [];
    for (var element in ls) {
      Map<String, dynamic> data = Map.from(element);
      Map<String, dynamic> dataNews = <String, dynamic>{};
      data.forEach((key, value) {
        dataNews[key.toLowerCase()] = value;
      });
      lsNew.add(dataNews);
    }
    // map((key, value) => lsNew[key.toUpperCase()] = value);

    return lsNew;
  }

  void setValueByKeyNew(dynamic chiTietDon,
      {dynamic value = "", List<Widget> lsRows = const []}) {
    // Control control = findControlInputFormAllPage(key);

    if (lsRows.isEmpty) {
      lsRows = lsRow;
    }
    for (var element in lsRows) {
      Row row = element as Row;
      for (var child in row.children) {
        try {
          // Expanded exp = child as Expanded;
          if (child is FormObject) {
            FormObject form = child as FormObject;
            Control control = form.control!;
            if (chiTietDon[control.id.toUpperCase()] != null) {
              if (control.formatShow == FormatValuePresent.Percent) {
                control.value.value =
                    chiTietDon[control.id.toUpperCase()]?.toString() ?? "0";
              } else if (control.formatShow == FormatValuePresent.Money) {
                control.value.value =
                    chiTietDon[control.id.toUpperCase()]?.toInt()?.toString() ??
                        "0";
              } else {
                control.value.value =
                    chiTietDon[control.id.toUpperCase()]?.toString() ?? "";
              }
            }
          }
        } catch (e) {
          printDebug(e);
        }
      }
    }
  }

  void setValueByKeyAllNewByIndexPage(dynamic chiTietDon, int index) {
    for (var row in lsPages[index].row) {
      for (var child in row) {
        try {
          if (child is Control) {
            Control control = child;
            if (chiTietDon[control.id.toUpperCase()] != null) {
              if (control.formatShow == FormatValuePresent.Percent) {
                control.value.value =
                    chiTietDon[control.id.toUpperCase()]?.toString() ?? "0";
              } else if (control.formatShow == FormatValuePresent.Money) {
                control.value.value =
                    chiTietDon[control.id.toUpperCase()]?.toInt()?.toString() ??
                        "0";
              } else {
                control.value.value =
                    chiTietDon[control.id.toUpperCase()]?.toString() ?? "";
              }
            }
          }
        } catch (e) {
          printDebug(e);
        }
      }
    }
  }

  void setValueByKeyAllNew(dynamic chiTietDon,
      {dynamic value = "", List<Widget> lsRows = const []}) {
    // Control control = findControlInputFormAllPage(key);

    // if (lsRows.isEmpty) {
    //   lsRows = lsRow;
    // }

    for (var page in lsPages) {
      for (var row in page.row) {
        for (var child in row) {
          try {
            if (child is Control) {
              Control control = child;
              if (chiTietDon[control.id.toUpperCase()] != null) {
                if (control.formatShow == FormatValuePresent.Percent) {
                  control.value.value =
                      chiTietDon[control.id.toUpperCase()]?.toString() ?? "0";
                } else if (control.formatShow == FormatValuePresent.Money) {
                  control.value.value = chiTietDon[control.id.toUpperCase()]
                          ?.toInt()
                          ?.toString() ??
                      "0";
                } else {
                  control.value.value =
                      chiTietDon[control.id.toUpperCase()]?.toString() ?? "";
                }
              }
            }
          } catch (e) {
            printDebug(e);
          }
        }
      }
    }

    // for (var element in lsRows) {
    //   Row row = element as Row;
    //   for (var child in row.children) {
    //     try {
    //       // Expanded exp = child as Expanded;
    //       if (child is FormObject) {
    //         FormObject form = child as FormObject;
    //         Control control = form.control!;
    //         if (chiTietDon[control.id.toUpperCase()] != null) {
    //           if (control.formatShow == FormatValuePresent.Percent) {
    //             control.value.value =
    //                 chiTietDon[control.id.toUpperCase()]?.toString() ?? "0";
    //           } else if (control.formatShow == FormatValuePresent.Money) {
    //             control.value.value =
    //                 chiTietDon[control.id.toUpperCase()]?.toInt()?.toString() ??
    //                     "0";
    //           } else {
    //             control.value.value =
    //                 chiTietDon[control.id.toUpperCase()]?.toString() ?? "";
    //           }
    //         }
    //       }
    //     } catch (e) {
    //       printDebug(e);
    //     }
    //   }
    // }
  }

  void setValueSelectedForDropDownALLNew() {
    for (var page in lsPages) {
      for (var rows in page.row) {
        // Row row = rows as Row;
        for (var child in rows) {
          try {
            // Expanded exp = child as Expanded;
            if (child.typeInput == TypeInputForm.dropDownSearch) {
              var control = child;
              if (control.listData.isNotEmpty) {
                control.valueSelected = control.listData[0];
                control.value.value =
                    control.listData[0][control.keyGetValueForID];
              }
            }
          } catch (e) {
            // printDebug(e);
          }
        }
      }
    }
  }

  void setDataForDropDownALLNew(formKD, {String id = "", dynamic listData}) {
    for (var page in lsPages) {
      for (var rows in page.row) {
        // Row row = rows as Row;
        for (var child in rows) {
          try {
            // Expanded exp = child as Expanded;
            if (child.typeInput == TypeInputForm.dropDownSearch) {
              if (id != "" && listData != null && child.id == id) {
                child.setListData(listData);
              } else {
                if (formKD != null &&
                    formKD.data != {} &&
                    child.keyGetListDataFormStart.isNotEmpty) {
                  child.setListData(
                      formKD?.data?[child.keyGetListDataFormStart] ??
                          []); //fake
                }
              }
            }
          } catch (e) {
            printDebug(e);
          }
        }
      }
    }
  }

  void onEditingComplete(FormObject obj) {
    printDebug(obj);
  }

  bool validateForm() {
    return false;
  }

  void addInputParamsDefault(List<Control> controls) {
    lsPages[currentPage].row.add(controls);
  }

  void onChanges(FormObject obj) {}

  void jumpleToIndexPage(int index) {
    if (isOverViewPage.value) {
      isOverViewPage.value = false;
    }

    currentPage = index;
  }

  void previousPage() {
    if (isOverViewPage.value) {
      isOverViewPage.value = false;
    } else {
      currentPage--;
    }
  }

  void jumpleOverviewPage() {
    isOverViewPage.value = true;
  }

  void nextPage() {
    if (currentPage < lsPages.length - 1) {
      currentPage++;
    } else {
      isOverViewPage.value = true;
      printDebug('Hết page để next rồi');
    }
  }

  // Control findDefaultControl(String id, RxString value) {
  //   Control? controlFind;
  //   controlFind = findAndUpdateControlById(id);

  //   if (controlFind == null) {
  //     controlFind = Control.init(id: id, value: value);
  //   }

  //   return controlFind;
  // }

  Control? findAndUpdateControlById(String id,
      {bool update = false, Control? controlNew}) {
    Control? controlFind;

    for (var pages in lsPages) {
      for (var row in pages.row) {
        for (var control in row) {
          if (control.id == id) {
            controlFind = control;
            if (update) control = controlNew!;

            break;
          }
        }
        if (controlFind != null) break;
      }
      if (controlFind != null) break;
    }
    return controlFind;
  }

  void addObjectToPage(List<Control> controls) {
    //
    if (controls.isNotEmpty) {
      Control? control = findAndUpdateControlById(controls[0].id);
      if (control == null) {
        //thêm mới
        lsPages[currentPage].row.add(controls.sublist(0, 1));
      } else {
        //update
        Control controlNew = controls[0];
        findAndUpdateControlById(controlNew.id,
            controlNew: controlNew, update: true);
      }
    }
  }

  void initWidgetForm() {
    lsRow = renderRow(lsPages[currentPage].row);
  }

  List<Widget> getWidgetRender() {
    return lsRow;
  }

  List<List<Control>> filterWidget(List<List<Control>> listControls,
      {String idRemove = ""}) {
    List<List<Control>> ls = [];

    for (var element in listControls) {
      List<Control> rows = [];
      for (var widget in element) {
        if (widget.id != idRemove) {
          rows.add(widget);
        }
      }
      ls.add(rows);
      rows = [];
    }

    return ls;
  }

  void setVisible(List<List<Control>> listControls, bool visible) {
    Map<String, dynamic> data = <String, dynamic>{};
    for (var element in listControls) {
      // Row row = element as Row;
      for (var control in element) {
        control.visible.value = visible;
      }
    }
  }

  List<Widget> renderRow(List<List<Control>>? ls) {
    List<Widget> lsRow = [];
    for (var row in ls!) {
      List<Widget> columns = [];

      row.asMap().forEach((index, value) {
        bool isLastRow = index == (row.length - 1);
        if (index == (row.length - 2) && !row[row.length - 1].visible.value) {
          isLastRow = true;
        }
        columns.add(renderView(value, index, isLastRow)!);
      });

      Widget rowE = Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: columns);

      lsRow.add(rowE);
    }
    return lsRow;
  }

  Widget? renderView(Control control, index, isLastRow) {
    control.setIndexRow(index);
    control.isLastRow = isLastRow;
    switch (control.typeInput) {
      case TypeInputForm.checkbox:
        return CheckBoxForm(
          control: control,
          onChangeCallback: onChanges,
          onEditingComplete: onEditingComplete,
        );
      case TypeInputForm.textField:
        return TextFieldForm(
          control: control,
          onChangeCallback: onChanges,
          onEditingComplete: onEditingComplete,
        );
      case TypeInputForm.text:
        return TextForm(
          control: control,
          onChangeCallback: onChanges,
        );
      case TypeInputForm.dateTimePicker:
        return DatePickerProductionView(
            control: control,
            title: control.title.value,
            minDate: control.isMinDateCurrent == true ? DateTime.now() : null,
            onChangeCallback: onChanges,
            onEditingComplete: onEditingComplete
            // onChangeCallback: onChanges,
            );
      case TypeInputForm.dropDownSearch:
        return DropdownSearchProduction(
            onChangeCallback: onChanges,
            control: control,
            onEditingComplete: onEditingComplete);
      case TypeInputForm.lineBox:
        return Padding(
          padding: const EdgeInsets.only(top: 16, bottom: 0),
          child: Container(
            height: 8,
            width: Get.width,
            decoration: const BoxDecoration(color: neutral_100),
          ),
        );
      case TypeInputForm.line:
        return LineForm(control: control);
      case TypeInputForm.button:
        return ButtonForm(
            control: control,
            onChangeCallback: onChanges,
            onEditingComplete: onEditingComplete);
      case TypeInputForm.buttonOCR:
        return ButtonOcrForm(
          control: control,
          onChangeCallback: onChanges,
        );
      case TypeInputForm.kyPhi:
        return KyPhiForm(control: control);

      case TypeInputForm.buttonUploadImage:
        return ButtonUploadImageForm(
            onChangeCallback: onChanges,
            control: control,
            onEditingComplete: onEditingComplete);
      default:
        return const SizedBox(width: 0, height: 0);
    }
  }

  void popToScreen({required String screen}) {
    Navigator.of(Get.context!).popUntil(ModalRoute.withName(screen));
  }
}
