import 'dart:async';
import 'package:flutter/material.dart';

class GetBaseScreen extends StatelessWidget {
  GetBaseScreen({Key? key}) : super(key: key);

  final GlobalKey<ScaffoldState> _scaffoldStateKey =
      GlobalKey(debugLabel: "GetBasicScreen");

  @override
  Widget build(BuildContext context) {
    var appBar = createAppBar();
    var floatingButton = createFloatingButton();

    return WillPopScope(
      child: Scaffold(
        key: UniqueKey(),
        appBar: appBar,
        floatingActionButton: floatingButton,
        bottomNavigationBar: createBottomBar(),
        body: createBody(),
      ),
      onWillPop: () {
        return willPopToPreviosScreen();
      },
    );
  }

  Widget? createBody() {
    return null;
  }

  PreferredSizeWidget? createAppBar() {
    return null;
  }

  Widget? createBottomBar() {
    return null;
  }

  Widget? createFloatingButton() {
    return null;
  }

  Future<bool> willPopToPreviosScreen() {
    return Future(() => true);
  }
}
