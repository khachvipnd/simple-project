import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/colors.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/widgets/base/imageview/image_view_widget.dart';

import '../../constants/size.dart';
import '../base/app_controller.dart';

class Header extends StatefulWidget {
  const Header({Key? key, this.appController, this.callback}) : super(key: key);
  final AppController? appController;
  final CallbackData? callback;

  @override
  State<StatefulWidget> createState() => _Header();
}

class _Header extends State<Header> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 80 + MediaQuery.of(context).padding.top,
        child: Stack(
          children: [
            Positioned(
                top: MediaQuery.of(context).padding.top,
                bottom: 0,
                left: 0,
                right: 0,
                child: Center(
                  child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Center(
                        child: Row(
                          children: [
                            Obx(() => renderAvatart()),
                            Obx(() => renderUserName()),
                            Expanded(
                              // flex: 1,
                              child: Container(height: 80),
                            ),
                            // renderSearchButton(),
                            renderRingButton(),
                          ],
                        ),
                      )),
                ))
          ],
        ));
  }

  Widget renderUserName() {
    return Center(
      child: Padding(
          padding: const EdgeInsets.only(left: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("titlewelcome".tr,
                  style: normalStyle.copyWith(
                      color: neutral_400, fontWeight: FontWeight.w400)),
              const SizedBox(
                height: 6,
              ),
              Text(widget.appController?.name.value ?? "",
                  style: boldStyle.copyWith(
                    fontSize: fontSize16,
                    color: neutral_500,
                  ))
            ],
          )),
    );
  }

  Widget renderRingButton() {
    return InkWell(
      onTap: () {},
      child: Padding(
          padding: const EdgeInsets.all(0),
          child: Stack(
            children: [
              Padding(
                  padding: const EdgeInsets.only(right: 8),
                  child: Center(
                    child: SvgPicture.asset(
                      'assets/svgs/icon_ring.svg',
                      height: 24.0,
                      width: 24.0,
                      color: neutral_500,
                    ),
                  )),
              Obx(() => Positioned(
                  right: 0,
                  top: 4,
                  child: Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                        color: widget.appController?.numberNoti.value == 0
                            ? Colors.transparent
                            : red_500,
                        borderRadius:
                            const BorderRadius.all(Radius.circular(10))),
                    // padding: const EdgeInsets.only(left: 14),
                    child: Center(
                        child: Text(
                      widget.appController?.numberNoti.value != 0
                          ? ((widget.appController?.numberNoti.value ?? 0)
                              .toString())
                          : "",
                      textAlign: TextAlign.center,
                      style: boldStyle.copyWith(
                          color: Colors.white, fontSize: fontSize11),
                    )),
                  )
                  //Notify not read

                  // Text("", style: boldStyle.copyWith(color: Colors.red),)
                  ))
            ],
          )),
    );
  }

  Widget renderSearchButton() {
    return InkWell(
      onTap: () {
        // printDebug("onTap called.");
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SvgPicture.asset(
          'assets/svgs/icon_seach.svg',
          height: 24.0,
          width: 24.0,
        ),
      ),
    );
  }

  Widget renderAvatart() {
    // if (widget.appController!.isLogin) {
    return InkWell(
        onTap: (() {
          if (widget.callback != null) {
            widget.callback!(true);
          }
        }),
        child: SizedBox(
            height: 48,
            width: 48,
            child: ImageViewWidget(
              heightImage: 48,
              widthImage: 48,
              fit: BoxFit.cover,
              radius: 24,
              url: widget.appController?.avatar.value,
            )));

    // return SizedBox(
    //     height: 48,
    //     width: 48,
    //     child: Stack(
    //         fit: StackFit.expand,
    //         children: [SvgPicture.asset(avatar, height: 48, width: 48)]));
  }
}
