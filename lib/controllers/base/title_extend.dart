import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/colors.dart';
import 'package:yourpti/constants/styles.dart';
// import 'package:yourpti/screens.dart';

import '../../constants/size.dart';

class TitleExtend extends StatefulWidget {
  TitleExtend({Key? key, this.title, this.callback});

  CallbackData? callback;
  String? title;
  @override
  State<StatefulWidget> createState() => _TitleExtend();
}

class _TitleExtend extends State<TitleExtend> {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 25,
        padding: const EdgeInsets.only(left: 16, right: 16, top: 0),
        child: Row(children: [
          Expanded(
              child: Text(widget.title ?? "utilities".tr,
                  style: boldStyle.copyWith(
                      color: neutral_500,
                      fontSize: fontSize18,
                      fontWeight: FontWeight.w600))),
        ]));
  }
}
