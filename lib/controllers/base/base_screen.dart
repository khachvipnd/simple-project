import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/extensions/file.dart';
import 'package:yourpti/widgets/base/dialog/dialog.dart';
import 'package:yourpti/widgets/base/dropdowns/drop_list_model.dart';

import '../../constants/colors.dart';
import '../../constants/enums.dart';
import '../../constants/size.dart';
import '../../constants/styles.dart';
import '../../screens.dart';
import '../../widgets/base/dropdown_search/view_search_content.dart';

abstract class BaseWidget extends StatelessWidget {
  const BaseWidget({Key? key}) : super(key: key);

  showAlertDialogMessage(
      {String? title,
      String content = "",
      double maxHeight = 150,
      String textCancel = "cancel",
      String textDone = "accept",
      VoidCallback? onDoneClick,
      VoidCallback? onCancelClick}) {
    if (Get.context == null) {
      return;
    }
    Future.delayed(Duration.zero, () {
      showDialog(
          context: Get.context!,
          // barrierDismissible: false,
          builder: (BuildContext context) => Dialog(
              elevation: 0,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(24)),
              ),
              backgroundColor: Colors.transparent,
              child: DialogView(
                content: content,
                titleButtonCancel: textCancel,
                titleButtonDone: textDone,
                maxHeight: maxHeight,
                onDoneClick: onDoneClick,
                onCancelClick: onCancelClick,
                title: title,
              )));
    });
  }

  showPopupConfirmLogin() {
    showAlertDialogMessage(
        title: "requiredlogin",
        content: "requiredlogincontent",
        onCancelClick: (() {}),
        onDoneClick: (() {
          Get.toNamed(loginView);
        }),
        textDone: "login",
        maxHeight: 170);
  }
}

abstract class BaseScreen extends StatefulWidget {
  const BaseScreen({Key? key}) : super(key: key);
}

abstract class BaseState<Screen extends BaseScreen> extends State<Screen> {
  var isShowLoading = false;
  AppBar headerView(
      {required String title,
      Color backgroundColor = white,
      Color textColor = neutral_500,
      List<Widget>? actions,
      Widget? leading,
      TabBar? tabBar,
      double elevation = 0.5,
      bool isShowBack = true}) {
    return AppBar(
      iconTheme: IconThemeData(
        color: textColor, //change your color here
      ),
      elevation: elevation,
      bottom: tabBar,
      backgroundColor: backgroundColor,
      centerTitle: true,
      automaticallyImplyLeading: isShowBack,
      actions: actions,
      leading: leading,
      title: Text(
        title.tr,
        textAlign: TextAlign.center,
        style: boldStyle.copyWith(fontSize: fontSize16, color: textColor),
      ),
    );
  }

  double heightBottomSafa() {
    return MediaQuery.of(context).padding.bottom;
  }

  TabBar tabBars(
      {List<Tab>? tabs,
      bool isScrollable = false,
      TabController? controller,
      EdgeInsetsGeometry labelPadding =
          const EdgeInsets.symmetric(horizontal: 0),
      EdgeInsetsGeometry indicatorPadding =
          const EdgeInsets.symmetric(horizontal: 0),
      CallbackData? callback}) {
    return TabBar(
        controller: controller,
        isScrollable: isScrollable,
        indicatorColor: neutral_400,
        labelPadding: labelPadding,
        labelColor: primary,
        unselectedLabelStyle: normalStyle.copyWith(fontSize: fontSize12),
        labelStyle: normalStyle.copyWith(
            fontSize: fontSize12, fontWeight: FontWeight.w400),
        indicatorPadding: indicatorPadding,
        unselectedLabelColor: neutral_400,
        onTap: (index) {
          if (callback != null) {
            callback(index);
          }
        },
        indicator: const UnderlineTabIndicator(
            borderSide: BorderSide(width: 3.0, color: primary),
            insets: EdgeInsets.symmetric(horizontal: 16.0)),
        tabs: tabs ?? []);
  }

  Widget getDefaultFloatingButton({required VoidCallback onPress}) {
    var floatingBtn = Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 0, right: 16),
          child: FloatingActionButton(
            child: const Icon(
              Icons.check,
              color: Colors.white,
            ),
            onPressed: onPress,
          ),
        )
      ],
    );

    return floatingBtn;
  }

  showDropDown({
    String title = "",
    double heightView = 300,
    List<OptionItem> items = const [],
    // required this.context,
    bool isSearch = false,
    CallbackData? callback,
    String hindText = "",
    Widget? childPopup,
    OptionItem? initItem,
  }) {
    FocusScope.of(context).unfocus();
    var height = heightView;

    if (items.isNotEmpty) {
      height = (items.length * 42 + 150);
    }
    if (height <= 330) {
      height = 330;
    }
    if (height >= Get.height - 100) {
      height = Get.height - 100;
    }
    showModalBottomSheet<void>(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return SizedBox(
            height: height,
            child: ViewSearchContent(
              isSearch: isSearch,
              title: title,
              childPopup: childPopup,
              items: items,
              callback: (value) {
                if (callback != null) {
                  callback(value);
                }
                // setState(() {});
              },
              listData: const [],
            ),
          );
        });
  }

  showAlertDialogMessage(
      {String? title,
      String content = "",
      double maxHeight = 150,
      String textCancel = "cancel",
      String textDone = "accept",
      VoidCallback? onDoneClick,
      VoidCallback? onCancelClick,
      bool forceHideXButton = false}) {
    showDialog(
        context: context,
        // barrierDismissible: false,
        builder: (BuildContext context) => Dialog(
            elevation: 0,
            shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(24)),
            ),
            backgroundColor: Colors.transparent,
            child: DialogView(
              content: content,
              maxHeight: maxHeight,
              onDoneClick: onDoneClick,
              onCancelClick: onCancelClick,
              title: title,
              titleButtonCancel: textCancel,
              titleButtonDone: textDone,
              forceHideXButton: forceHideXButton,
            )));
  }

  showPopupConfirmLogin() {
    showAlertDialogMessage(
        title: "requiredlogin",
        content: "requiredlogincontent",
        onCancelClick: (() {}),
        onDoneClick: (() {
          Get.toNamed(loginView);
        }),
        textDone: "login",
        maxHeight: 170);
  }

  showAlertDialogAction(VoidCallback onPressYes, BuildContext context,
      String message, String content, String nameAction) {
    // show the dialog
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(message),
        content: Text(content),
        actions: [
          CupertinoDialogAction(
              isDefaultAction: true,
              isDestructiveAction: true,
              child: Text(nameAction),
              onPressed: () {
                Navigator.pop(context, false);
                onPressYes();
              })
        ],
      ),
    );
  }

  void showMessage(String content, ScaffoldState scaffoldState) {
    // final snackBar = SnackBar(
    //   content: Text(content),
    // action: SnackBarAction(
    //   label: 'Undo',
    //   onPressed: () {
    //     // Some code to undo the change.
    //   },
    // ),
    // );
    // Find the Scaffold in the widget tree and use
    // it to show a SnackBar.
    // scaffoldState.showSnackBar(snackBar);
  }

  Widget getFloatingButton({
    required Color color,
    required IconData iconData,
    required VoidCallback onPress,
  }) {
    var floatingBtn = Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(bottom: 0, right: 16),
          child: FloatingActionButton(
            child: Icon(
              iconData,
              color: Colors.white,
            ),
            onPressed: onPress,
            backgroundColor: color,
          ),
        )
      ],
    );

    return floatingBtn;
  }

  void popToScreen({required String screen}) {
    Navigator.of(context).popUntil(
      ModalRoute.withName(screen),
    );
  }

  void showProcessingFeatureMessage(ScaffoldState scaffoldState) {
    showMessage("Tính năng đang được xây dựng", scaffoldState);
  }

  closeKeyborad() {
    FocusScope.of(context).unfocus();
  }

  Widget? createBottomBar() {
    return null;
  }

  Widget? createFloatingButton() {
    return null;
  }

  void showPicker(
      {CallbackData? callback, CameraType type = CameraType.full}) async {
    switch (type) {
      case CameraType.camera:
        XFile? image = await _imgFromCamera();
        if (callback != null) {
          callback(image?.path);
        }
        Navigator.of(context).pop();
        break;
      case CameraType.photo:
        XFile? image = await _imgFromGallery();
        if (callback != null) {
          callback(image?.path);
        }
        Navigator.of(context).pop();
        break;
      case CameraType.full:
        showModalBottomSheet(
            context: context,
            builder: (BuildContext bc) {
              return SafeArea(
                child: Wrap(
                  children: <Widget>[
                    ListTile(
                        leading: const Icon(Icons.photo_library),
                        title: const Text('Thư viện ảnh',
                            style: TextStyle(fontSize: fontSizeNomal)),
                        onTap: () async {
                          XFile? image = await _imgFromGallery();
                          Navigator.of(context).pop();
                          if (callback != null && image != null) {
                            callback(image.path);
                          }
                        }),
                    ListTile(
                      leading: const Icon(Icons.photo_camera),
                      title: const Text('Camera',
                          style: TextStyle(fontSize: fontSizeNomal)),
                      onTap: () async {
                        XFile? image = await _imgFromCamera();
                        Navigator.of(context).pop();
                        if (callback != null && image != null) {
                          callback(image.path);
                        }
                      },
                    ),
                  ],
                ),
              );
            });
        break;
      default:
    }
  }

  Future<XFile?> _imgFromCamera() async {
    final ImagePicker _picker = ImagePicker();
    XFile? image = await _picker.pickImage(
        source: ImageSource.camera,
        maxHeight: 1200,
        maxWidth: 1600,
        imageQuality: 80);
    // printDebug(image?.readAsBytes());
    return image;
  }

  Future<XFile?> _imgFromGallery() async {
    final ImagePicker _picker = ImagePicker();
    XFile? image = await _picker.pickImage(
        source: ImageSource.gallery,
        maxHeight: 1200,
        maxWidth: 1600,
        imageQuality: 80);
    getFileSize(image?.path ?? "");
    return image;
  }

  getFileSize(String filepath) async {
    var file = File(filepath);
    printDebug(file.size);
  }

  showLoading({int timeout = 15}) {
    Future.delayed(Duration(seconds: timeout), () {
      if (isShowLoading) {
        closeLoading();
      }
    });

    Future.delayed(Duration.zero, () {
      isShowLoading = true;
      Get.dialog(
          Center(
              child: SizedBox(
            width: 120,
            height: 120,
            child: Stack(
              children: [
                Center(
                  child: Image.asset(
                    "assets/images/progress_indicator.gif",
                  ),
                ),
                Center(
                  child: Image.asset(
                    "assets/images/icon_logo_progress.png",
                    width: 50,
                    height: 50,
                  ),
                ),
              ],
            ),
          )),
          barrierDismissible: false);
    });
  }

  closeLoading() {
    if (isShowLoading) {
      isShowLoading = false;
      Get.back();
    }
  }

  void printDebug(dynamic data) {
    if (kDebugMode) {
      print(data);
    }
  }
}

class HighlightIndex {
  final int idxFrom;
  final int length;

  int get idxTo {
    return idxFrom + length;
  }

  HighlightIndex(this.idxFrom, this.length);

  static List<HighlightIndex> fromString(String content, String highlight) {
    List<int> indexes = [];
    int idxStart = 0;
    while (true) {
      final index = content.indexOf(highlight, idxStart);
      if (index >= 0) {
        indexes.add(index);
        idxStart += index + highlight.length;
      } else {
        return indexes.map((e) => HighlightIndex(e, highlight.length)).toList();
      }
    }
  }
}
