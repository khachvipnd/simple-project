import 'package:flutter/material.dart';
// import 'package:get/get.dart';
import 'package:yourpti/constants/colors.dart';
import '../base/base_screen.dart';

mixin BasicState<Screen extends BaseScreen> on BaseState<Screen> {
  final GlobalKey<ScaffoldState> _scaffoldStateKey =
      GlobalKey(debugLabel: "BasicState");

  @override
  Widget build(BuildContext context) {
    var appBar = createAppBar();
    var floatingButton = createFloatingButton();
    if (numberTabController > 0) {
      return DefaultTabController(
        length: numberTabController,
        child: WillPopScope(
          child: Scaffold(
            backgroundColor: white,
            key: _scaffoldStateKey,
            appBar: appBar,
            floatingActionButton: floatingButton,
            bottomNavigationBar: createBottomBar(),
            body: isSafeArea == true
                ? Container(
                    color: colorSafeArea,
                    child: SafeArea(
                        left: false, right: false, child: createBody()))
                : createBody(),
          ),
          onWillPop: () {
            return willPopToPreviosScreen();
          },
        ),
      );
    }
    if (!isScaffold) {
      return WillPopScope(
          onWillPop: () {
            return willPopToPreviosScreen();
          },
          child: createBody());
    }
    return WillPopScope(
      child: Scaffold(
        backgroundColor: white,
        // key: _scaffoldStateKey,
        appBar: appBar,
        floatingActionButton: floatingButton,
        bottomNavigationBar: createBottomBar(),
        body: isSafeArea == true
            ? GestureDetector(
                onPanUpdate: (details) {
                  // Swiping in right direction.
                  if (details.delta.dx > 0) {
                    // Get.back();
                  }

                  // Swiping in left direction.
                  if (details.delta.dx < 0) {}
                },
                onTap: () {
                  FocusScope.of(context).unfocus();
                },
                child: Container(
                    color: colorSafeArea,
                    child: SafeArea(
                        left: false,
                        right: false,
                        top: isTopSafeArea,
                        bottom: isBottomSafeArea,
                        child: createBody())))
            : GestureDetector(
                onPanUpdate: (details) {
                  // Swiping in right direction.
                  if (details.delta.dx > 0) {
                    // Get.back();
                  }

                  // Swiping in left direction.
                  if (details.delta.dx < 0) {
                    // Get.back();
                  }
                },
                onTap: () {
                  FocusScope.of(context).unfocus();
                },
                child: createBody()),
      ),
      onWillPop: () {
        return willPopToPreviosScreen();
      },
    );
  }

  bool get isSafeArea {
    return true;
  }

  bool get isScaffold {
    return true;
  }

  bool get isBottomSafeArea {
    return true;
  }

  bool get isTopSafeArea {
    return true;
  }

  Color get colorSafeArea {
    return white;
  }

  int get numberTabController {
    return 0;
  }

  Widget createBody();

  PreferredSizeWidget? createAppBar() {
    return null;
  }

  Future<bool> willPopToPreviosScreen() {
    return Future(() => true);
  }
}
