import 'dart:convert';

import 'package:get/get.dart';
import 'package:yourpti/controllers/base/base_controller.dart';

class AppController extends BaseController {
  String language = "vn";

  var avatar = "".obs;
  var name = "".obs;
  var priceRemuneration = "0".obs;
  bool isOpenCer = false;
  dynamic jsonHome;
  RxInt numberNoti = 0.obs;
  @override
  Future<void> onInit() async {
    super.onInit();
  }

  offNameView({required String screen}) {
    Get.offNamed(screen);
  }
}
