import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';

import '../../constants/callbacks.dart';
import '../../constants/enums.dart';
import '../../constants/styles.dart';
import '../../widgets/base/dialog/dialog.dart';

class BaseController extends GetxController with WidgetsBindingObserver {
  var isShowLoading = false;
  RxBool isShowKey = false.obs;

  @override
  void onInit() {
    super.onInit();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void onClose() {
    WidgetsBinding.instance.removeObserver(this);
    super.onClose();
  }

  @override
  void didChangeMetrics() {
    final bottomInset = WidgetsBinding.instance.window.viewInsets.bottom;
    final newValue = bottomInset > 0.0;

    if (newValue != isShowKey.value) {
      isShowKey.value = newValue;
      // if (newValue) {
      //   showkey.value = "0";
      // } else {
      //   showkey.value = "1";
      // }

      // print("1");
    }
  }

  void showPicker(
      {CallbackData? callback,
      CameraType type = CameraType.full,
      bool resizeImage = true}) async {
    switch (type) {
      case CameraType.camera:
        XFile? image = await _imgFromCamera(resizeImage);
        if (callback != null) {
          callback(image?.path);
        }
        Navigator.of(Get.context!).pop();
        break;
      case CameraType.photo:
        XFile? image = await _imgFromGallery(resizeImage);
        if (callback != null) {
          callback(image?.path);
        }
        // Navigator.of(Get.context!).pop();
        break;
      case CameraType.full:
        showModalBottomSheet(
            context: Get.context!,
            builder: (BuildContext bc) {
              return SafeArea(
                child: Wrap(
                  children: <Widget>[
                    ListTile(
                        leading: const Icon(Icons.photo_library),
                        title: const Text('Thư viện ảnh',
                            style: TextStyle(fontSize: fontSizeNomal)),
                        onTap: () async {
                          XFile? image = await _imgFromGallery(resizeImage);
                          Navigator.of(Get.context!).pop();
                          if (callback != null && image != null) {
                            callback(image.path);
                          }
                        }),
                    ListTile(
                      leading: const Icon(Icons.photo_camera),
                      title: const Text('Camera',
                          style: TextStyle(fontSize: fontSizeNomal)),
                      onTap: () async {
                        XFile? image = await _imgFromCamera(resizeImage);
                        Navigator.of(Get.context!).pop();
                        if (callback != null && image != null) {
                          callback(image.path);
                        }
                      },
                    ),
                  ],
                ),
              );
            });
        break;
      default:
    }
  }

  Future<XFile?> _imgFromCamera(resizeImage) async {
    final ImagePicker _picker = ImagePicker();
    XFile? image = await _picker.pickImage(
        source: ImageSource.camera, imageQuality: resizeImage ? 80 : 100);
    return image;
  }

  Future<XFile?> _imgFromGallery(resizeImage) async {
    final ImagePicker _picker = ImagePicker();
    XFile? image = await _picker.pickImage(
        source: ImageSource.gallery, imageQuality: resizeImage ? 80 : 100);
    return image;
  }

  showLoading({int timeout = 30}) {
    Future.delayed(Duration(seconds: timeout), () {
      if (isShowLoading) {
        closeLoading();
      }
    });

    Future.delayed(Duration.zero, () {
      if (isShowLoading) return;
      isShowLoading = true;
      Get.dialog(
          Center(
              child: SizedBox(
            width: 120,
            height: 120,
            child: Stack(
              children: [
                Center(
                  child: Image.asset(
                    "assets/images/progress_indicator.gif",
                  ),
                ),
                Center(
                  child: Image.asset(
                    "assets/images/icon_logo_progress.png",
                    width: 50,
                    height: 50,
                  ),
                ),
              ],
            ),
          )),
          barrierDismissible: false);
    });
  }

  showAlertDialogMessage(
      {String? title,
      String content = "",
      double maxHeight = 150,
      VoidCallback? onDoneClick,
      VoidCallback? onCancelClick,
      String? titleButtonDone,
      String? titleButtonCancel,
      int? timeForDismiss}) {
    if (Get.context == null) {
      return;
    }
    Future.delayed(Duration.zero, () {
      showDialog(
          context: Get.context!,
          // barrierDismissible: false,
          builder: (BuildContext context) => Dialog(
              elevation: 0,
              shape: const RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(24)),
              ),
              backgroundColor: Colors.transparent,
              child: DialogView(
                  content: content,
                  maxHeight: maxHeight,
                  onDoneClick: onDoneClick,
                  onCancelClick: onCancelClick,
                  title: title,
                  titleButtonDone: titleButtonDone,
                  titleButtonCancel: titleButtonCancel,
                  timeForDismiss: timeForDismiss)));
    });
  }

  closeLoading() {
    if (isShowLoading) {
      isShowLoading = false;
      Get.back();
    }
  }

  void printDebug(dynamic data) {
    if (kDebugMode) {
      print(data);
    }
  }
}
