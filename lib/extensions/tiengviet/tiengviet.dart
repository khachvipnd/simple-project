import 'tiengviet_core.dart';

String tiengviet(String text) => TiengVietCore.unsign(text);

abstract class TiengViet {
  static String parse(String text) {
    return TiengVietCore.unsign(text);
  }
}
