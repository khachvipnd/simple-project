import 'dart:developer';

import 'package:yourpti/constants/strings.dart';
import 'manager.dart';

class NetworkManger {
  final APIManager apiManager = APIManager();

  NetworkManger();

  Future<APIResponse<dynamic>> post({
    required String url,
    Map<String, dynamic>? body,
    Map<String, dynamic>? headers,
  }) async {
    String tokenString =
        await storageManger.getStorePreferences(type: token) ?? "";
    String language = storageManger.getStorage(type: apiLanguage) ?? "";
    headers?["Content-Type"] = "application/json";
    if (tokenString.isNotEmpty) {
      headers?["Authorization"] = "Bearer $tokenString";
    }
    if (language.isNotEmpty) {
      body?["lang"] = language;
    }

    return apiManager
        .postWithURLEncoded(url: url, body: body, headers: headers)
        .then((value) {
      _loggingParam(
          url: url, body: body, headers: headers, message: value.message);
      // if (value.apiCode == APICode.expiredSession) {
      //   locator<NavigationService>().pushReplacementNamedTo(LoginScreen.sName);
      // }

      // if (value.apiCode != APICode.ok) {
      //   throw Exception(value.message);
      // }

      return value;
    });
  }

  Future<APIResponse<dynamic>> get({
    required String url,
    Map<String, dynamic>? params,
    Map<String, dynamic>? headers,
  }) async {
    String tokenString =
        await storageManger.getStorePreferences(type: token) ?? "";
    String language = storageManger.getStorage(type: apiLanguage) ?? "";
    if (tokenString.isNotEmpty) {
      headers?["Authorization"] = "Bearer $tokenString";
    }
    if (language.isNotEmpty) {
      params?["lang"] = language;
    }

    return apiManager
        .getWithURLEncoded(url: url, parame: params, headers: headers)
        .then((value) {
      _loggingParam(
          url: url, body: params, headers: headers, message: value.message);
      // if (value.apiCode == APICode.expiredSession) {
      //   locator<NavigationService>().pushReplacementNamedTo(LoginScreen.sName);
      // }

      if (value.apiCode != APICode.ok) {
        throw Exception(value.message);
      }

      return value;
    });
  }

  Future<APIResponse<dynamic>> updateFile({
    required String url,
    Map<String, dynamic>? body,
    Map<String, String>? headers,
    Map<String, String>? mappingFile,
  }) async {
    String tokenString =
        await storageManger.getStorePreferences(type: token) ?? "";
    String language = storageManger.getStorage(type: apiLanguage) ?? "";
    if (tokenString.isNotEmpty) {
      headers?["Authorization"] = "Bearer $tokenString";
    }
    if (language.isNotEmpty) {
      body?["lang"] = language;
    }

    return apiManager
        .uploadFiles(url, body, headers, mappingFile)
        .then((value) {
      _loggingParam(
          url: url, body: body, headers: headers, message: value.message);
      // if (value.apiCode == APICode.expiredSession) {
      //   // process logic
      //   locator<NavigationService>().pushReplacementNamedTo(LoginScreen.sName);
      // }

      if (value.apiCode == APICode.expiredSession) {
      } else {
        // if (value.apiCode != APICode.ok) {
        //   throw Exception(value.message);
        // }
      }

      return value;
    });
  }

  void _loggingParam({
    String? url,
    Map<String, dynamic>? body,
    Map<String, dynamic>? headers,
    String? message,
  }) {
    log("Network URL $url");
    log("Network Body $body");
    log("Network Headers $headers");
    log("Network Message $message");
  }
}
