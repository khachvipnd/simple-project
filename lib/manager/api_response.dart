enum APICode { serverError, failRequest, expiredSession, ok }

class APIResponse<T> {
  T? value;
  final String? message;
  final String? code;
  int total = 0;

  APIResponse({this.value, this.message, this.code, this.total = 0});

  APIResponse.error(String? errorStr)
      : this(value: null, message: errorStr, code: "-1");

  APIResponse.success({dynamic value})
      : this(value: value, message: "", code: "000");

  APIResponse.clone(T value, APIResponse one)
      : this(value: value, message: one.message, code: one.code);

  APICode get apiCode {
    if (code == "000") {
      return APICode.ok;
    }

    if (code == "-1") {
      return APICode.serverError;
    }

    if (code == "010") {
      return APICode.expiredSession;
    }

    return APICode.failRequest;
  }

  bool get success {
    return apiCode == APICode.ok;
  }

  bool get fail {
    return !success;
  }
}
