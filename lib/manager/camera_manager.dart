import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:yourpti/extensions/file.dart';

import '../constants/callbacks.dart';
import '../constants/enums.dart';
import '../constants/styles.dart';

class CameraManager {
  const CameraManager();
  void showPicker(
      {CallbackData? callback,
      CameraType type = CameraType.full,
      BuildContext? context}) async {
    if (context == null) {
      return;
    }
    switch (type) {
      case CameraType.camera:
        XFile? image = await _imgFromCamera();
        if (callback != null) {
          callback(image);
        }
        Navigator.of(context).pop();
        break;
      case CameraType.photo:
        XFile? image = await _imgFromGallery();
        if (callback != null) {
          callback(image);
        }
        Navigator.of(context).pop();
        break;
      case CameraType.full:
        showModalBottomSheet(
            context: context,
            builder: (BuildContext bc) {
              return SafeArea(
                child: Wrap(
                  children: <Widget>[
                    ListTile(
                        leading: const Icon(Icons.photo_library),
                        title: const Text('Thư viện ảnh',
                            style: TextStyle(fontSize: fontSizeNomal)),
                        onTap: () async {
                          XFile? image = await _imgFromGallery();
                          Navigator.of(context).pop();
                          if (callback != null && image != null) {
                            callback(image);
                          }
                        }),
                    ListTile(
                      leading: const Icon(Icons.photo_camera),
                      title: const Text('Camera',
                          style: TextStyle(fontSize: fontSizeNomal)),
                      onTap: () async {
                        XFile? image = await _imgFromCamera();
                        Navigator.of(context).pop();
                        if (callback != null && image != null) {
                          callback(image);
                        }
                      },
                    ),
                  ],
                ),
              );
            });
        break;
      default:
    }
  }

  Future<XFile?> _imgFromCamera() async {
    final ImagePicker _picker = ImagePicker();
    XFile? image = await _picker.pickImage(
        source: ImageSource.camera,
        maxHeight: 1200,
        maxWidth: 1600,
        imageQuality: 80);
    return image;
  }

  Future<XFile?> _imgFromGallery() async {
    final ImagePicker _picker = ImagePicker();
    XFile? image = await _picker.pickImage(
        source: ImageSource.gallery,
        maxHeight: 1200,
        maxWidth: 1600,
        imageQuality: 80);

    getFileSize(image!.path);
    return image;
  }

  getFileSize(String filepath) async {
    var file = File(filepath);
    if (kDebugMode) {
      print(file.size);
    }
  }
}
//Alternatively for extension:
