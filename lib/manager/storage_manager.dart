import 'package:flutter/material.dart';
import 'package:get_storage/get_storage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../constants/callbacks.dart';

class StorageManager {
  StorageManager();
  final box = GetStorage();

  void addStorage({required String type, dynamic data}) async {
    await box.write(type, data);
  }

  Future<void> addStoragePreferences(
      {required String type, String data = ""}) async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setString(type, data);
  }

  dynamic getStorage({required String type}) {
    return box.read(type);
  }

  Future<String?> getStorePreferences({required String type}) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.getString(type);
  }

  removeStoragePreferences({required String type}) async {
    final prefs = await SharedPreferences.getInstance();
    return prefs.remove(type);
  }

  removeStorage({required String type}) {
    return box.remove(type);
  }

  VoidCallback getStreamStorage(
      {required String type, required CallbackData callback}) {
    return box.listenKey(type, (value) {
      // printDebug('new key is $value');
      return callback(value);
    });
  }
}
