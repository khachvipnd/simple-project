import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:dio/dio.dart';
import '../constants/strings.dart';
import 'package:crypto/crypto.dart';
import 'package:convert/convert.dart';
import 'manager.dart';

class APIManager {
  APIManager();
  var countRefrest = 0;
  Future<APIResponse<dynamic>> getRequest({
    required String url,
    Map<String, dynamic>? params,
    Map<String, dynamic>? headers,
  }) async {
    Dio dio = Dio();
    try {
      final requestOption = Options(
          headers: headers,
          sendTimeout: 15000,
          receiveTimeout: 15000,
          followRedirects: false,
          validateStatus: (status) {
            return (status ?? 0) < 500;
          });

      Response response =
          await dio.get(url, queryParameters: params, options: requestOption);

      var jsonBody = response.data;

      // If the call to the server was successful, parse the JSON
      // var jsonData = jsonBody['data'];
      var code = jsonBody['code'];
      var message = jsonBody['message'];
      var jsonData = jsonBody['data'];
      if (code != null) {
        return APIResponse<dynamic>(
            value: jsonData, code: code, message: message);
      }

      return APIResponse<dynamic>(
          value: null,
          code: response.statusCode.toString(),
          message: response.statusMessage);
    } catch (e) {
      return APIResponse.error(null);
    }
  }

  Future<APIResponse<dynamic>> post({
    String? url,
    Map<String, dynamic>? body,
    Map<String, dynamic>? headers,
  }) async {
    final isInterNet = await check();
    if (!isInterNet) {
      return APIResponse<dynamic>(
          code: "-111", message: "Bạn đang không bật mạng", value: null);
    } else {
      return doPostRequest(url: url, body: body, headers: headers)
          .then((response) {
        var jsonBody = response.data;
        // If the call to the server was successful, parse the JSON
        // var jsonData = jsonBody['data'];
        var code = jsonBody['code'];
        var total = jsonBody['Total'];
        var message = jsonBody['message'];
        var jsonData = jsonBody['data'];

        if (code != null) {
          return APIResponse<dynamic>(
              value: jsonData, code: code, message: message);
        }

        return APIResponse<dynamic>(
            value: null,
            code: response.statusCode.toString(),
            total: total,
            message: response.statusMessage);
      }).catchError((e) {
        return APIResponse.error(e.toString());
      });
    }
  }

  Future<APIResponse<dynamic>> postWithURLEncoded({
    String? url,
    Map<String, dynamic>? body,
    Map<String, dynamic>? headers,
  }) async {
    final isInterNet = await check();
    if (!isInterNet) {
      return APIResponse<dynamic>(
          code: "-111", message: "Bạn đang không bật mạng", value: null);
    } else {
      log(url ?? "");
      return doPostRequestWithUrlEncode(url: url, body: body, headers: headers)
          .then((response) async {
        var jsonBody = response.data;
        // If the call to the server was successful, parse the JSON
        // var jsonData = jsonBody['data'];
        var code = jsonBody['code'];
        var message = jsonBody['message'];
        var total = jsonBody['Total'] ?? 0;
        var jsonData = jsonBody['data'];
        if (code == "010") {
          // if (countRefrest >= 2) {
          //   storageManger.addStorage(type: expiredSession, data: "1");
          // }
          String tokenString =
              await storageManger.getStorePreferences(type: token) ?? "";
          final dataHash = {"hash": generateMd5(tokenString + keyMD5Token)};
          // printDebug(generateMd5(tokenString + keyMD5Token));
          final bodyToken = {"data": jsonEncode(dataHash)};
          var responseToken = await postWithURLEncoded(
              url: urlBase + urlRefrestToken,
              body: bodyToken,
              headers: headers);
          String tokenNew = responseToken.value["token"] ?? "";
          if (responseToken.code == "000" && tokenNew.isNotEmpty) {
            storageManger.addStorage(type: token, data: "");
            await storageManger.addStoragePreferences(
                type: token, data: responseToken.value["token"] ?? "");
            var tokenRefrest = responseToken.value["token"] ?? "";
            countRefrest = 0;
            if (tokenRefrest.isNotEmpty) {
              headers?["Authorization"] = "Bearer $tokenRefrest";
            }
            final response = await postWithURLEncoded(
                url: url, body: body, headers: headers);
            return response;
          } else {
            // countRefrest += 1;
            storageManger.addStorage(type: expiredSession, data: "1");
            return responseToken;
          }
        }
        if (code != null) {
          return APIResponse<dynamic>(
              value: jsonData, code: code, message: message, total: total);
        }

        return APIResponse<dynamic>(
            value: null,
            total: total,
            code: response.statusCode.toString(),
            message: response.statusMessage);
      }).catchError((e) {
        return APIResponse.error(e.toString());
      });
    }
  }

  Future<APIResponse<dynamic>> getWithURLEncoded({
    String? url,
    Map<String, dynamic>? parame,
    Map<String, dynamic>? headers,
  }) async {
    final isInterNet = await check();
    if (!isInterNet) {
      return APIResponse<dynamic>(
          code: "-111", message: "Bạn đang không bật mạng", value: null);
    } else {
      return doGetRequestWithUrlEncode(
              url: url, parame: parame, headers: headers)
          .then((response) {
        var jsonBody = response.data;
        // If the call to the server was successful, parse the JSON
        // var jsonData = jsonBody['data'];
        var code = jsonBody['code'];
        var message = jsonBody['message'];
        var jsonData = jsonBody['data'];
        if (code != null) {
          return APIResponse<dynamic>(
              value: jsonData, code: code, message: message);
        }

        return APIResponse<dynamic>(
            value: null,
            code: response.statusCode.toString(),
            message: response.statusMessage);
      }).catchError((e) {
        return APIResponse.error(e.toString());
      });
    }
  }

  Future<Response> doPostRequest({
    String? url,
    Map<String, dynamic>? body,
    Map<String, dynamic>? headers,
  }) async {
    Dio dio = Dio();
    try {
      var requestOption = Options(
          contentType: "application/json",
          headers: headers,
          followRedirects: false,
          validateStatus: (status) {
            return (status ?? 0) < 500;
          });

      Response response =
          await dio.post(url ?? "", data: body, options: requestOption);

      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<Response> doPostRequestWithUrlEncode({
    String? url,
    Map<String, dynamic>? body,
    Map<String, dynamic>? headers,
  }) async {
    Dio dio = Dio();
    try {
      var requestOption = Options(
          contentType: Headers.formUrlEncodedContentType,
          headers: headers,
          followRedirects: false,
          validateStatus: (status) {
            return (status ?? 0) < 500;
          });

      Response response =
          await dio.post(url ?? "", data: body, options: requestOption);

      return response;
    } catch (e) {
      // printDebug(e);
      rethrow;
    }
  }

  Future<Response> doGetRequestWithUrlEncode({
    String? url,
    Map<String, dynamic>? parame,
    Map<String, dynamic>? headers,
  }) async {
    Dio dio = Dio();
    try {
      var requestOption = Options(
          contentType: Headers.formUrlEncodedContentType,
          headers: headers,
          followRedirects: false,
          validateStatus: (status) {
            return (status ?? 0) < 500;
          });

      Response response = await dio.get(url ?? "",
          queryParameters: parame, options: requestOption);

      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<APIResponse<String>> uploadImage(
      String url,
      Map<String, String> bodyMap,
      Map<String, String> headers,
      File imageFile,
      String fileName) async {
    var uri = Uri.parse(url);
    var request = http.MultipartRequest(
      "POST",
      uri,
    );
    request.headers.addAll(headers);

    var keys = bodyMap.keys;
    for (var i = 0; i < bodyMap.length; i++) {
      var key = keys.elementAt(i);
      request.fields[key] = bodyMap[key]!;
    }

    var multFile = await http.MultipartFile.fromPath("image", imageFile.path);
    request.files.add(multFile);

    var response = await request.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    //Get the response from the server
    var jsonBody = jsonDecode(responseString);
    var code = jsonBody['code'];
    var message = jsonBody['message'];

    if (response.statusCode == 201) {
      // If the call to the server was successful, parse the JSON
      // var jsonData = jsonBody['data'];
      var imageID = fileName;
      return APIResponse<String>(value: imageID, code: code, message: message);
    } else {
      return APIResponse<String>(value: null, code: code, message: message);
    }
  }

  Future<Map<String, dynamic>> uploadNewFile(
      String url,
      Map<String, String> bodyMap,
      Map<String, String> headers,
      Map<String, File> fileMapping) async {
    var uri = Uri.parse(url);
    var request = http.MultipartRequest(
      "POST",
      uri,
    );
    request.headers.addAll(headers);

    var keys = bodyMap.keys;
    for (var i = 0; i < bodyMap.length; i++) {
      var key = keys.elementAt(i);
      request.fields[key] = bodyMap[key]!;
    }

    var fileKeys = fileMapping.keys;
    for (var i = 0; i < fileMapping.length; i++) {
      var key = fileKeys.elementAt(i);
      var fileName = fileMapping[key]!.path.split("/").last;
      var fileExtension = fileName.split(".").last;
      var multFile = await http.MultipartFile.fromPath(
          key, fileMapping[key]!.path,
          filename: fileName, contentType: MediaType("image", fileExtension));

      request.files.add(multFile);
    }

    return request
        .send()
        .then((response) => response.stream.toBytes())
        .then((responseData) {
      var responseString = String.fromCharCodes(responseData);
      //Get the response from the server
      var jsonBody = jsonDecode(responseString);

      return jsonBody;
    });
  }

  Future<APIResponse<dynamic>> uploadFiles(
    String? url,
    Map<String, dynamic>? bodyMap,
    Map<String, String>? headers,
    Map<String, String>? mapping,
  ) async {
    final isInterNet = await check();
    if (!isInterNet) {
      return APIResponse<dynamic>(
          code: "-111", message: "Bạn đang không bật mạng", value: null);
    } else {
      var uri = Uri.parse(url ?? "");
      var request = http.MultipartRequest(
        "POST",
        uri,
      );
      if (headers != null) {
        request.headers.addAll(headers);
      }
      if (bodyMap != null) {
        var keys = bodyMap.keys;
        for (var i = 0; i < bodyMap.length; i++) {
          var key = keys.elementAt(i);
          request.fields[key] = bodyMap[key]!;
        }
      }

      if (mapping != null) {
        for (var key in mapping.keys) {
          String imageFileURL = mapping[key]!;
          if (imageFileURL.isNotEmpty) {
            var multFile = await http.MultipartFile.fromPath(key, imageFileURL,
                filename: key + ".jpg",
                contentType: MediaType.parse("image/jpg"));
            request.files.add(multFile);
          }
        }
      }

      var response = await request.send();
      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      //Get the response from the server
      var jsonBody = jsonDecode(responseString);
      var code = jsonBody['code'];
      var message = jsonBody['message'];
      var jsonData = jsonBody['data'];
      if (code != null) {
        return APIResponse<dynamic>(
            value: jsonData, code: code, message: message);
      } else {
        return APIResponse<String>(value: null, code: code, message: message);
      }
    }
  }

  Future<String> readResponse(HttpClientResponse response) {
    var completer = Completer<String>();
    var contents = StringBuffer();
    response.transform(utf8.decoder).listen((data) {
      contents.write(data);
    }, onDone: () => completer.complete(contents.toString()));
    return completer.future;
  }

  Future<bool> check() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile) {
      return true;
    } else if (connectivityResult == ConnectivityResult.wifi) {
      return true;
    }
    return false;
  }

  generateMd5(String data) {
    return md5.convert(utf8.encode(data)).toString();
  }
}
