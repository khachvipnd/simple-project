import 'dart:async';

import 'package:flutter/services.dart';
import 'package:location/location.dart';

class LocationInfor {
  double latitude = 0;
  double longitude = 0;

  LocationInfor({
    this.latitude = 0,
    this.longitude = 0,
  });
}

enum LocationPermissionStatus {
  /// The permission to use location services has been granted.
  granted,

  /// The permission to use location services has been denied by the user. May
  /// have been denied forever on iOS.
  denied,

  /// The permission to use location services has been denied forever by the
  /// user. No dialog will be displayed on permission request.
  deniedForever
}

typedef LocationEventHandler = dynamic Function(dynamic);

class LocationManager {
  late Location location;
  late LocationInfor curInfor = LocationInfor(latitude: 0, longitude: 0);
  final platform = const MethodChannel('ListeningLocation');
  static final LocationManager _instance = LocationManager._internal();

  factory LocationManager() {
    return _instance;
  }

  LocationManager._internal() {
    location = Location();
    location
        .changeSettings(distanceFilter: 100)
        .then((value) => print("changeSettings $value"));
    location
        .enableBackgroundMode(enable: true)
        .then((value) => print("enableBackgroundMode $value"));
  }

  LocationInfor get currentLocation {
    return curInfor;
  }

  Future<LocationPermissionStatus> getPermission() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return LocationPermissionStatus.deniedForever;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      return LocationPermissionStatus.denied;
    }
    return LocationPermissionStatus.granted;
  }

  Future<LocationPermissionStatus> requestPermission() async {
    final _permissionGranted = await location.requestPermission();
    if (_permissionGranted != PermissionStatus.granted) {
      return LocationPermissionStatus.denied;
    }
    return LocationPermissionStatus.granted;
  }

  void requestLocation() {
    location.getLocation().then((value) {
      curInfor = LocationInfor(
          latitude: value.latitude ?? 0, longitude: value.longitude ?? 0);
    });
  }

  onChangeLocation(ValueChanged<LocationInfor> onChanged) {
    location.onLocationChanged.listen((event) {
      curInfor = LocationInfor(
          latitude: event.latitude ?? 0, longitude: event.longitude ?? 0);

      onChanged(curInfor);
    });
  }

  Future<LocationInfor?> updateLastestLocation() {
    return location.getLocation().then((value) {
      curInfor = LocationInfor(
          latitude: value.latitude ?? 0, longitude: value.longitude ?? 0);
      return curInfor;
    });
  }
}
