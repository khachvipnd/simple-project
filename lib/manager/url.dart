// urlBase;'http://partner1api.v-sis.vn:8081/api/';
//http://partner1api.v-sis.vn:8081/api/

// urlBaseSearch dùng server bên mình
const String urlBaseSearch = 'https://betadichvukhachhang.pti.com.vn/api/';
// urlBase;'https://uat-apiseller.pjico.com.vn:9443/api/';

// UAT
const String urlBase = 'https://uat-apiseller.pjico.com.vn:9443/api/';
const String urlKenhBan = 'https://uat-apiseller.pjico.com.vn:9443/api/';
const String urlDomainSingture = 'https://uat-apiseller.pjico.com.vn:9443/api/';
const String urlDomainCer = "https://uat-apiseller.pjico.com.vn:9443/";
const String urlImage = 'https://uat-apiseller.pjico.com.vn:9443/';
const String urlImage2 = 'https://uat-apiseller.pjico.com.vn:9443/';
// 038095029265
// Product
// const String urlBase = 'https://inb-baohiem-api-svc.pjico.com.vn/api/';
// const String urlKenhBan = 'https://inb-baohiem-api-svc.pjico.com.vn/api/';
// const String urlDomainSingture =
//     'https://inb-baohiem-api-svc.pjico.com.vn/api/';
// const String urlDomainCer = "https://inb-baohiem-api-svc.pjico.com.vn/";
// const String urlImage = 'https://inb-baohiem-api-svc.pjico.com.vn/';
// const String urlImage2 = 'https://inb-baohiem-api-svc.pjico.com.vn/';

// search gara,city ..

const String urlCity = 'search/city';
const String urlGaraNode = 'search/gara';
const String urlUnitNode = 'search/units';
const String urlHostpitalNote = "search/hostpital";

const String urlProduction =
    urlKenhBan; // => đang dùng tạm cho chỗ các sp seller
const String urlPaymentBase = "http://betapayment.pti.com.vn/api/";
const String urlLogin = 'Fs_LOGIN';
const String urlRefrestToken = "Login/Fs_REFRESH_TOKEN";
// Home
const String urlHome = 'bhhd_app/Fs_APP_PRESENTATION';
const String urlProducts = 'bhhd_app/Fs_bh_kenh_lh_nv';
const String urlUtil = 'bhhd_app/Fs_tien_ich_lke_all';
const String urlPromotions = 'bhhd_app/Fs_uu_dai_lke_all';
const String urlNews = 'bhhd_app/Fs_btin_lke_all';

//payment
const String urlPaymentHoiThanhToan = 'Payment/Fs_HOI_THANH_TOAN_PAYMENT';
const String urlPaymentHoiThanhToanVa = 'Payment/Fs_HOI_THANH_TOAN_VA';
//motor 2b
const String urlMotorFormKhoiDong = '2B/Fs_2BGCN_KD';
const String urlMotorTinhPhi = '2B/Fs_KENH_2B_TINH_PHI';
const String urlMotorNhapDon = '2B/Fs_KENH_2BGCN_NHAP';
const String urlMotorChiTiet = '2B/Fs_2BGCN_ID';
//Xe cơ giới
const String urlXeFormKhoiDong = 'XE/Fs_XEGCN_KD';
const String urlXeTinhPhi = 'XE/Fs_XEGCN_TINH';
const String urlXeTinhPhiKM = 'XE/Fs_XEGCN_GGIA_TINH';
const String urlXeNhapDon = 'XE/Fs_XEGCN_NHAP';
const String urlXeChiTiet = 'XE/Fs_XEGCN_ID';
const String urlXeHieuXe = 'XE/Fs_HIEU_XE';
const String urlXeDSDkmr = 'XE/Fs_DS_DKMR';
const String urlXeLayKyPhi = 'XE/Fs_XEGCN_TRA_CHEN';
const String urlXeMaKhaiThac = '/PUB/Fs_DS_MA_KT';
const String urlXeMaKhaiThacTong = '/PUB/Fs_DS_MA_KT_TONG';
const String urlXeMaKhaiThacCap2 = '/PUB/Fs_DS_MA_KT_c2';
const String urlXeLoaiXeMoi = 'XE/Fs_LOAI_XE';
const String urlXeTyLeBoiThuong = 'XE/Fs_TC_TL_BOITHUONG_XCG';
const String urlSendOTP = "app/Fs_SEND_OTP";
const String urlVerifyOTP = "app/Fs_VERIFY_OTP";
const String urlInforCar = "XE/Fs_THONGTIN_XE";
// ocr
const String urlBaseOCR = 'https://apiocr.pti.com.vn/';
const String urlCheckIDPassport = "ocr/recognition";
const String urlCheckOCR = "reg/recognition";
const String ocr = "pub/ocr";
//Certificate
const String urlCertificateFormKhoiDong = 'PUB/Fs_GCN_TIM_KD';
const String urlCertificateDuyet = 'PUB/Fs_GCN_DUYET_NH';
//get Relative Person
const String urlGetRelativePerson = "bhhd_app/Fs_HD_TTIN_NGUOITHAN";
const String urlCertificateFind = 'PUB/Fs_GCN_TIM';
const String urlUpdateInformationPersonal = "bhhd_app/Fs_ttin_ca_nhan_ct_up";
const String urlUpdateAvatar = "bhhd_app/Fs_UPLOAD_AVARTA";
// const String urlUpdateAvatar = "PUB/Fs_UPLOAD_AVARTA";
const String urlRegister = "PUB/Fs_APP_REGISTER";
const String urlRegisterUser = 'PUB/Fs_AGENT_REGISTER';
const String urlSingture = "PUB/Fs_eKYC_FACE_MATCHING";
const String urlCheckReferCode = 'pub/Fs_VERIFY_REFCODE';

// address
const String urlAddress = 'bhhd_app/Fs_KVUC_LKE';

//Person package
const String urlPersonKD = "person/init";
// const String urlNGHKD = "NGUOI/Fs_NGH_KD";
const String urlPersonTinhPhi = "person/premium-insurance";
const String urlPersonNhap = "person/insert";
const String urlPersonDetail = "person/detail";
const String urlPersonCheckID = "NGUOI/Fs_KIEM_TRA_GTRI";

// const String urlPersonKD = "person/init";
// // const String urlNGHKD = "NGUOI/Fs_NGH_KD";
// const String urlPersonTinhPhi = "person/premium-insurance";
// const String urlPersonNhap = "person/insert";
const String urlPersonDetailOld = "NGUOI/Fs_VCOVHD_ID";
// search partner
const String urlHostpital = "bhhd_app/Fs_BH_CNSK_MA_BVIEN_LKE";
const String urlGara = "bhhd_app/Fs_Garage_lke";
const String urlDVTV = "bhhd_app/Fs_donvi_lke";

// compensation
const String urlAddCompensation = "PUB/Fs_CLAIM_PERSON_NH";
const String urlCommissionStatistics = "PUB/Thong_ke_dthu";
//BHN+UT
const String urlPersonKDBHN = "CNG/F_CNGGCN_KD";
const String urlPersonTinhPhiBHN = "CNG/F_CNGGCN_TINH";
const String urlPersonDetailBHN = "DLI/F_DLIHDGOC_CT";
const String urlPersonNhapBHN = "CNG/F_CNGHDGOC_NHAP";

// my product
// const String urlMyProject = "PUB/Fs_PERSON_PRODUCT_LKE";
const String urlMyProject = "bhhd_app/Fs_ds_bh_tham_gia";
//insurance card
const String urlInsuranceCard = 'bhhd_app/Fs_ANH_CT';
const String urlAuthenticationUser = 'HT/Fs_ttin_ca_nhan_hoi';
const String urlUpdatePassword = 'HT/Fs_UPDATE_PASSWORD';
const String urlFindRelative = 'bhhd_app/Fs_timkiem_ngthan';
const String urlResetPass = "PUB/Fs_MANSD_DMK";
const String urlBankinginfor = "bhhd_app/Fs_bank_lke";
const String urlListbankuser = "bhhd_app/Fs_bank_user_lke";
const String urlbankupdate = "bhhd_app/Fs_bank_user";

const String urlListCer = "api/KENHBAN/INBC/F_GRID_MAU_LKE";
const String urlFileCer = "api/KENHBAN/INBC/F_KET_XUAT";
const String urlTemporary = "PUB/Fs_KENH_TLAO_LKE";
const String urlTL = "PUB/Fs_DT_CBQL_LKE";
const String urlStatisHome = "pub/Fs_KENH_DASHBOARD_TKE";
const String urlRevenueGenerated = 'PUB/Fs_KENH_DTHU_LKE';
const String urlHistoryDemnify = 'PUB/Fs_CLAIM_PERSON_LKE';
const String urlRemuneration = "PUB/Fs_THU_LAO_LKE";
const String urlKDThongKe = "PUB/Fs_QU_SL_HOI";
const String urlHospitalContact = "bhhd_app/Fs_BVIEN_LKET_LKE";
// const String urlDetailProductUserBuy = "bhhd_app/Fs_ct_sp_bh_tham_gia";
const String urlDetailProductUserBuy = "bhhd_app/Fs_goi_sp_lke";
const String urlDetailInsuranceUserBuy = "bhhd_app/Fs_ct_sp_bh_tham_gia";
const String urlDetailHistoryProduct = "bhhd_app/Fs_goi_sp_lke";
const String urlDetailProduct = "bhhd_app/Fs_ttin_lh_nv";
const String urlUploadImageCompress = 'PUB/Fs_CLAIM_UPLOAD_PAPERS';
const String urlDeleteImageCompenstation = 'PUB/Fs_KENH_FILES_XOA';
const String urlRateProduct = "bhhd_app/Fs_danhgia_yc_bt";
//house
const String urlHouseFormKhoiDong = 'PHH/Fs_PHHGCN_KD';
const String urlHouseTinhPhi = 'PHH/Fs_PHHGCN_TINH_PHI';
const String urlHouseNhapDon = 'PHH/Fs_PHHGCN_NHAP';
const String urlHouseChiTiet = 'PHH/Fs_PHHGCN_ID';
const String urlHouseDTBHLK = 'PHH/Fs_DT_BH_LKE';
const String urlHouseLHBH = 'PHH/Fs_KENH_LHNV_LKE';
const String urlHouseQTBH = 'PHH/Fs_QT_BH_LKE';
const String urlHouseKtraPhi = 'PHH/Fs_PHHGCN_PHI_KTRA';
const String urlHouseTinhHanVay = 'PHH/Fs_PHHGCN_TINH_HAN_VAY';
const String urlHouseCtBhLK = 'PHH/Fs_CTR_BH_LKE';
// Tinh Quan/Huyen
const String urlHuyen = 'PHH/Fs_MA_HUYEN_LKE';
const String urlTinh = 'PHH/Fs_MA_TINH_LKE';

const String urlTimeLine = "bhhd_app/Fs_timeline_boithuong";
const String urlUploadFile = "PUB/Fs_UPLOAD_FILE";
const String urlGetFileCar = "PUB/Fs_KENH_FILES_LKE";

//Notify
const String urlGetAllNotify = "PUB/Fs_THONGBAO_LKE";
const String urlUpdateNotifyStatus = "PUB/Fs_THONGBAO_XEM";
//yeu cau huy GCN
const String urlRequestCancellation = "PUB/Fs_GCN_YC_HUY";

///TRình phí
const String urlTrinhPhi = 'XE/Fs_DS_TRINH_PHI';
const String urlTrinhPhiHoanThanh = 'XE/Fs_XEPHI_TRINH_LKE';
const String urlGuiTrinhPhi = 'XE/Fs_XEPHI_TRINH_NH';
const String urlDuyetTrinhPhiNhap = 'XE/Fs_XEPHI_TRINH_DUYET';
const String urlGuiTrinhPhiGCN = 'XE/Fs_XEPHI_TRINH_CT_NH';

//Tỉnh huyện:

const String urlTinhLke = 'KHMA/Fs_MA_TINH_LKE';
const String urlHuyenLke = 'KHMA/Fs_MA_HUYEN_LKE';
const String urlXaLke = 'KHMA/Fs_MA_XA_LKE';

// Du lịch
const String urlTourFormKhoiDong = 'DLI/F_DLIGCN_KD';
const String urlTourNhapDon = 'DLI/F_DLIHDGOC_NHAP';
const String urlTourTinhPhi = 'DLI/F_DLIGCN_TINH';
const String urlTourChiTiet = "DLI/F_DLIHDGOC_CT";

//common
const String urlUploadFileFree = 'FileManager/Upload';
const String urlRevenueRemunerationDetail = 'PUB/Fs_KENH_DTTL_CT';
const String urlGetBranch = "PUB/Fs_KENH_DTHU_KD";
