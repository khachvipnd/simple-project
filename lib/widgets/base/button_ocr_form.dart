import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';
import 'package:yourpti/utils/common.dart';

import '../../constants/colors.dart';
import '../../constants/size.dart';

class ButtonOcrForm extends StatefulWidget implements FormObject {
  ButtonOcrForm({Key? key, this.control, this.onChangeCallback});

  OnChangeCallBack? onChangeCallback;
  OnChangeCallBack? onEditingComplete;
  bool isShowCCCD = true;
  bool isShowDK = true;
  bool isShowDKiem = true;
  Color colorText = const Color(0xFFF68632);
  @override
  Control? control;
  TypeOCR? typeOCR;
  @override
  State<ButtonOcrForm> createState() => _ButtonOcrForm();

  @override
  void callReloadView() {}
}

class _ButtonOcrForm extends State<ButtonOcrForm> {
  Control? control;

  @override
  void initState() {
    super.initState();
    control = widget.control;

    if (widget.control!.style.isNotEmpty) {
      widget.colorText =
          HexColor.fromHex(widget.control!.style['textColor'] ?? "");
    }
  }

  @override
  Widget build(BuildContext context) {
    // String widget2 = widget.title;
    return Obx(() => !widget.control!.visible.value
        ? Container(
            child: null,
          )
        : Expanded(
            flex: widget.control!.flex,
            child: InkWell(
                onTap: () {
                  showModalBottomSheet<void>(
                      backgroundColor: Colors.transparent,
                      isScrollControlled: true,
                      isDismissible: true,
                      context: context,
                      builder: (BuildContext context) {
                        return SizedBox(
                          height: 230,
                          child: DialogSelectOptions(
                            callback: ((data) {
                              Navigator.pop(context);
                              widget.typeOCR = data;
                              widget.onChangeCallback!(widget);
                            }),
                            isShowCCCD: widget.control?.isShowCCCD ?? true,
                            isShowDK: widget.control?.isShowDK ?? true,
                            isShowDKiem: widget.control?.isShowDKiem ?? true,
                          ),
                        );
                      });
                },
                child: Padding(
                    padding: EdgeInsets.only(
                        left: widget.control!.indexRow > 0 ? 10 : 16,
                        top: 10,
                        right: widget.control!.isLastRow ? 16 : 0),
                    child: Text(widget.control?.title.value ?? 'Chụp CMT',
                        textAlign: TextAlign.left,
                        style: normalStyle.copyWith(
                          color: widget.colorText,
                          fontSize: fontSize,
                        ))))));

//          Expanded(
//             flex: widget.control!.flex,

//             child: InkWell(
//               onTap: () {

//               },
//               child: Text(
//                 widget.control?.title ?? 'Chụp CMT',
//                 textAlign: TextAlign.left,
//                 style: boldStyle.copyWith(
//                   color: widget.colorText,
//                   fontSize: fontSize,
//                 ),

// // ,        TextStyle(
// //             color: neutral_500,
// //             fontSize: fontSize,
// //             fontWeight: FontWeight.bold,
// //             fontFamily: "Helveticaneue"),
//               ),
//             )));
    // decoration: InputDecoration(;
    //   labelText: widget.control!.title ?? "",
    // ),
    // initialValue: widget.control!.defaultValue,
    // onChanged: (String? value) {
    //   // debugprintDebug('"$value');
    //   control?.value = value!;

    //   if (widget.onChangeCallback != null) widget.onChangeCallback!(this);
    // },
    // onSaved: (String? value) {
    //   // printDebug('"$value"');
    //   // printDebug('"$value"');
    // },
    // onEditingComplete: () {
    //   if (widget.onEditingComplete != null) widget.onEditingComplete!(this);
    // },
    // );
  }
}

class DialogSelectOptions extends StatelessWidget {
  DialogSelectOptions(
      {Key? key,
      this.callback,
      this.isShowCCCD = true,
      this.isShowDK = true,
      this.isShowDKiem = true})
      : super(key: key);
  CallbackData? callback;
  bool isShowCCCD = true;
  bool isShowDK = true;
  bool isShowDKiem = true;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
          color: white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(24), topRight: Radius.circular(24))),
      child: Column(children: [
        const SizedBox(height: 20),
        Text(
          "OCR giấy tờ",
          style: boldStyle.copyWith(),
        ),
        if (isShowCCCD) renderOption(callback, TypeOCR.cccd),
        if (isShowDK) renderOption(callback, TypeOCR.dangKy),
        if (isShowDKiem) renderOption(callback, TypeOCR.DangKiem)
      ]),
    );
  }

  Widget renderOption(callback, TypeOCR type) {
    return InkWell(
      onTap: () {
        callback(type);
      },
      child: Column(
        children: [
          const SizedBox(height: 20),
          Text(type == TypeOCR.cccd
              ? 'CMT/CCCD'
              : (type == TypeOCR.dangKy ? 'Đăng ký' : 'Dăng kiểm')),
        ],
      ),
    );
  }
}
