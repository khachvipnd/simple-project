import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/colors.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/extensions/string_extension.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';
import 'package:yourpti/utils/common.dart';
import 'package:get/get.dart';
import 'package:yourpti/widgets/base/format_money/masked_editing_control.dart';

import '../../constants/size.dart';

class TextFieldForm extends StatefulWidget implements FormObject {
  TextFieldForm(
      {Key? key, this.onChangeCallback, this.control, this.onEditingComplete});

  OnChangeCallBack? onChangeCallback;
  OnChangeCallBack? onEditingComplete;
  TextEditingController? controller;
  @override
  Control? control;

  @override
  State<TextFieldForm> createState() => _TextFieldForm();
  var textValidate = "".obs;
  @override
  void callReloadView() {
    controller!.text = "";
  }
}

class _TextFieldForm extends State<TextFieldForm> {
  List<TextInputFormatter> _inputFormatters = [];

  var focusNode = FocusNode();
  String messageInvalid = "";
  @override
  void initState() {
    super.initState();
    try {
      _inputFormatters = [];
      focusNode.addListener(() {
        // printDebug(focusNode.hasFocus);
        if (!focusNode.hasFocus) {
          if (widget.onEditingComplete != null) {
            widget.onEditingComplete!(widget);
          }
          print('Offforcus');
          if (widget.control!.formatShow == FormatValuePresent.Money) {
            if (widget.controller?.text == '') {
              widget.controller?.text = "0";
            }
          }
        } else if (focusNode.hasFocus) {
          print('Onforcus');
          if (widget.control!.formatShow == FormatValuePresent.Money) {
            if (widget.controller?.text == "0") {
              widget.controller?.text = "";
            }
          }
        }
      });

      widget.controller =
          TextEditingController(text: widget.control!.value.value);

      switch (widget.control!.formatShow) {
        case FormatValuePresent.Money:
          widget.controller = MaskedTextController(
              text: widget
                  .control!.value.value); // lưu ý value phải là số nhé. sml.
          break;
        case FormatValuePresent.Normal:
          break;
        case FormatValuePresent.Percent:
          break;
        case FormatValuePresent.Free:
          break;
      }

      // widget.controller!.addListener(() {
      //   final String text = widget.controller!.text.toLowerCase();
      //   widget.controller!.value = widget.controller!.value.copyWith(
      //     text: text,
      //     selection:
      //         TextSelection(baseOffset: text.length, extentOffset: text.length),
      //     composing: TextRange.empty,
      //   );
      // });
      if (widget.control!.keyType == TextInputType.number) {
        if (widget.control?.isDouble == true) {
          widget.controller =
              NumberTextController(text: widget.control!.value.value);
        }

        _inputFormatters
            .add(FilteringTextInputFormatter.allow(RegExp('[0-9.,]')));
        // _inputFormatters
        //     .add(FilteringTextInputFormatter.deny(',', replacementString: '.'));
      }
      if (widget.control!.limitLength != null) {
        _inputFormatters
            .add(LengthLimitingTextInputFormatter(widget.control!.limitLength));
      }

      if (widget.control!.keyType == TextInputType.phone) {
        _inputFormatters
            .add(FilteringTextInputFormatter.allow(RegExp(r"\d+([\.]\d+)?")));
        _inputFormatters.add(FilteringTextInputFormatter.digitsOnly);
      }
      if (widget.control!.formatShow == FormatValuePresent.Percent) {
        _inputFormatters = [];

        _inputFormatters
            .add(FilteringTextInputFormatter.deny(',', replacementString: '.'));
        _inputFormatters.add(
            FilteringTextInputFormatter.allow(RegExp(r'(^\d*\.?\d{0,2})')));
      }
    } catch (e) {
      // printDebug(e);
      print('TextFieldForm');
      print(e);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didUpdateWidget(covariant TextFieldForm oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => !widget.control!.visible.value
        ? Container(
            child: null,
          )
        : Expanded(
            flex: widget.control!.flex,
            child: Padding(
              padding: EdgeInsets.only(
                  left: widget.control!.indexRow > 0 ? 10 : 16,
                  top: 10,
                  right: widget.control!.isLastRow ? 16 : 0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // titleView(
                  //     title: widget.control!.title,
                  //     isRequail: widget.control!.required),
                  // const SizedBox(
                  //   height: 6,
                  // ),
                  inputView(),
                  if (widget.textValidate.value.isNotEmpty)
                    const SizedBox(
                      height: 6,
                    ),
                  Obx(() => Text(
                        widget.textValidate.value,
                        style: normalStyleValidate.copyWith(
                            color: red_600, fontSize: 11),
                      ))
                ],
              ),
            )));
  }

  Widget inputView() {
    return Obx(() => SizedBox(
          height: 50,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color:
                  widget.control!.enabled.value ? grayconst_100 : secondary_300,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  width: 10,
                ),
                // if (widget.icon != null)
                //   // SvgPicture.asset(
                //   //   widget.icon ?? "",
                //   //   height: 18,
                //   //   width: 18,
                //   // ),
                // if (widget.icon != null)
                //   const SizedBox(
                //     width: 10,
                //   ),
                Expanded(
                    child: Center(
                  child: TextFormField(
                    // key: UniqueKey(),
                    controller: widget.controller,
                    // initialValue: widget.content,
                    // maxLines: widget.obscureText ? null : widget.maxLines,
                    inputFormatters: _inputFormatters,
                    keyboardType:
                        widget.control!.keyType == TextInputType.number
                            ? const TextInputType.numberWithOptions(
                                decimal: true, signed: true)
                            : widget.control!.keyType,
                    style: normalStyle.copyWith(
                      fontSize: fontSizeNomal,
                      color: neutral_500,
                      fontWeight: FontWeight.w400,
                    ),
                    focusNode: focusNode,
                    onEditingComplete: () {
                      // printDebug("onEditingComplete");
                    },
                    onFieldSubmitted: (value) {
                      FocusScope.of(context).unfocus();
                      // printDebug('onFieldSubmitted: ${value}');
                    },
                    onChanged: (value) {
                      // printDebug(widget.control!.formatShow);
                      // printDebug(widget.controller);
                      widget.control?.value.value = value;

                      if (widget.onChangeCallback != null) {
                        widget.onChangeCallback!(widget);
                      }
                      widget.textValidate.value = "";
                    },
                    validator: (value) {
                      // if (value!.isEmpty) {
                      //   textValidate.value = widget.textValidate ?? "";
                      // }
                      return onValidate(value);
                    },
                    // ignore: unrelated_type_equality_checks
                    textAlign: widget.control!.keyType == KeyboardType.Number
                        ? TextAlign.right
                        : TextAlign.left,
                    decoration: InputDecoration(
                        isDense: true,
                        enabled: widget.control!.enabled.value,
                        label: titleView(
                            title: (widget.control?.title.value ?? "").tr,
                            isRequail: widget.control!.required,
                            titleFather: widget.control!.title.value),
                        // labelText: (widget.title ?? "").tr,
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        contentPadding: const EdgeInsets.only(left: 5),
                        border: InputBorder.none,
                        labelStyle: normalStyle.copyWith(
                            color: neutral_400, fontSize: fontSize16),
                        focusColor: neutral_400,
                        errorMaxLines: 1,
                        errorStyle: const TextStyle(
                            fontSize: 0, height: 0.8, color: red_500),
                        errorBorder: InputBorder.none),
                  ),
                ))
                // const Icon(Icons.visibility_rounded, color: unselectedTextColor)

                // if (widget.iconAction != null)
                //   InkWell(
                //     onTap: () {
                //       if (widget.callbackAction != null) {
                //         widget.callbackAction;
                //       }
                //     },
                //     child: SvgPicture.asset(
                //       widget.iconAction ?? "",
                //       height: 18,
                //       width: 18,
                //     ),
                //   ),

                // if (widget.iconAction != null) const SizedBox(width: 10)
              ],
            ),
          ),
        ));
  }

  // Widget inputView() {
  //   return Obx(() => TextFormField(
  //         controller: widget.controller,
  //         inputFormatters: _inputFormatters,
  //         keyboardType: widget.control!.keyType,
  //         style: const TextStyle(
  //           fontSize: fontSizeNomal,
  //           fontFamily: "Helveticaneue",
  //           color: neutral_500,
  //           // height: 10,
  //           fontWeight: FontWeight.w400,
  //         ),

  //         decoration: InputDecoration(
  //             filled: true,
  //             enabled: widget.control!.enabled.value,
  //             contentPadding: const EdgeInsets.only(
  //               top: 11,
  //               bottom: 11,
  //               left: 12,
  //             ),
  //             // prefix: const Padding(padding: EdgeInsets.only(left: 0)),
  //             fillColor:
  //                 widget.control!.enabled.value ? grayconst_100 : secondary_300,
  //             isCollapsed: true,
  //             border: OutlineInputBorder(
  //                 borderRadius: BorderRadius.circular(8.0),
  //                 borderSide: const BorderSide(
  //                     width: 0,
  //                     // color: grayconst_100,
  //                     style: BorderStyle.none)),
  //             errorMaxLines: 1,
  //             errorStyle:
  //                 const TextStyle(fontSize: 11, height: 0.8, color: red_500),
  //             hintText: widget.control!.title),

  //         validator: (value) {
  //           return onValidate(value);
  //         },
  //         onFieldSubmitted: (value) {
  //           printDebug(value);
  //         },
  //         onSaved: (String? value) {
  //           printDebug(value);
  //         },
  //         onChanged: (String? value) {
  //           // debugprintDebug('"$value');
  //           widget.control?.value.value = value!;

  //           if (widget.onChangeCallback != null)
  //             widget.onChangeCallback!(widget);
  //         },

  //         textAlign: widget.control!.keyType == KeyboardType.Number
  //             ? TextAlign.right
  //             : TextAlign.left,
  //         onEditingComplete: () {
  //           printDebug("object");
  //         },
  //         // onEditingComplete: () {
  //         //   if (widget.onEditingComplete != null) widget.onEditingComplete!(widget);
  //         // },
  //       ));

  //   // return TextFormField(
  //   //   controller: widget.controller,
  //   //   // initialValue: widget.content,
  //   //   // maxLines: widget.obscureText ? null : widget.maxLines,
  //   //   keyboardType: control!.keyType,
  //   //   style: const TextStyle(
  //   //     fontSize: fontSizeNomal,
  //   //     fontFamily: "Helveticaneue",
  //   //     color: neutral_500,
  //   //     fontWeight: FontWeight.w400,
  //   //   ),
  //   //   onChanged: (value) {
  //   //     if (widget.onChangeCallback != null) {
  //   //       widget.onChangeCallback!(widget);
  //   //     }
  //   //   },
  //   //   validator: (value) {
  //   //     if (control!.notValidate) {
  //   //       //-> những thằng nào k bắt buộc validate thì thêm thằng này = true
  //   //       return null;
  //   //     }

  //   //     if (control!.required && value == "") {
  //   //       // setState(() {
  //   //       //   messageInvalid = "invalid_noti".tr;
  //   //       // });
  //   //       return "invalid_noti".tr;
  //   //     }
  //   //     //Validate Số điện thoại
  //   //     if (control!.id == "so_dt" ||
  //   //         control!.id == "SO_DT" ||
  //   //         control!.id == "phone" ||
  //   //         control!.id == "PHONE") {
  //   //       if (control!.value.length < 9 || control!.value.length > 10) {
  //   //         // setState(() {
  //   //         //   messageInvalid = "invalid_noti".tr;
  //   //         // });
  //   //         return "invalid_noti".tr;
  //   //       }
  //   //     }
  //   //     //Validate Email
  //   //     if (control!.id == "email" || control!.id == "EMAIL") {}
  //   //     //Validate số CMT/CCCD
  //   //     if (control!.id == "ma_thue" || control!.id == "cmt") {}

  //   //     //Validate biển xe
  //   //     if (control!.id == "bien_xe" || control!.id == "BIEN_XE") {}

  //   //     return null;
  //   //   },
  //   //   decoration: InputDecoration(
  //   //       filled: true,
  //   //       contentPadding: EdgeInsets.only(top: 10, bottom: 10, left: 16),
  //   //       fillColor: grayconst_100,
  //   //       isCollapsed: true,
  //   //       border: OutlineInputBorder(
  //   //           borderRadius: BorderRadius.circular(8.0),
  //   //           borderSide: const BorderSide(
  //   //               width: 0,
  //   //               // color: grayconst_100,
  //   //               style: BorderStyle.none)),
  //   //       errorStyle: const TextStyle(fontSize: 11, height: 0.3),
  //   //       hintText: control!.title),

  //   //   // ),
  //   // );
  // }

  Widget titleView(
      {required String title, String? titleFather, bool isRequail = false}) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          // if (titleFather != null)
          //   TextSpan(text: ' $titleFather', style: boldStyle),
          TextSpan(
              text: title,
              style: normalStyle.copyWith(
                color: neutral_500,
                fontWeight: FontWeight.w400,
              )),
          TextSpan(
              text: isRequail ? '* ' : ' ',
              style: normalStyle.copyWith(
                color: red_600,
                fontWeight: FontWeight.w500,
              )),
        ],
      ),
    );
  }

  String? onValidate(value) {
    widget.textValidate.value = "";
    if (widget.control!.notValidate) {
      //-> những thằng nào k bắt buộc validate thì thêm thằng này = true
      return null;
    }

    if (widget.control!.required && value == "") {
      widget.textValidate.value = "invalid_noti".tr;
      return widget.textValidate.value;
    }
    //Validate Số điện thoại
    if (widget.control!.keyType == TextInputType.phone && value != "") {
      if (!value.toString().isValidPhone()) {
        widget.textValidate.value = "Sai định dạng số điện thoại".tr;
        return widget.textValidate.value;
        //   return
      }
    }
    // if (widget.control!.id == "so_dt" ||
    //     widget.control!.id == "SO_DT" ||
    //     widget.control!.id == "phone" ||
    //     widget.control!.id == "PHONE") {
    //   // if (widget.control!.value.value.length < 9 ||
    //   //     widget.control!.value.value.length > 10) {
    //   if (!value.toString().isValidPhone()) {
    //     textValidate.value = "invalid_noti".tr;
    //     return textValidate.value;
    //     //   return
    //   }
    // }
    //Validate Email
    if (widget.control!.keyType == TextInputType.emailAddress && value != "") {
      if (!value.toString().isValidEmail()) {
        widget.textValidate.value = "emailnotempty".tr;
        return "";
      }
    }
    // if (widget.control!.id == "email" || widget.control!.id == "EMAIL") {
    //   if (!value.toString().isValidEmail()) {
    //     textValidate.value = "emailnotempty".tr;
    //     return "";
    //   }
    // }
    //Validate số CMT/CCCD
    if (widget.control!.id == "ma_thue" || widget.control!.id == "cmt") {}

    //Validate biển xe
    if (widget.control!.id == "bien_xe" || widget.control!.id == "BIEN_XE") {}

    return null;
  }
}
