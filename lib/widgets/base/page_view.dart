// import 'package:flutter/material.dart';
// import 'package:yourpti/constants/colors.dart';
// import 'package:yourpti/constants/styles.dart';

// import '../../constants/callbacks.dart';
// import '../../controllers/searching_partner/searching_partner_view.dart';

// class PageViewCustom extends StatefulWidget {
//   PageViewCustom(
//       {Key? key,
//       required this.scrollController,
//       this.callback,
//       required this.lsTab,
//       this.colorTabSelected,
//       this.colorTab,
//       required this.contents,
//       required this.isLine})
//       : super(key: key);

//   ScrollController? scrollController;
//   CallbackData? callback;
//   List<TabObject> lsTab;
//   Color? colorTabSelected;
//   Color? colorTab;
//   bool isLine = false;
//   List<Widget> contents = [];
//   @override
//   State<StatefulWidget> createState() => _PageViewCustom();
// }

// class _PageViewCustom extends State<PageViewCustom> {
//   // _PageViewCustom(
//   //     {Key? key,
//   //     })
//   //     : super(key: key);

//   PageController controller = PageController();
//   @override
//   void dispose() {
//     controller.dispose();
//     super.dispose();
//   }

//   @override
//   void initState() {
//     super.initState();

//     controller.addListener(() {});
//   }

//   TabObject updateTabSelected(int indexTabSelected) {
//     // List<TabObject> ls = widget.lsTab;
//     TabObject tabSelected = widget.lsTab.first;
//     for (var i = 0; i < widget.lsTab.length; i++) {
//       if (i == indexTabSelected) {
//         widget.lsTab[i].selected = true;
//       } else {
//         widget.lsTab[i].selected = false;
//       }
//     }

//     return tabSelected;
//   }

//   Widget renderItemTab(TabObject data) {
//     // List<Widget> lsChild = [];
//     return InkWell(
//         onTap: () {
//           TabObject tabSelected = updateTabSelected(widget.lsTab.indexOf(data));
//           // TabObject tabSelected = ls.first;

//           // widget.lsTab = ls;
//           if (controller.hasClients) {
//             controller.animateToPage(
//               widget.lsTab.indexOf(tabSelected),
//               duration: const Duration(milliseconds: 400),
//               curve: Curves.easeInOut,
//             );
//           }
//           if (widget.callback != null) {
//             widget.callback!(widget.lsTab);
//           }
//           setState(() {});
//         },
//         child: Padding(
//           padding: const EdgeInsets.only(right: 8),
//           child: Container(
//               decoration: BoxDecoration(
//                   color: !widget.isLine && data.selected
//                       ? widget.colorTabSelected ?? primary
//                       : widget.colorTab ?? Colors.white,
//                   borderRadius: BorderRadius.circular(20)),
//               child: Column(
//                 children: [
//                   Padding(
//                       padding: const EdgeInsets.symmetric(
//                           horizontal: 5, vertical: 0),
//                       child: Center(child: LayoutBuilder(
//                         builder:
//                             (BuildContext context, BoxConstraints constraints) {
//                           // printDebug(constraints.widthConstraints());

//                           return Column(children: [
//                             Text(
//                               data.title,
//                               style: normalStyle.copyWith(
//                                   fontWeight: FontWeight.w400, color: primary),
//                             ),
//                             Container(
//                                 // margin: EdgeInsets.symmetric(horizontal: 10),
//                                 // padding: EdgeInsets.symmetric(
//                                 //     vertical: 10, horizontal: 100),
//                                 height: 3,
//                                 // width: MediaQuery.of(context).size.width,
//                                 width: 100,
//                                 // constraints: const BoxConstraints(
//                                 //   minWidth: 100,
//                                 //   maxHeight: 3,
//                                 // ),
//                                 // width: double.infinity,
//                                 decoration: BoxDecoration(
//                                     color: primary,
//                                     borderRadius: BorderRadius.circular(3)))
//                           ]);
//                         },
//                       ))),
//                 ],
//               )),
//         ));

//     // return lsChild;
//   }

//   @override
//   Widget build(BuildContext context) {
//     final _kTabPages = <Widget>[
//       const Center(
//         child: Text('Tab 1'),
//       ),
//       const Center(
//         child: Text('Tab 2'),
//       ),
//       const Center(
//         child: Text('Tab 3'),
//       )
//       // Center(child: Icon(Icons.search, size: 64.0, color: Colors.teal)),
//       // Center(child: Icon(Icons.search, size: 64.0, color: Colors.cyan)),
//       // Center(child: Icon(Icons.search, size: 64.0, color: Colors.blue)),
//       // Center(child: Icon(Icons.search, size: 64.0, color: Colors.blue)),
//       // Center(child: Icon(Icons.search, size: 64.0, color: Colors.blue)),
//       // Center(child: Icon(Icons.search, size: 64.0, color: Colors.blue)),
//       // Center(child: Icon(Icons.search, size: 64.0, color: Colors.blue)),
//       // Center(child: Icon(Icons.search, size: 64.0, color: Colors.blue)),
//     ];
//     final _kTabs = <Tab>[
//       Tab(
//           child:
//               Text('Thông tin', style: normalStyle.copyWith(color: primary))),
//       Tab(
//           child: Text('Gói bảo hiểm',
//               style: normalStyle.copyWith(color: primary))),
//       Tab(
//           child: Text('Lịch sử mua bảo hiểm',
//               style: normalStyle.copyWith(color: primary))),
//       // Tab(child: Text('DK', style: normalStyle.copyWith(color: primary))),
//       // Tab(child: Text('DK', style: normalStyle.copyWith(color: primary))),
//       // Tab(child: Text('DK', style: normalStyle.copyWith(color: primary))),
//       // Tab(child: Text('DK', style: normalStyle.copyWith(color: primary))),
//       // Tab(child: Text('DK', style: normalStyle.copyWith(color: primary))),
//     ];

//     return DefaultTabController(
//         length: _kTabs.length,
//         child: Scaffold(
//             appBar: AppBar(
//               shadowColor: Colors.transparent,
//               centerTitle: true,
//               // leading: Icon(Icons.person_outline),
//               toolbarHeight: 20,
//               bottom: PreferredSize(
//                   child: TabBar(
//                       isScrollable: true,
//                       unselectedLabelColor: Colors.white.withOpacity(0.3),
//                       // indicatorColor: primary,
//                       indicator: const UnderlineTabIndicator(
//                         borderSide: BorderSide(
//                             color: primary, width: 2), // Indicator height
//                         insets: EdgeInsets.symmetric(
//                             horizontal: 10), // Indicator width
//                       ),
//                       tabs: _kTabs),
//                   preferredSize: const Size.fromHeight(30.0)),
//               // actions: <Widget>[
//               //   Padding(
//               //     padding: const EdgeInsets.only(right: 16.0),
//               //     child: Icon(Icons.add_alert),
//               //   ),
//               // ],
//             ),
//             body: TabBarView(children: _kTabPages))
//         // child: Scaffold(
//         //   appBar: AppBar(
//         //     //  title: Text(widget.title),
//         //     // actions: <Widget>[
//         //     //   IconButton(
//         //     //     icon: Icon(Icons.search),
//         //     //     onPressed: () {},
//         //     //   ),
//         //     //   IconButton(
//         //     //     icon: Icon(Icons.account_circle),
//         //     //     onPressed: () {},
//         //     //   ),
//         //     // ],
//         //     bottom: TabBar(
//         //       isScrollable: true,
//         //       tabs: _kTabs,
//         //     ),
//         //   ),
//         //   body: TabBarView(
//         //     children: _kTabPages,
//         //   ),
//         // ),
//         );

//     // return Column(
//     //   children: [
//     //     Padding(
//     //         padding: const EdgeInsets.only(left: 16),
//     //         child: SingleChildScrollView(
//     //             controller: widget.scrollController,
//     //             scrollDirection: Axis.horizontal,
//     //             child: Expanded(
//     //               child: Row(
//     //                   crossAxisAlignment: CrossAxisAlignment.start,
//     //                   mainAxisAlignment: MainAxisAlignment.start,
//     //                   children: [
//     //                     ...List.generate(
//     //                       widget.lsTab.length,
//     //                       (index) {
//     //                         printDebug(index);
//     //                         return renderItemTab(widget.lsTab[index]);
//     //                         // here by default width and height is 0
//     //                         // return Container();
//     //                       },
//     //                     )
//     //                   ]),
//     //             ))),
//     //     Expanded(
//     //         child: PageView(
//     //             onPageChanged: (int index) {
//     //               printDebug(index);
//     //               if (widget.lsTab.length == widget.contents.length) {
//     //                 updateTabSelected(index);
//     //                 setState(() {});
//     //               }
//     //             },
//     //             controller: controller,
//     //             children: widget.contents))
//     //   ],
//     // );
//   }
// }
