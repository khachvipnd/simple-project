import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/styles.dart';

import '../../constants/colors.dart';
import '../../models/ControlObject.dart';

class KyPhiForm extends StatelessWidget {
  KyPhiForm({Key? key, this.control}) : super(key: key);

  Control? control;

  List<Widget> renderDsTra() {
    return control!.listData.map(
      (e) {
        return SizedBox(
          height: 40,
          child: Row(
            children: [
              Expanded(flex: 1, child: Center(child: Text(e['NGAY']))),
              Expanded(flex: 1, child: Center(child: Text(e['TIEN'])))
            ],
          ),
        );
      },
    ).toList();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Obx(() => !control!.visible.value
        ? Container(
            child: null,
          )
        : Padding(
            padding: EdgeInsets.only(
                left: control!.indexRow > 0 ? 10 : 16,
                top: 10,
                right: control!.isLastRow ? 16 : 0),
            child: Container(
              // height: 100,
              width: Get.width - 32,
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(
                  color: gray_200, borderRadius: BorderRadius.circular(10)),
              child: Column(
                // axis:Axis.vertical,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 30,
                    width: Get.width - 32,
                    child: Center(
                      child: Text(
                        control?.title.value ?? "",
                        style: normalStyle.copyWith(color: white),
                      ),
                    ),
                    decoration: BoxDecoration(color: primary_500),
                  ),
                  Row(children: [
                    Expanded(flex: 1, child: Center(child: Text('Date'.tr))),
                    Expanded(flex: 1, child: Center(child: Text('money'.tr)))
                  ]),
                  ...renderDsTra(),
                  SizedBox(
                    height: 10,
                  )
                ],
              ),
            )));
  }
}
