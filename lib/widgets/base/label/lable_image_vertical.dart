import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:yourpti/constants/icons.dart';
import 'package:yourpti/constants/styles.dart';

class LableImageVertical extends StatelessWidget {
  String? title;
  double foneSize;
  Alignment alignment = Alignment.center;
  TextStyle styleTitle = normalStyle;
  TextAlign textAlign = TextAlign.center;
  bool? imagenetwork;
  String? imageUrl;
  LableImageVertical(
      {Key? key,
      this.title,
      this.foneSize = fontSizeNomal,
      this.styleTitle = normalStyle,
      this.alignment = Alignment.center,
      this.imagenetwork = false,
      this.imageUrl = ""})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return inforItemNotRating(title: title ?? "");
  }

  Widget inforItemNotRating({String? title}) {
    return Column(
      children: [
        imagenetwork == false
            ? Image.asset(
                iconCamera2,
                width: 48,
                height: 48,
              )
            : ClipRRect(
                borderRadius: BorderRadius.circular(48),
                child: CachedNetworkImage(
                  imageUrl: imageUrl ?? "",
                  width: 48,
                  height: 48,
                  fit: BoxFit.fill,
                )),
        Text((title ?? "").tr, style: styleTitle, textAlign: textAlign),
      ],
    );
  }
}
