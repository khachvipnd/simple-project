import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:yourpti/constants/styles.dart';

enum TypeView { title, click, child, button, nomarl }

class LableView extends StatelessWidget {
  TypeView typeView = TypeView.title;
  String? content;
  String? title;
  double foneSize;
  Widget? item;
  Alignment alignment = Alignment.centerRight;
  TextStyle styleTitle = normalStyle;
  TextStyle styleContent = normalStyle;
  TextAlign textAlign = TextAlign.end;
  int flexTitle = 3;
  int flex = 7;
  int? maxLines;
  int itemFlex = 2;
  LableView(
      {Key? key,
      this.content,
      this.title,
      this.foneSize = fontSizeNomal,
      this.item,
      this.styleContent = normalStyle,
      this.styleTitle = normalStyle,
      this.textAlign = TextAlign.end,
      this.flexTitle = 3,
      this.flex = 7,
      this.itemFlex = 2,
      this.maxLines,
      this.alignment = Alignment.centerRight,
      this.typeView = TypeView.title})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    switch (typeView) {
      case TypeView.nomarl:
        return inforItemNomarl(title: title, content: content);
      case TypeView.title:
        return inforItemNotRating(content: content ?? "", title: title ?? "");

      case TypeView.button:
        return inforItemButton(
            content: content ?? "", title: title ?? "", item: item);
      case TypeView.click:
        if (item == null) {
          return inforItemClick(content: content ?? "", title: title ?? "");
        }
        return inforItemClickPhone(
            content: content ?? "", title: title ?? "", item: item);

      default:
        return inforItemNotRating(content: content ?? "", title: title ?? "");
    }
  }

  Widget inforItem({String? content, String? title, Widget? item}) {
    return Row(
      children: [
        Expanded(
            flex: flexTitle, child: Text((title ?? "").tr, style: styleTitle)),
        const SizedBox(
          width: 3,
        ),
        Expanded(
            flex: flex - flexTitle,
            child: Align(
              alignment: alignment,
              child: Text(
                content ?? "",
                style: styleContent,
                textAlign: textAlign,
              ),
            )),
        item == null
            ? Container()
            : Expanded(
                flex: itemFlex,
                child: Align(
                  alignment: alignment,
                  child: item,
                ))
      ],
    );
  }

  Widget inforItemButton({String? content, String? title, Widget? item}) {
    return Row(
      children: [
        Expanded(
            flex: flexTitle, child: Text((title ?? "").tr, style: styleTitle)),
        const SizedBox(
          width: 3,
        ),
        Expanded(
            flex: flex - flexTitle,
            child: Align(
              alignment: alignment,
              child: Text(
                content ?? "",
                maxLines: maxLines,
                style: styleContent,
                textAlign: textAlign,
              ),
            )),
        item == null
            ? Container()
            : Expanded(
                flex: itemFlex,
                child: Align(
                  alignment: alignment,
                  child: item,
                ))
      ],
    );
  }

  Widget inforItemNomarl({String? content, String? title}) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text((title ?? "").tr + ":", style: styleTitle),
        const SizedBox(
          width: 3,
        ),
        Expanded(
            child: Align(
          alignment: alignment,
          child: Text(
            content ?? "",
            maxLines: maxLines,
            style: styleContent,
            textAlign: textAlign,
          ),
        )),
      ],
    );
  }

  Widget inforItemNotRating({String? content, String? title}) {
    return Row(
      children: [
        Expanded(
            flex: flexTitle,
            child: Text((title ?? "").tr + ":", style: styleTitle)),
        const SizedBox(
          width: 3,
        ),
        Expanded(
            flex: flex - flexTitle,
            child: Align(
              alignment: alignment,
              child: Text(
                content ?? "",
                maxLines: maxLines,
                style: styleContent,
                textAlign: textAlign,
              ),
            )),
      ],
    );
  }

  Widget inforItemClick({String? content, String? title}) {
    return Row(
      children: [
        Expanded(
            flex: flexTitle,
            child: Text((title ?? "").tr + ":", style: styleTitle)),
        const SizedBox(
          width: 3,
        ),
        Expanded(
            flex: flex - flexTitle,
            child: GestureDetector(
              onTap: () {
                final phone = content;
                launch("tel://$phone");
              },
              child: Text(
                content ?? "",
                maxLines: maxLines,
                style: styleContent,
                textAlign: textAlign,
              ),
            )),
      ],
    );
  }

  Widget inforItemClickPhone({String? content, String? title, Widget? item}) {
    return Row(
      children: [
        Expanded(
            flex: flexTitle,
            child: Text((title ?? "").tr + ":", style: styleTitle)),
        const SizedBox(
          width: 3,
        ),
        Expanded(
          flex: 3,
          child: GestureDetector(
              onTap: () {
                final phone = content;
                launch("tel://$phone");
              },
              child: Text(
                content ?? "",
                maxLines: maxLines,
                style: styleContent,
                textAlign: textAlign,
              )),
        ),
        item == null ? Container() : Expanded(flex: itemFlex, child: item)
      ],
    );
  }
}
