import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yourpti/constants/styles.dart';

class LableImageView extends StatelessWidget {
  Color color;
  String? content;
  double foneSize;
  String item;
  double? width;
  double? height;
  Alignment alignment = Alignment.centerLeft;
  TextStyle styleTitle = normalStyle;
  TextStyle styleContent = normalStyle;

  LableImageView(
      {this.content,
      this.foneSize = fontSizeNomal,
      required this.item,
      required this.color,
      this.styleContent = normalStyle,
      this.width = 20.0,
      this.height = 20.0});

  @override
  Widget build(BuildContext context) {
    return inforItemNotRating(content: content ?? "", item: item, color: color);
  }

  Widget inforItemNotRating(
      {String? content, required String item, required Color color}) {
    return Row(
      children: [
        SvgPicture.asset(
          item,
          width: width,
          height: height,
          color: this.color,
        ),
        const SizedBox(
          width: 9.5,
        ),
        Expanded(
            child: Align(
          alignment: alignment,
          child: Text(
            content ?? "",
            style: styleContent,
          ),
        )),
      ],
    );
  }
}
