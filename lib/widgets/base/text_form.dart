import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';

import '../../constants/colors.dart';
import '../../constants/size.dart';

class TextForm extends StatefulWidget implements FormObject {
  TextForm({Key? key, this.onChangeCallback, this.control});

  OnChangeCallBack? onChangeCallback;
  OnChangeCallBack? onEditingComplete;

  @override
  Control? control;

  @override
  State<TextForm> createState() => _TextForm();

  @override
  void callReloadView() {}
}

class _TextForm extends State<TextForm> {
  Control? control;
  TextAlign textAlign = TextAlign.left;
  TextStyle textStyle = boldStyle.copyWith(
    color: neutral_500,
    fontSize: fontSize,
  );
  @override
  void initState() {
    super.initState();
    control = widget.control;
    if (control?.style != null) {
      textAlign = control?.style['textAlign'] == 'right'
          ? TextAlign.right
          : TextAlign.left;

      textStyle = boldStyle.copyWith(
        color: control?.style['textColor'] != null
            ? HexColor.fromHex(control?.style['textColor'])
            : neutral_500,
        fontSize: control?.style['fontSize'] != null
            ? control?.style['fontSize'].toDouble()
            : fontSize,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    // String widget2 = widget.title;
    return Obx(() => !widget.control!.visible.value
        ? Container(
            child: null,
          )
        : Expanded(
            flex: widget.control!.flex,
            child: Padding(
              padding: EdgeInsets.only(
                  left: widget.control!.indexRow > 0 ? 10 : 16,
                  top: 10,
                  right: widget.control!.isLastRow ? 16 : 0),
              child: Text(
                widget.control!.title.value,
                textAlign: textAlign,
                style: textStyle,
// ,        TextStyle(
//             color: neutral_500,
//             fontSize: fontSize,
//             fontWeight: FontWeight.bold,
//             fontFamily: "Helveticaneue"),
              ),
            )));
    // decoration: InputDecoration(;
    //   labelText: widget.control!.title ?? "",
    // ),
    // initialValue: widget.control!.defaultValue,
    // onChanged: (String? value) {
    //   // debugprintDebug('"$value');
    //   control?.value = value!;

    //   if (widget.onChangeCallback != null) widget.onChangeCallback!(this);
    // },
    // onSaved: (String? value) {
    //   // printDebug('"$value"');
    //   // printDebug('"$value"');
    // },
    // onEditingComplete: () {
    //   if (widget.onEditingComplete != null) widget.onEditingComplete!(this);
    // },
    // );
  }
}
