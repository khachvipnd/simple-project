import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';

import '../../constants/colors.dart';

class LineForm extends StatefulWidget implements FormObject {
  LineForm({Key? key, this.control});

  @override
  Control? control;

  @override
  State<LineForm> createState() => _LineForm();

  @override
  void callReloadView() {}
}

class _LineForm extends State<LineForm> {
  Control? control;

  @override
  void initState() {
    super.initState();
    control = widget.control;
  }

  @override
  Widget build(BuildContext context) {
    // String widget2 = widget.title;
    return Obx(() => !widget.control!.visible.value
        ? Container(
            child: null,
          )
        : Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Container(
              height: 1,
              width: Get.width - 32,
              decoration: const BoxDecoration(color: neutral_300),
            )));
  }
}
