import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/styles.dart';

import '../../constants/colors.dart';
import '../../constants/icons.dart';
import '../../constants/size.dart';

class CheckBoxWidget extends StatefulWidget {
  CheckBoxWidget(
      {Key? key, this.title = "", this.isCheck = false, this.callback})
      : super(key: key);
  String title = "";
  bool isCheck = false;
  CallbackActionBool? callback;
  @override
  State<CheckBoxWidget> createState() => _CheckBoxWidgetState();
}

class _CheckBoxWidgetState extends State<CheckBoxWidget> {
  var isCheck = false;
  @override
  void initState() {
    super.initState();
    isCheck = widget.isCheck;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        isCheck = !isCheck;
        setState(() {});
        if (widget.callback != null) {
          widget.callback!(isCheck);
        }
      },
      child: SizedBox(
        height: 40,
        child: Row(
          children: [
            isCheck == true
                ? SvgPicture.asset(iconCheckboxSelected)
                : SvgPicture.asset(iconCheckbox),
            const SizedBox(
              width: 6,
            ),
            Text(widget.title.tr,
                textAlign: TextAlign.center,
                style: normalStyle.copyWith(
                    fontSize: fontSize,
                    color: neutral_500,
                    fontWeight: FontWeight.w400))
          ],
        ),
      ),
    );
  }
}
