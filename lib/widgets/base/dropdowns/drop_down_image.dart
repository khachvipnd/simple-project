import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/colors.dart';
import 'package:yourpti/constants/icons.dart';
import 'package:yourpti/constants/styles.dart';

import 'drop_list_model.dart';

class DropDownImageView extends StatefulWidget {
  DropDownImageView(
      {Key? key, this.options, this.selectImage, this.hint = "", this.callback})
      : super(key: key);
  List<OptionItem>? options = [];
  OptionItem? selectImage;
  String hint = "";
  CallbackActionOptionItem? callback;
  @override
  State<DropDownImageView> createState() => _DropDownImageViewState();
}

class _DropDownImageViewState extends State<DropDownImageView> {
  List<OptionItem> options = [];
  late OptionItem selectImage;
  @override
  void initState() {
    super.initState();
    options = widget.options ?? [];
    for (var element in options) {
      if (element.isSelect) {
        selectImage = element;
      }
    }
    if (widget.selectImage != null) {
      selectImage = widget.selectImage!;
    }
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
        child: DropdownButton<OptionItem>(
      // hint: SvgPicture.asset(
      //   widget.hint,
      //   width: 30,
      //   height: 20,
      // ),
      // value: selectImage,
      isDense: true,
      // isExpanded: true,
      // icon: SvgPicture.asset(selectImage.image,
      //     width: 30, height: 20, fit: BoxFit.fitHeight),
      value: selectImage,
      onChanged: (newValue) {
        if (widget.callback != null) {
          widget.callback!(newValue!);
        }
        for (var element in options) {
          element.isSelect = false;
          if (element.id == newValue!.id) {
            element.isSelect = true;
          }
        }
        setState(() {
          selectImage = newValue!;
        });
      },
      selectedItemBuilder: (context) {
        return options.map(
          (item) {
            return Image.asset(item.image, width: 30, height: 20);
          },
        ).toList();
      },
      icon: const Icon(
        Icons.ac_unit,
        color: primary,
      ),
      items: options.map((OptionItem value) {
        return DropdownMenuItem<OptionItem>(
          value: value,
          child: Row(children: [
            Image.asset(
              value.image,
              width: 30,
              height: 20,
            ),
            const SizedBox(
              width: 4,
            ),
            if (value.isSelect)
              Image.asset(
                check,
                width: 20,
                height: 20,
              ),
          ]),
        );
      }).toList(),
    ));
  }
}

// class DropdownWidget extends StatefulWidget {
//   List<String> items;
//   SvgPicture? icon;
//   double width;

//   DropdownWidget({
//     Key? key,
//     required this.items,
//     required this.icon,
//     required this.width,
//   }) : super(key: key);

//   @override
//   State<DropdownWidget> createState() => _DropdownWidgetState();
// }

// class _DropdownWidgetState extends State<DropdownWidget> {
//   String? selectedValue;
//   bool selected = false;

//   final List _selectedTitles = [];
//   final List _selectedTitlesIndex = [];

//   final GFCheckboxType type = GFCheckboxType.basic;

//   @override
//   void initState() {
//     super.initState();
//     if (widget.items.isNotEmpty) {
//       _selectedTitles.add(widget.items[3]);
//     }
//   }

//   void _onItemSelect(bool selected, int index) {
//     if (selected == true) {
//       setState(() {
//         _selectedTitles.add(widget.items[index]);
//         _selectedTitlesIndex.add(index);
//       });
//     } else {
//       setState(() {
//         _selectedTitles.remove(widget.items[index]);
//         _selectedTitlesIndex.remove(index);
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return SizedBox(
//       width: widget.width,
//       child: DropdownButtonHideUnderline(
//         child: DropdownButton2(
//           items: List.generate(
//             widget.items.length,
//             (index) => DropdownMenuItem<String>(
//               value: widget.items[index],
//               child: Container(
//                 decoration: BoxDecoration(
//                   border: Border(
//                     bottom: BorderSide(
//                       color: constants.Colors.white.withOpacity(0.1),
//                       width: 1,
//                     ),
//                   ),
//                 ),
//                 child: StatefulBuilder(
//                   builder: (context, setStateSB) => GFCheckboxListTile(
//                     value: _selectedTitles.contains(widget.items[index]),
//                     onChanged: (bool selected) {
//                       _onItemSelect(selected, index);
//                       setStateSB(() {});
//                     },
//                     selected: selected,
//                     title: Text(
//                       widget.items[index],
//                       style: constants.Styles.smallTextStyleWhite,
//                     ),
//                     padding: const EdgeInsets.only(top: 14, bottom: 13),
//                     margin: const EdgeInsets.only(right: 12, left: 2),
//                     size: 22,
//                     activeBgColor: constants.Colors.greyCheckbox,
//                     activeBorderColor: constants.Colors.greyCheckbox,
//                     inactiveBgColor: constants.Colors.greyCheckbox,
//                     activeIcon: SvgPicture.asset(constants.Assets.checkboxIcon),
//                     inactiveBorderColor: constants.Colors.greyXMiddle,
//                     type: type,
//                   ),
//                 ),
//               ),
//             ),
//           ),
//           value: selectedValue,
//           onChanged: (value) {
//             setState(() {
//               selectedValue = value as String;
//             });
//           },
//           icon: SvgPicture.asset(constants.Assets.arrowDropdown),
//           iconSize: 21,
//           buttonHeight: 27,
//           itemHeight: 66,
//           dropdownMaxHeight: 191,
//           dropdownWidth: 140,
//           dropdownDecoration: BoxDecoration(
//             borderRadius: BorderRadius.circular(8),
//             border: Border.all(
//               color: constants.Colors.purpleMain,
//             ),
//             color: constants.Colors.greyDark,
//           ),
//           selectedItemBuilder: (context) {
//             return _selectedTitles.map(
//               (item) {
//                 return Row(
//                   children: [
//                     widget.icon ?? const SizedBox(),
//                     const SizedBox(width: 8),
//                     Text(
//                       item,
//                       style: constants.Styles.bigBookTextStyleWhite,
//                     ),
//                   ],
//                 );
//               },
//             ).toList();
//           },
//         ),
//       ),
//     );
//   }
// }
