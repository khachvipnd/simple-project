class DropListModel {
  DropListModel(this.listOptionItems);

  final List<OptionItem> listOptionItems;
}

class OptionItem {
  final String id;
  String title;
  String ma = "";
  String image = "";
  bool isSelect = false;
  dynamic object;

  OptionItem(
      {required this.id,
      required this.title,
      this.isSelect = false,
      this.object,
      this.ma = "",
      this.image = ""});
}
