import 'package:flutter/material.dart';

import '../../../constants/size.dart';
import 'drop_list_model.dart';

class SelectDropList extends StatefulWidget {
  final OptionItem? itemSelected;
  final DropListModel? dropListModel;
  IconData? icon;
  final Function(OptionItem optionItem)? onOptionSelected;

  SelectDropList(
      {this.itemSelected, this.dropListModel, this.onOptionSelected});

  @override
  _SelectDropListState createState() => _SelectDropListState();
}

class _SelectDropListState extends State<SelectDropList>
    with SingleTickerProviderStateMixin {
  OptionItem? optionItemSelected;
  DropListModel? dropListModel;

  AnimationController? expandController;
  Animation<double>? animation;

  bool isShow = false;

  @override
  void initState() {
    super.initState();
    optionItemSelected = widget.itemSelected;
    dropListModel = widget.dropListModel;
    expandController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 350));
    animation = CurvedAnimation(
      parent: expandController!,
      curve: Curves.fastOutSlowIn,
    );
    _runExpandCheck();
  }

  @override
  void didUpdateWidget(SelectDropList oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  void _runExpandCheck() {
    if (isShow) {
      expandController?.forward();
    } else {
      expandController?.reverse();
    }
  }

  @override
  void dispose() {
    expandController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 17),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            color: Colors.white,
            boxShadow: const [
              BoxShadow(
                  blurRadius: 10, color: Colors.black26, offset: Offset(0, 2))
            ],
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const Icon(
                Icons.card_travel,
                color: Color(0xFF307DF1),
              ),
              const SizedBox(
                width: 10,
              ),
              Expanded(
                  child: GestureDetector(
                onTap: () {
                  isShow = !isShow;
                  _runExpandCheck();
                  setState(() {});
                },
                child: Text(
                  (optionItemSelected!.title),
                  style: const TextStyle(
                      color: Color(0xFF307DF1), fontSize: fontSize16),
                ),
              )),
              Align(
                alignment: const Alignment(1, 0),
                child: Icon(
                  isShow ? Icons.arrow_drop_down : Icons.arrow_right,
                  color: const Color(0xFF307DF1),
                  size: 20,
                ),
              ),
            ],
          ),
        ),
        SizeTransition(
            axisAlignment: 1.0,
            sizeFactor: animation!,
            child: Container(
                margin: const EdgeInsets.only(bottom: 10),
                padding: const EdgeInsets.only(bottom: 10),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(20),
                      bottomRight: Radius.circular(20)),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 4,
                        color: Colors.black26,
                        offset: Offset(0, 4))
                  ],
                ),
                child: _buildDropListOptions(
                    dropListModel!.listOptionItems, context))),
//          Divider(color: Colors.grey.shade300, height: 1,)
      ],
    );
  }

  Column _buildDropListOptions(List<OptionItem> items, BuildContext context) {
    return Column(
      children: items.map((item) => _buildSubMenu(item, context)).toList(),
    );
  }

  Widget _buildSubMenu(OptionItem item, BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 26.0, top: 5, bottom: 5),
      child: GestureDetector(
        child: Row(
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                padding: const EdgeInsets.only(top: 20),
                decoration: const BoxDecoration(
                  border: Border(top: BorderSide(color: Colors.grey, width: 1)),
                ),
                child: Text(item.title,
                    style: const TextStyle(
                        color: Color(0xFF307DF1),
                        fontWeight: FontWeight.w400,
                        fontSize: fontSize),
                    maxLines: 3,
                    textAlign: TextAlign.start,
                    overflow: TextOverflow.ellipsis),
              ),
            ),
          ],
        ),
        onTap: () {
          optionItemSelected = item;
          isShow = false;
          expandController!.reverse();
          widget.onOptionSelected!(item);
        },
      ),
    );
  }
}
