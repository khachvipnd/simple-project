import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/styles.dart';

import 'drop_list_model.dart';

class DropDownUnderLineWidget extends StatefulWidget {
  DropDownUnderLineWidget(
      {Key? key, this.options, this.selectImage, this.hint = "", this.callback})
      : super(key: key);
  List<OptionItem>? options = [];
  OptionItem? selectImage;
  String hint = "";
  CallbackActionOptionItem? callback;
  @override
  State<DropDownUnderLineWidget> createState() =>
      _DropDownUnderLineWidgetState();
}

class _DropDownUnderLineWidgetState extends State<DropDownUnderLineWidget> {
  List<OptionItem> options = [];
  OptionItem? selectImage;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    options = widget.options ?? [];
    selectImage = widget.selectImage;
  }

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
        child: DropdownButton<OptionItem>(
      hint: Text(
        widget.hint.tr,
        style: normalStyle,
      ),
      value: selectImage,
      isDense: true,
      // isExpanded: true,
      onChanged: (newValue) {
        if (widget.callback != null) {
          widget.callback!(newValue!);
        }
        setState(() {
          selectImage = newValue!;
        });
      },
      items: options.map((OptionItem value) {
        return DropdownMenuItem<OptionItem>(
          value: value,
          child: Text(value.title.tr, style: normalStyle),
        );
      }).toList(),
    ));
  }
}
