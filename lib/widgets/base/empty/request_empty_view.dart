import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/colors.dart';
import 'package:yourpti/constants/icons.dart';
import 'package:yourpti/constants/styles.dart';

import '../../../constants/size.dart';

class RequestEmptyView extends StatelessWidget {
  RequestEmptyView({Key? key, this.title = "", this.content = ""})
      : super(key: key);
  String title = "";
  String content = "";

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SvgPicture.asset(iconReuqetsEmpty),
        const SizedBox(
          height: 40,
        ),
        Text(
          title.tr,
          textAlign: TextAlign.center,
          style: boldStyle.copyWith(fontSize: fontSize16, color: neutral_500),
        ),
        const SizedBox(
          height: 10,
        ),
        Text(
          title.tr,
          textAlign: TextAlign.center,
          style: normalStyle.copyWith(fontSize: fontSize13, color: neutral_400),
        )
      ],
    );
  }
}
