import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';

import '../../constants/colors.dart';
import '../../constants/size.dart';

// ignore: must_be_immutable
class ButtonForm extends StatefulWidget implements FormObject {
  ButtonForm(
      {Key? key,
      this.onChangeCallback,
      this.control,
      colorText = neutral_500,
      this.onEditingComplete});

  OnChangeCallBack? onChangeCallback;
  OnChangeCallBack? onEditingComplete;
  Color? colorText;
  Color? colorBorder;
  TextStyle? textStyle;
  @override
  Control? control;

  @override
  State<ButtonForm> createState() => _ButtonForm();

  @override
  void callReloadView() {}
}

class _ButtonForm extends State<ButtonForm> {
  @override
  void initState() {
    super.initState();

    if (widget.control!.style.isNotEmpty) {
      // widget.colorText = HexColor.fromHex(widget.control!.style['textColor']);
      widget.colorText =
          HexColor.fromHex(widget.control!.style['textColor'] ?? "#FFFFFF");
      widget.colorBorder =
          HexColor.fromHex(widget.control!.style['colorBorder'] ?? "#FFFFFF");

      widget.textStyle = widget.control!.style['textStyle'] == 'bold'
          ? boldStyle.copyWith(
              color: widget.colorText,
              fontSize: widget.control!.style['fontSize'] != null
                  ? widget.control!.style['fontSize'].toDouble()
                  : fontSize,
            )
          : normalStyle.copyWith(
              color: widget.colorText,
              fontSize: widget.control!.style['fontSize'] != null
                  ? widget.control!.style['fontSize'].toDouble()
                  : fontSize,
            );
    }
  }

  @override
  Widget build(BuildContext context) {
    // String widget2 = widget.title;
    return Obx(() => !widget.control!.visible.value
        ? Container(
            child: null,
          )
        : Container(
            child: Expanded(
                flex: widget.control!.flex,
                child: InkWell(
                    onTap: () {
                      if (!widget.control!.enabled.value) {
                        return;
                      }
                      if (widget.onChangeCallback != null) {
                        widget.onChangeCallback!(widget);
                      }
                      if (widget.onEditingComplete != null) {
                        widget.onEditingComplete!(widget);
                      }
                    },
                    child: Padding(
                        padding: EdgeInsets.only(
                            left: widget.control!.indexRow > 0 ? 10 : 16,
                            // top: 10,
                            right: widget.control!.isLastRow ? 16 : 0),
                        child: Container(
                          decoration: BoxDecoration(
                              // color: primary,
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(
                                  color: widget.colorBorder ?? white)),
                          child: Padding(
                            padding: EdgeInsets.all(
                                widget.colorBorder != null ? 8.0 : 0),
                            child: Text(
                              widget.control!.title.value,
                              textAlign: TextAlign.left,
                              style: widget.textStyle ??
                                  boldStyle.copyWith(
                                    color: widget.colorText,
                                    fontSize: fontSize,
                                  ),
// ,        TextStyle(
//             color: neutral_500,
//             fontSize: fontSize,
//             fontWeight: FontWeight.bold,
//             fontFamily: "Helveticaneue"),
                            ),
                          ),
                        ))))));
    // decoration: InputDecoration(;
    //   labelText: widget.control!.title ?? "",
    // ),
    // initialValue: widget.control!.defaultValue,
    // onChanged: (String? value) {
    //   // debugprintDebug('"$value');
    //   control?.value = value!;

    //   if (widget.onChangeCallback != null) widget.onChangeCallback!(this);
    // },
    // onSaved: (String? value) {
    //   // printDebug('"$value"');
    //   // printDebug('"$value"');
    // },
    // onEditingComplete: () {
    //   if (widget.onEditingComplete != null) widget.onEditingComplete!(this);
    // },
    // );
  }
}
