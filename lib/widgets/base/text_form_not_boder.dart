import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/size.dart';

import '../../constants/callbacks.dart';
import '../../constants/colors.dart';
import '../../constants/styles.dart';
import 'package:yourpti/extensions/string_extension.dart';

// ignore: must_be_immutable
class TextFormNotBorderWidget extends StatefulWidget {
  // ignore: use_key_in_widget_constructors
  TextFormNotBorderWidget(
      {Key? key,
      this.callback,
      this.hintText = "",
      this.maxLines,
      this.controller,
      this.callbackAction,
      this.icon,
      this.iconAction,
      this.title = "",
      this.textValidate,
      this.titlefather,
      this.fontSizeTitle = fontSize,
      this.styleFather = boldStyle,
      this.isRequail = false,
      this.obscureText = false,
      this.keyboardType = TextInputType.text});
  CallbackActionString? callback;
  CallbackAction? callbackAction;
  String? titlefather;
  double fontSizeTitle = fontSize;
  TextStyle styleFather = boldStyle;
  String? content;
  TextEditingController? controller;
  String hintText = "";
  int? maxLines;
  String? icon;
  bool isRequail = false;
  String? title;
  String? iconAction;
  String? textValidate;
  TextInputType keyboardType;

  bool obscureText = false;

  @override
  State<TextFormNotBorderWidget> createState() =>
      _TextFormNotBorderWidgetState();
}

class _TextFormNotBorderWidgetState extends State<TextFormNotBorderWidget> {
  var textValidate = "".obs;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        titleView(
            title: (widget.title ?? "").tr,
            isRequail: widget.isRequail,
            titleFather: widget.titlefather),
        const SizedBox(
          height: 6,
        ),
        inputView(),
        if (textValidate.value.isNotEmpty)
          const SizedBox(
            height: 6,
          ),
        Obx(() => Text(
              textValidate.value,
              style: normalStyleValidate.copyWith(color: red_600, fontSize: 11),
            ))
      ],
    );
  }

  Widget inputView() {
    return SizedBox(
      height: widget.maxLines == null ? 40 : 90,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: grayconst_100,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              width: 10,
            ),
            if (widget.icon != null)
              SvgPicture.asset(
                widget.icon ?? "",
                height: 18,
                width: 18,
              ),
            if (widget.icon != null)
              const SizedBox(
                width: 10,
              ),
            Expanded(
              child: Center(
                child: TextFormField(
                  controller: widget.controller,
                  // initialValue: widget.content,
                  maxLines: widget.maxLines,
                  keyboardType: widget.keyboardType,
                  style: normalStyle.copyWith(
                    fontSize: fontSizeNomal,
                    color: neutral_500,
                    fontWeight: FontWeight.w400,
                  ),
                  onChanged: (value) {
                    if (widget.callback != null) {
                      widget.callback!(value);
                    }
                  },
                  validator: (value) {
                    // if (value!.isEmpty) {
                    //   textValidate.value = widget.textValidate ?? "";
                    // }
                    return onValidate(value);
                  },
                  decoration: InputDecoration(
                      isCollapsed: true,
                      border: InputBorder.none,
                      errorMaxLines: 1,
                      errorStyle: const TextStyle(
                          fontSize: 0, height: 0.8, color: red_500),
                      hintText: (widget.hintText).tr),
                ),
              ),
            ),
            // const Icon(Icons.visibility_rounded, color: unselectedTextColor)

            if (widget.iconAction != null)
              InkWell(
                onTap: () {
                  if (widget.callbackAction != null) {
                    widget.callbackAction;
                  }
                },
                child: SvgPicture.asset(
                  widget.iconAction ?? "",
                  height: 18,
                  width: 18,
                ),
              ),

            if (widget.iconAction != null) const SizedBox(width: 10)
          ],
        ),
      ),
    );
  }

  Widget titleView(
      {required String title, String? titleFather, bool isRequail = false}) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          if (titleFather != null)
            TextSpan(text: ' $titleFather', style: widget.styleFather),
          TextSpan(
              text: ' $title',
              style: normalStyle.copyWith(
                  color: neutral_500,
                  fontWeight: FontWeight.w400,
                  fontSize: widget.fontSizeTitle)),
          TextSpan(
              text: isRequail ? '* ' : ' ',
              style: normalStyle.copyWith(
                  color: red_600,
                  fontWeight: FontWeight.w500,
                  fontSize: widget.fontSizeTitle)),
        ],
      ),
    );
  }

  String? onValidate(value) {
    if (widget.isRequail) {
      if (widget.keyboardType == TextInputType.phone &&
          !value.toString().isValidPhone()) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }
      if (widget.keyboardType == TextInputType.emailAddress &&
          !value.toString().isValidEmail()) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }
      if (value.toString().isEmpty) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }
      textValidate.value = "";
      return null;
    }
    return null;
  }
}
