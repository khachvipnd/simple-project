import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../constants/colors.dart';
import '../../../constants/icons.dart';
import '../../../constants/size.dart';
import '../../../constants/styles.dart';

class ViewNotData extends StatelessWidget {
  ViewNotData({Key? key, this.title = "", this.content = ""}) : super(key: key);
  String title = "";
  String content = "";
  @override
  Widget build(BuildContext context) {
    return viewNotData();
  }

  Widget viewNotData() {
    return Container(
      padding: paddingLeftRight,
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset(iconNoDemnify),
          const SizedBox(
            height: 50,
          ),
          Text(
            title.tr,
            style: boldStyle.copyWith(
                fontSize: fontSize16,
                fontWeight: FontWeight.w500,
                color: neutral_500),
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            content.tr,
            textAlign: TextAlign.center,
            style: normalStyle.copyWith(
                fontSize: fontSize13,
                fontWeight: FontWeight.w400,
                color: neutral_400),
          ),
        ],
      ),
    );
  }
}
