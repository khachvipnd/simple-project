import 'package:flutter/material.dart';

abstract class ImageView {
  ImageProvider get imageProvider;
  String get heroTag;
}
