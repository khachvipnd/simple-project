import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yourpti/constants/colors.dart';
import '../../../constants/icons.dart';
import '../../../constants/size.dart';
import 'imageurl.dart';
import 'imageview.dart';

class ImageViewWidget extends StatelessWidget {
  ImageViewWidget(
      {Key? key,
      this.url,
      this.heightImage,
      this.widthImage,
      this.aspectRatio = 3 / 4,
      this.iconError,
      this.radius = 6,
      this.fit = BoxFit.fitWidth})
      : super(key: key);
  String? url;
  double? heightImage = 100;
  double? widthImage = 120;
  double aspectRatio = 3 / 4;
  double radius = 6;
  Widget? iconError = SvgPicture.asset(avatar);
  BoxFit fit = BoxFit.fitWidth;
  final CacheManager customCacheManager = CacheManager(Config('customCacheKey',
      stalePeriod: const Duration(days: 15), maxNrOfCacheObjects: 200));
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
        borderRadius: BorderRadius.circular(radius),
        child: CachedNetworkImage(
          imageUrl: url ?? "",
          cacheManager: customCacheManager,
          errorWidget: (context, url, error) =>
              iconError ??
              SvgPicture.asset(
                avatar,
                height: heightImage,
                width: widthImage,
              ),
          placeholder: (context, url) {
            return SizedBox(
              height: heightImage,
              width: widthImage,
              child: Padding(
                padding: const EdgeInsets.only(
                    bottom: normalPadding, right: normalPadding),
                child: AspectRatio(
                    aspectRatio: aspectRatio,
                    child: Container(
                      height: heightImage,
                      width: widthImage,
                      color: neutral_100,
                    )),
              ),
            );
          },
          imageBuilder: (context, imageProvider) {
            return SizedBox(
                height: heightImage,
                width: widthImage,
                child: AspectRatio(
                    aspectRatio: aspectRatio,
                    child: Image(
                      height: heightImage,
                      width: widthImage,
                      image: imageProvider,
                      fit: fit,
                    )));
          },
        ));
  }
}

class ImageViewURL implements ImageURL, ImageView {
  String? routeUrl;
  bool atLocal = false;

  ImageViewURL({
    this.routeUrl,
    this.atLocal = false,
  });
  String get fullUrl {
    return '$routeUrl';
  }

  @override
  ImageProvider get imageProvider {
    if (atLocal) {
      return FileImage(File(url));
    }
    return CachedNetworkImageProvider(url);
  }

  @override
  String get url {
    return routeUrl ?? "";
  }

  @override
  String get heroTag => url;
}
