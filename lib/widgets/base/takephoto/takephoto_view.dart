import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/manager/camera_manager.dart';
import 'dart:io' as io;
import '../../../constants/colors.dart';
import '../../../constants/enums.dart';
import '../../../constants/icons.dart';
import '../../../constants/size.dart';

class TakePhotoView extends StatefulWidget {
  TakePhotoView({
    Key? key,
    this.callback,
    this.height,
    this.width,
    this.title,
    this.radius = 8,
    this.isShowImage = true,
    this.cameraType = CameraType.full,
  }) : super(key: key);
  double? height = 100;
  double? width = 100;
  String? title;
  double radius = 8;
  bool isShowImage = true;
  CallbackActionString? callback;
  CameraType cameraType = CameraType.full;

  @override
  State<TakePhotoView> createState() => _TakePhotoViewState();
}

class _TakePhotoViewState extends State<TakePhotoView> {
  String path = "";

  final CameraManager cameraManager = const CameraManager();
  @override
  Widget build(BuildContext context) {
    return InkWell(
        onTap: () {
          cameraManager.showPicker(
              context: context,
              type: widget.cameraType,
              callback: (image) {
                path = image.path;
                if (widget.callback != null) {
                  widget.callback!(image.path);
                }
                if (widget.isShowImage) {
                  setState(() {});
                }

                // Navigator.of(context).pop();
              });
        },
        child: Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(widget.radius),
            color: gray_200,
          ),
          padding: const EdgeInsets.all(10),
          margin: const EdgeInsets.all(6),
          child: (path == "" || widget.isShowImage == false)
              ? Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                      (path == "" || widget.isShowImage == false)
                          ? SvgPicture.asset(
                              iconCamera,
                              width: 30,
                              height: 20,
                            )
                          : Image.file(io.File(path)),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(
                          widget.title == null
                              ? "takephoto".tr
                              : widget.title!.tr,
                          textAlign: TextAlign.center,
                          style: normalStyle.copyWith(
                              fontSize: fontSize12,
                              fontWeight: FontWeight.w400,
                              color: neutral_400))
                    ])
              : ClipRRect(
                  borderRadius: BorderRadius.circular(widget.radius),
                  child: Image.file(
                    io.File(path),
                    fit: BoxFit.fill,
                    height: widget.height,
                    width: widget.width,
                  )),
        ));
  }
}
