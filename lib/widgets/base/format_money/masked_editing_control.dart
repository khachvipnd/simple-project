import 'package:flutter/material.dart';

class MaskedTextController extends TextEditingController
    with WidgetsBindingObserver {
  MaskedTextController(
      {String? text,
      this.mask = "",
      this.maxleng = 100,
      this.maxNumber = 0,
      Map<String, RegExp>? translator})
      : super(text: text) {
    this.translator = translator ?? MaskedTextController.getDefaultTranslator();
    WidgetsBinding.instance.addObserver(this);
    addListener(() {
      var previous = _lastUpdatedText;
      if (selection.extentOffset >= 0) {
        if (_lastUpdatedText.length == this.text.length) {
          if (numberOffset == -1) {
            numberOffset = this.text.length;
          } else {
            if (isNext) {
              numberOffset = 0;
              isNext = false;
            } else {
              numberOffset = this.text.length - selection.extentOffset;
            }
          }
        }
      }
      if (beforeChange(previous, this.text)) {
        updateText(this.text);
        afterChange(previous, this.text);
      } else {
        updateText(_lastUpdatedText);
      }
    });

    updateText(this.text);
  }
  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    final bottomInset = WidgetsBinding.instance.window.viewInsets.bottom;
    final newValue = bottomInset > 0.0;

    // if (newValue != isShowKey) {
    //   isShowKey = newValue;
    //   if (text.isEmpty && newValue == false) {
    //     text = "0";
    //   }
    //   if (text == "0" && newValue) {
    //     text = "";
    //   }
    // }
  }

  bool isShowKey = false;
  String mask = "";
  int maxleng = 100000;
  double maxNumber = 0;
  bool isNext = false;
  int extentOffset = 0;
  int numberOffset = 0;
  bool isDouble = false;
  Map<String, RegExp>? translator;

  Function afterChange = (String previous, String next) {};
  Function beforeChange = (String previous, String next) {
    return true;
  };

  String _lastUpdatedText = '';
  void updateText(String text) {
    if (_lastUpdatedText != this.text) {
      this.text = _applyMask(mask, text);
      final newString = text.replaceAll(",", "");
      final lastString = _lastUpdatedText.replaceAll(",", "");
      if (newString.length <= lastString.length &&
          selection.start < 0 &&
          numberOffset == 3) {
        numberOffset += 1;
      }

      _lastUpdatedText = this.text;
      moveCursorToOffset();
    } else {
      // this.moveCursorToEnd();
    }
  }

  void updateMask(String mask, {bool moveCursorToEnd = true}) {
    this.mask = mask;
    updateText(text);

    if (moveCursorToEnd) {
      moveCursorToOffset();
    }
  }

  void moveCursorToOffset() {
    // printDebug(extentOffset);
    var text = _lastUpdatedText;
    if (extentOffset > (text).length) {
      extentOffset -= 1;
      moveCursorToOffset();
      return;
    } else {
      if (numberOffset < 0) {
        extentOffset = (text).length;
      } else {
        extentOffset = (text).length - numberOffset;
      }
    }

    if (extentOffset < -1) {
      extentOffset = -1;
    }
    selection = TextSelection.fromPosition(TextPosition(offset: extentOffset));
  }

  @override
  set text(String newText) {
    if (super.text != newText) {
      if (super.text.length > _lastUpdatedText.length) {
        final extentOffset = _lastUpdatedText.length - numberOffset;
        final offset = extentOffset % 4;

        // if(offset)
      }
      String newValue = super.text.replaceAll(",", "");
      if (isNumeric(newValue) == false) {
        super.text = _lastUpdatedText;
        if (super.text.isEmpty && newText == "0") {
          super.text = newText;
        }
        isNext = true;
      } else {
        super.text = newText;
      }

      moveCursorToOffset();
    }
  }

  static Map<String, RegExp> getDefaultTranslator() {
    return {
      'A': RegExp(r'[A-Za-z]'),
      '0': RegExp(r'[0-9]'),
      '@': RegExp(r'[A-Za-z0-9]'),
      '*': RegExp(r'.*')
    };
  }

  String _applyMask(String mask, String value) {
    String result = '';

    final newValue = value.replaceAll(",", "");
    if (newValue.isEmpty && value.isNotEmpty) {
      return _lastUpdatedText;
    }
    List<String> arrayData = newValue.split(".");
    if (_lastUpdatedText.length >= maxleng &&
        value.length > _lastUpdatedText.length) {
      return _lastUpdatedText;
    }
    if (arrayData.length > 2 || isNumeric(newValue) == false) {
      // final newValue = this._lastUpdatedText.replaceAll(",", "");
      // arrayData = newValue.split(".");
      numberOffset = -1;
      if (newValue.isEmpty) {
        numberOffset = 0;
        return value;
      }
      return _lastUpdatedText;
    }

    if (maxNumber > 0 && double.parse(newValue) > maxNumber) {
      return _lastUpdatedText;
    }
    var number = "";
    var number1 = "";
    if (arrayData.isNotEmpty) {
      number = arrayData[0];
    }
    if (arrayData.length > 1) {
      number1 = arrayData[1];
    }
    if (number1.length > 2) {
      numberOffset = -1;
      return _lastUpdatedText;
    }
    var arrayNumber = number.split('');
    arrayNumber = arrayNumber.reversed.toList();
    final arrayNumber1 = number1.split('');
    for (int i = 0; i < arrayNumber.length; i++) {
      if (i % 3 == 0 && i != 0) {
        result = result + "," + arrayNumber[i];
      } else {
        result = result + arrayNumber[i];
      }
    }
    String result1 = "";
    for (int i = 0; i < arrayNumber1.length; i++) {
      result1 = result1 + arrayNumber1[i];
    }
    if (result1.isNotEmpty || newValue.contains(".")) {
      return result.split('').reversed.join() + "." + result1;
    }
    return result.split('').reversed.join();
  }

  bool isNumeric(String? s) {
    if (s == null || s == "") {
      return false;
    }
    double? value = double.tryParse(s);
    return value != null;
  }
}

class NumberTextController extends TextEditingController {
  NumberTextController(
      {String? text,
      this.mask = "",
      Map<String, RegExp>? translator,
      this.maxNumber = 0})
      : super(text: text) {
    this.translator = translator ?? MaskedTextController.getDefaultTranslator();

    addListener(() {
      var previous = _lastUpdatedText;
      if (selection.extentOffset >= 0) {
        if (_lastUpdatedText.length == this.text.length) {
          if (numberOffset == -1) {
            numberOffset = this.text.length;
          } else {
            if (isNext) {
              numberOffset = 0;
              isNext = false;
            } else {
              numberOffset = this.text.length - selection.extentOffset;
            }
          }
        }
      }
      if (beforeChange(previous, this.text)) {
        updateText(this.text);
        afterChange(previous, this.text);
      } else {
        updateText(_lastUpdatedText);
      }
    });

    updateText(this.text);
  }

  String mask = "";
  int maxleng = 100000;
  double maxNumber = 0;
  bool isNext = false;
  int extentOffset = 0;
  int numberOffset = 0;
  bool isDouble = false;
  Map<String, RegExp>? translator;

  Function afterChange = (String previous, String next) {};
  Function beforeChange = (String previous, String next) {
    return true;
  };

  String _lastUpdatedText = '';
  void updateText(String text) {
    if (_lastUpdatedText != this.text) {
      this.text = _applyMask(mask, text);
      final newString = text.replaceAll(",", "");
      final lastString = _lastUpdatedText.replaceAll(",", "");
      if (newString.length <= lastString.length &&
          selection.start < 0 &&
          numberOffset == 3) {
        numberOffset += 1;
      }

      _lastUpdatedText = this.text;
      moveCursorToOffset();
    } else {
      // this.moveCursorToEnd();
    }
  }

  void updateMask(String mask, {bool moveCursorToEnd = true}) {
    this.mask = mask;
    updateText(text);

    if (moveCursorToEnd) {
      moveCursorToOffset();
    }
  }

  void moveCursorToOffset() {
    // printDebug(extentOffset);
    var text = _lastUpdatedText;
    if (extentOffset > (text).length) {
      extentOffset -= 1;
      moveCursorToOffset();
      return;
    } else {
      if (numberOffset < 0) {
        extentOffset = (text).length;
      } else {
        extentOffset = (text).length - numberOffset;
      }
    }

    if (extentOffset < -1) {
      extentOffset = -1;
    }
    selection = TextSelection.fromPosition(TextPosition(offset: extentOffset));
  }

  @override
  set text(String newText) {
    if (super.text != newText) {
      if (super.text.length > _lastUpdatedText.length) {
        final extentOffset = _lastUpdatedText.length - numberOffset;
        final offset = extentOffset % 4;

        // if(offset)
      }
      String newValue = super.text.replaceAll(",", "");
      if (newValue.isNotEmpty) {
        if (isNumeric(newValue) == false) {
          super.text = _lastUpdatedText;
          isNext = true;
        } else {
          super.text = newText;
        }
      } else {
        String newValue = newText.replaceAll(",", "");

        if (isNumeric(newValue) == false) {
          super.text = _lastUpdatedText;
          isNext = true;
        } else {
          if (maxNumber == 0 || double.parse(newValue) < maxNumber) {
            super.text = newText;
          } else {
            super.text = _lastUpdatedText;
          }
        }
      }

      moveCursorToOffset();
    }
  }

  static Map<String, RegExp> getDefaultTranslator() {
    return {
      'A': RegExp(r'[A-Za-z]'),
      '0': RegExp(r'[0-9]'),
      '@': RegExp(r'[A-Za-z0-9]'),
      '*': RegExp(r'.*')
    };
  }

  String _applyMask(String mask, String value) {
    String result = '';

    final newValue = value.replaceAll(",", "");
    if (newValue.isEmpty && value.isNotEmpty) {
      return _lastUpdatedText;
    }
    List<String> arrayData = newValue.split(".");
    if (_lastUpdatedText.length >= maxleng &&
        value.length > _lastUpdatedText.length) {
      return _lastUpdatedText;
    }
    if (arrayData.length > 2 || isNumeric(newValue) == false) {
      // final newValue = this._lastUpdatedText.replaceAll(",", "");
      // arrayData = newValue.split(".");
      numberOffset = -1;
      if (newValue.isEmpty) {
        numberOffset = 0;
        return value;
      }
      return _lastUpdatedText;
    }

    if (maxNumber > 0 && double.parse(newValue) > maxNumber) {
      return _lastUpdatedText;
    }
    var number = "";
    var number1 = "";
    if (arrayData.isNotEmpty) {
      number = arrayData[0];
    }
    if (arrayData.length > 1) {
      number1 = arrayData[1];
    }
    if (number1.length > 2) {
      numberOffset = -1;
      return _lastUpdatedText;
    }
    var arrayNumber = number.split('');
    arrayNumber = arrayNumber.reversed.toList();
    final arrayNumber1 = number1.split('');
    for (int i = 0; i < arrayNumber.length; i++) {
      result = result + arrayNumber[i];
    }
    String result1 = "";
    for (int i = 0; i < arrayNumber1.length; i++) {
      result1 = result1 + arrayNumber1[i];
    }
    if (result1.isNotEmpty || newValue.contains(".")) {
      return result.split('').reversed.join() + "." + result1;
    }
    return result.split('').reversed.join();
  }

  bool isNumeric(String? s) {
    if (s == null || s == "") {
      return false;
    }
    double? value = double.tryParse(s);
    return value != null;
  }
}

class IntTextController extends TextEditingController {
  IntTextController(
      {String? text, this.mask = "", Map<String, RegExp>? translator})
      : super(text: text) {
    this.translator = translator ?? IntTextController.getDefaultTranslator();
    addListener(() {
      var previous = _lastUpdatedText;
      if (beforeChange(previous, this.text)) {
        updateText(this.text);
        afterChange(previous, this.text);
      } else {
        updateText(_lastUpdatedText);
      }
    });

    updateText(this.text);
  }

  String mask = "";

  Map<String, RegExp>? translator = IntTextController.getDefaultTranslator();
  int maxNumber = 100;
  Function afterChange = (String previous, String next) {};
  Function beforeChange = (String previous, String next) {
    return true;
  };

  String _lastUpdatedText = '';

  void updateText(String text) {
    if (text.isNotEmpty) {
      final newValue = text.replaceAll(",", "");
      if (newValue.isEmpty) {
        this.text = _lastUpdatedText;
      }
      try {
        int number = int.parse(text);
        if (number == 0) {
          this.text = _lastUpdatedText;
          return;
        }
        if (number > maxNumber) {
          this.text = _lastUpdatedText;
          return;
        } else {
          if (text != null) {
            this.text = text; //this._applyMask(this.mask, text);
          } else {
            this.text = '1';
          }

          _lastUpdatedText = this.text;
        }
      } catch (e) {
        this.text = _lastUpdatedText;
        return;
      }
    } else {
      _lastUpdatedText = this.text;
    }
  }

  void updateMask(String mask, {bool moveCursorToEnd = true}) {
    this.mask = mask;
    updateText(text);

    if (moveCursorToEnd) {
      this.moveCursorToEnd();
    }
  }

  void moveCursorToEnd() {
    var text = _lastUpdatedText;
    selection = TextSelection.fromPosition(TextPosition(offset: (text).length));
  }

  @override
  void set text(String newText) {
    if (super.text != newText) {
      super.text = newText;
      moveCursorToEnd();
    }
  }

  static Map<String, RegExp> getDefaultTranslator() {
    return {
      'A': RegExp(r'[A-Za-z]'),
      '0': RegExp(r'[0-9]'),
      '@': RegExp(r'[A-Za-z0-9]'),
      '*': RegExp(r'.*')
    };
  }

  String _applyMask(String mask, String value) {
    String result = '';

    var maskCharIndex = 0;
    var valueCharIndex = 0;

    while (true) {
      // if mask is ended, break.
      if (maskCharIndex == mask.length) {
        break;
      }

      // if value is ended, break.
      if (valueCharIndex == value.length) {
        break;
      }

      var maskChar = mask[maskCharIndex];
      var valueChar = value[valueCharIndex];

      // value equals mask, just set
      if (maskChar == valueChar) {
        result += maskChar;
        valueCharIndex += 1;
        maskCharIndex += 1;
        continue;
      }

      // apply translator if match
      if (translator!.containsKey(maskChar)) {
        if (translator![maskChar]!.hasMatch(valueChar)) {
          result += valueChar;
          maskCharIndex += 1;
        }

        valueCharIndex += 1;
        continue;
      }

      // not masked value, fixed char on mask
      result += maskChar;
      maskCharIndex += 1;
      continue;
    }

    return result;
  }
}
