import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';
import 'package:yourpti/widgets/base/dropdown_search/view_search_content.dart';

import '../../../constants/colors.dart';
import '../../../constants/size.dart';
import '../../../constants/styles.dart';
import '../dropdowns/drop_list_model.dart';

class DropdownSearch extends StatefulWidget implements FormObject {
  DropdownSearch(
      {Key? key,
      this.title = "",
      this.heightView,
      required this.items,
      // required this.context,
      this.isSearch = false,
      this.callback,
      this.titleDrop = "",
      this.hindText,
      this.childPopup,
      this.isRequail = false,
      this.initItem,
      this.textValidate,
      this.hintText = "",
      this.isEdit = false,
      this.control})
      : super(key: key);

  String title;
  bool isRequail = false;
  // BuildContext context;
  CallbackData? callback;
  List<OptionItem> items = [];
  double? heightView;
  bool isSearch = false;
  String titleDrop = "";
  String hintText = "";
  String? hindText;
  Widget? childPopup;
  OptionItem? initItem;
  String? textValidate;
  bool isEdit = false;

  @override
  Control? control;

  @override
  State<DropdownSearch> createState() => _DropdownSearchState();

  @override
  void callReloadView() {}
}

class _DropdownSearchState extends State<DropdownSearch> {
  Control? control;
  var textValidate = "".obs;
  // OptionItem? item;
  double heightView = Get.height - 100;
  TextEditingController controller = TextEditingController(text: "");
  @override
  void initState() {
    super.initState();
    if (widget.heightView != null) {
      heightView = widget.heightView ?? Get.height - 100;
    }
    // control = widget.control;
    // item = widget.initItem;
  }

  @override
  Widget build(BuildContext context) {
    controller.text =
        widget.initItem == null ? "" : (widget.initItem!.title.tr);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // if (widget.title.isNotEmpty)
        //   titleView(title: widget.title.tr, isRequail: widget.isRequail),
        // if (widget.title.isNotEmpty)
        //   const SizedBox(
        //     height: 6,
        //   ),
        widget.isEdit == true ? inputView() : viewNotEdit(),
        Obx(() => Container(
              padding:
                  EdgeInsets.only(top: textValidate.value.isNotEmpty ? 4 : 0),
              child: Text(
                textValidate.value,
                style: normalStyleValidate.copyWith(
                    color: red_600,
                    fontSize: textValidate.value.isNotEmpty ? 11 : 0),
              ),
            )),
        const SizedBox(
          height: 16,
        ),
        // if (textValidate.value.isNotEmpty)
        //   const SizedBox(
        //     height: 8,
        //   ),
        // Obx(() => Text(
        //       textValidate.value,
        //       style: normalStyleValidate.copyWith(color: red_600),
        //     ))
      ],
    );
  }

  Widget inputView() {
    return SizedBox(
      height: 50,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: grayconst_100,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              width: 10,
            ),

            Expanded(
              child: TextFormField(
                controller: controller,
                // initialValue: item == null ? "" : (item!.title),
                style: normalStyle.copyWith(
                  fontSize: fontSizeNomal,
                  color: neutral_500,
                  fontWeight: FontWeight.w400,
                ),
                onChanged: (value) {
                  widget.initItem?.title = value;
                  if (widget.callback != null) {
                    widget.callback!(widget.initItem);
                  }
                },
                validator: (value) {
                  // if (value!.isEmpty) {
                  //   textValidate.value = widget.textValidate ?? "";
                  // }
                  return onValidate(value);
                },
                decoration: InputDecoration(
                    isDense: true,
                    label: titleView(
                        title: (widget.title).tr, isRequail: widget.isRequail),
                    // labelText: (widget.title ?? "").tr,
                    contentPadding: const EdgeInsets.only(left: 5),
                    enabledBorder: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    // hintText: (widget.hindText == null
                    //     ? "selectitem".tr
                    //     : widget.hindText!.tr),
                    labelStyle: normalStyle.copyWith(
                        color: neutral_400, fontSize: fontSize16),
                    focusColor: neutral_400,
                    border: InputBorder.none,
                    errorMaxLines: 1,
                    errorStyle: const TextStyle(
                        fontSize: 0, height: 0.8, color: red_500),
                    errorBorder: InputBorder.none),
              ),
            ),
            // const Icon(Icons.visibility_rounded, color: unselectedTextColor)

            InkWell(
              onTap: () {
                showDropDown();
              },
              child: const Icon(
                Icons.expand_more,
                color: neutral_400,
                size: 26,
              ),
            ),

            const SizedBox(width: 10)
          ],
        ),
      ),
    );
  }

  Widget viewNotEdit() {
    return InkWell(
      onTap: () {
        showDropDown();
      },
      child: Container(
        height: 50,
        padding: const EdgeInsets.only(left: 10, right: 10),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: grayconst_100,
        ),
        child: Row(
          children: [
            Expanded(
                child: TextFormField(
                    controller: controller,
                    // initialValue: item == null ? "" : (item!.title),
                    validator: (value) {
                      // if (value!.isEmpty) {
                      //   textValidate.value = widget.textValidate ?? "";
                      // }
                      onValidate(value);
                      return null;
                    },
                    enabled: false,
                    decoration: InputDecoration(
                        isDense: true,
                        label: titleView(
                            title: (widget.title).tr,
                            isRequail: widget.isRequail),
                        // labelText: (widget.title ?? "").tr,
                        // hintText: (widget.hindText == null
                        //     ? "selectitem".tr
                        //     : widget.hindText!.tr),
                        contentPadding: const EdgeInsets.only(left: 5),
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        labelStyle: normalStyle.copyWith(
                            color: neutral_400, fontSize: fontSize16),
                        focusColor: neutral_400,
                        border: InputBorder.none,
                        errorMaxLines: 1,
                        errorStyle: const TextStyle(
                            fontSize: 0, height: 0.8, color: red_500),
                        errorBorder: InputBorder.none),
                    // decoration: const InputDecoration(
                    //     isCollapsed: true, border: InputBorder.none),
                    style: normalStyle)),
            // Text(
            //     item == null
            //         ? (widget.hindText == null
            //             ? "selectitem".tr
            //             : widget.hindText!.tr)
            //         : (item!.title),
            //     style: normalStyle.copyWith(fontSize: fontSizeNomal)),

            const Icon(
              Icons.expand_more,
              color: neutral_400,
              size: 26,
            )
          ],
        ),
      ),
    );
  }

  showDropDown() {
    FocusScope.of(context).unfocus();
    var heightView = this.heightView;
    if (control != null) {
      heightView = (control!.listData.length * 42 + 150);
    }
    if (widget.items.isNotEmpty) {
      heightView = (widget.items.length * 42 + 150);
    }
    if (heightView <= 330) {
      heightView = 330;
    }
    if (heightView >= Get.height - 150) {
      heightView = Get.height - 150;
    }
    if (widget.isSearch == true) {
      heightView = Get.height - 150;
    }
    showModalBottomSheet<void>(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return SizedBox(
            height: heightView,
            child: ViewSearchContent(
                isSearch: widget.isSearch,
                title: widget.titleDrop,
                childPopup: widget.childPopup,
                listData: control == null ? [] : control!.listData,
                items: widget.items,
                callback: (value) {
                  widget.initItem = value;
                  controller.text = widget.initItem?.title ?? "";
                  if (widget.callback != null) {
                    widget.callback!(value);
                  }
                  // setState(() {});
                }),
          );
        });
  }

  Widget titleView({required String title, bool isRequail = false}) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
              text: title,
              style: normalStyle.copyWith(
                color: neutral_500,
                fontWeight: FontWeight.w400,
              )),
          TextSpan(
              text: isRequail ? '* ' : ' ',
              style: normalStyle.copyWith(
                color: red_600,
                fontWeight: FontWeight.w500,
              )),
        ],
      ),
    );
  }

  onValidate(value) {
    if (widget.isRequail) {
      if (value.toString().isEmpty) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }
    }
    return null;
  }
}
