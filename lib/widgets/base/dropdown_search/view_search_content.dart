import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/icons.dart';

import '../../../constants/colors.dart';
import '../../../constants/size.dart';
import '../../../constants/styles.dart';
import '../../../extensions/tiengviet/tiengviet.dart';
import '../dropdowns/drop_list_model.dart';

class ViewSearchContent extends StatefulWidget {
  ViewSearchContent(
      {Key? key,
      this.items,
      required this.callback,
      this.isSearch = true,
      this.title = "",
      this.childPopup,
      required this.listData})
      : super(key: key);
  List<OptionItem>? items;
  final CallbackData callback;
  bool isSearch = true;
  Widget? childPopup;
  String title = "";
  final List listData;

  @override
  State<ViewSearchContent> createState() => _ViewSearchContentState();
}

class _ViewSearchContentState extends State<ViewSearchContent> {
  List<OptionItem> items = [];

  @override
  void initState() {
    super.initState();
    // items = widget.items;
    for (var element in widget.listData) {
      OptionItem option = OptionItem(id: "0", title: element['MA'].toString());
      items.add(option);
    }
    if (widget.items != null) {
      items = widget.items ?? [];
    }
    // items = listData.map(OptionItem)
  }

  @override
  Widget build(BuildContext context) {
    return buildViewContent();
  }

  Widget buildViewContent() {
    return Container(
      color: Colors.transparent,
      child: Container(
        padding: paddingLeftRight.copyWith(top: 24),
        decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0), topRight: Radius.circular(20.0)),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            widget.isSearch == true ? renderSearchTool() : viewTitle(),
            const SizedBox(
              height: 20,
            ),
            Expanded(
              child: widget.childPopup != null
                  ? widget.childPopup!
                  : ListView.builder(
                      itemCount: items.length,
                      itemBuilder: (context, index) {
                        return viewItemSelect(item: items[index]);
                      },
                    ),
            )
          ],
        ),
      ),
    );
  }

  Widget viewTitle() {
    return SizedBox(
        height: 50,
        child: Stack(
          children: [
            Positioned(
                top: 16,
                left: 20,
                child: InkWell(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: const Icon(
                    Icons.close,
                    color: neutral_400,
                    size: 20,
                  ),
                )),
            Positioned(
                top: 16,
                left: 20,
                right: 20,
                child: Center(
                    child: Text(
                  widget.title.tr,
                  style: boldStyle.copyWith(
                      fontSize: fontSize16, color: neutral_500),
                )))
          ],
        ));
  }

  Widget renderSearchTool() {
    return Container(
      height: 50,
      padding: paddingLeftRight,
      decoration: BoxDecoration(
          color: grayconst_100, borderRadius: BorderRadius.circular(8)),
      child: Row(children: [
        SvgPicture.asset(
          iconSearch,
          color: neutral_400,
        ),
        const SizedBox(
          width: 6,
        ),
        Expanded(
            child: TextFormField(
          onChanged: (value) {
            _searchObject(value);
          },
          decoration: InputDecoration(
              isCollapsed: true,
              border: InputBorder.none,
              errorMaxLines: 1,
              errorStyle:
                  const TextStyle(fontSize: 0, height: 0.8, color: red_500),
              hintText: 'find'.tr),
          style: const TextStyle(
            fontSize: fontSizeNomal,
            fontFamily: fontFamilyMain,
            color: neutral_400,
            fontWeight: FontWeight.w400,
          ),
        )),
      ]),
    );
  }

  Widget viewItemSelect({required OptionItem item}) {
    return InkWell(
      onTap: () {
        Navigator.of(context).pop();
        widget.callback(item);
      },
      child: Container(
        constraints: const BoxConstraints(minHeight: 40),
        child: Column(
          crossAxisAlignment: widget.isSearch == true
              ? CrossAxisAlignment.start
              : CrossAxisAlignment.center,
          children: [
            Text(item.title,
                textAlign: widget.isSearch == true
                    ? TextAlign.start
                    : TextAlign.center,
                style: const TextStyle(
                    fontSize: fontSizeNomal, color: Colors.black)),
            const SizedBox(
              height: 10,
            ),
            // widget.isSearch == true
            //     ? Container(
            //         color: Colors.grey,
            //         height: 0.6,
            //       )
            //     : Container()
          ],
        ),
      ),
    );
  }

  // Widget dialogContent(BuildContext context) {
  //   return Container(
  //     margin: const EdgeInsets.only(left: 0.0, right: 0.0),
  //     child: Stack(
  //       children: <Widget>[
  //         Container(
  //           padding: const EdgeInsets.only(
  //             top: 18.0,
  //           ),
  //           margin: const EdgeInsets.only(top: 13.0, right: 8.0),
  //           decoration: BoxDecoration(
  //               color: gray_500,
  //               shape: BoxShape.rectangle,
  //               borderRadius: BorderRadius.circular(6.0),
  //               boxShadow: const <BoxShadow>[
  //                 BoxShadow(
  //                   color: Colors.black26,
  //                   blurRadius: 0.0,
  //                   offset: Offset(0.0, 0.0),
  //                 ),
  //               ]),
  //           child: buildViewContent(),
  //         ),
  //         Positioned(
  //           right: 0.0,
  //           child: GestureDetector(
  //             onTap: () {
  //               Navigator.of(context).pop();
  //             },
  //             child: const Align(
  //               alignment: Alignment.topRight,
  //               child: CircleAvatar(
  //                 radius: 14.0,
  //                 backgroundColor: Colors.white,
  //                 child: Icon(Icons.close, color: Colors.red),
  //               ),
  //             ),
  //           ),
  //         ),
  //       ],
  //     ),
  //   );
  // }

  _searchObject(String text) {
    if (text.isEmpty) {
      setState(() {
        items = widget.items ?? [];
      });
      return;
    }
    setState(() {
      items = (widget.items ?? [])
          .where((element) => TiengViet.parse(element.title.toLowerCase())
              .contains(TiengViet.parse(text.toLowerCase())))
          .toList();
    });
  }
}
