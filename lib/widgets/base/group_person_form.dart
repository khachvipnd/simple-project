import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';

import '../../constants/colors.dart';
import '../../constants/size.dart';

class GroupPersonForm extends StatefulWidget implements FormObject {
  GroupPersonForm(
      {Key? key, this.onChangeCallback, this.control, colorText = neutral_500});

  OnChangeCallBack? onChangeCallback;
  OnChangeCallBack? onEditingComplete;
  Color? colorText;
  @override
  Control? control;

  List<dynamic> lsPerson = [];
  @override
  State<GroupPersonForm> createState() => _GroupPersonForm();

  @override
  void callReloadView() {
    // TODO: implement callReloadView
  }
}

class _GroupPersonForm extends State<GroupPersonForm> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.control!.style != {}) {
      widget.colorText = HexColor.fromHex(widget.control!.style['textColor']);
    }
  }

  @override
  Widget build(BuildContext context) {
    // String widget2 = widget.title;
    return Obx(() => !widget.control!.visible.value || widget.lsPerson.isEmpty
        ? Container(
            child: null,
          )
        : Expanded(
            flex: widget.control!.flex,
            child: Padding(
              padding: EdgeInsets.only(
                  left: widget.control!.indexRow > 0 ? 10 : 16,
                  top: 10,
                  right: widget.control!.isLastRow ? 16 : 0),
              child: Text(
                widget.control!.title.value,
                textAlign: TextAlign.left,
                style: boldStyle.copyWith(
                  color: widget.colorText,
                  fontSize: fontSize,
                ),
              ),
            )));
  }

  Widget renderView() {
    return Container();
  }
}
