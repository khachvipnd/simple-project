import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../constants/callbacks.dart';
import '../../constants/colors.dart';
import '../../constants/size.dart';
import '../../constants/styles.dart';
import 'package:yourpti/extensions/string_extension.dart';

// ignore: must_be_immutable
class TextFormWidget extends StatefulWidget {
  // ignore: use_key_in_widget_constructors
  TextFormWidget(
      {Key? key,
      this.callback,
      this.hintText = "",
      this.maxLines,
      this.controller,
      this.callbackAction,
      this.icon,
      this.iconAction,
      this.title = "",
      this.textValidate,
      this.titlefather,
      this.styleFather = boldStyle,
      this.isRequail = false,
      this.obscureText = false,
      this.isFormatPass = false,
      this.enabled = true,
      this.heightView = 90,
      this.keyboardType = TextInputType.text});
  CallbackActionString? callback;
  CallbackAction? callbackAction;
  String? titlefather;
  TextStyle styleFather = boldStyle;
  String? content;
  bool isFormatPass = false;
  TextEditingController? controller;
  String hintText = "";
  int? maxLines;
  String? icon;
  double heightView = 90;
  bool isRequail = false;
  String? title;
  String? iconAction;
  bool enabled = true;
  String? textValidate;
  TextInputType keyboardType;

  bool obscureText = false;

  @override
  State<TextFormWidget> createState() => _TextFormWidgetState();
}

class _TextFormWidgetState extends State<TextFormWidget> {
  var textValidate = "".obs;
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // titleView(
        //     title: (widget.title ?? "").tr,
        //     isRequail: widget.isRequail,
        //     titleFather: widget.titlefather),

        inputView(),
        // if (textValidate.value.isNotEmpty)
        Obx(() => Container(
              padding:
                  EdgeInsets.only(top: textValidate.value.isNotEmpty ? 4 : 0),
              child: Text(
                textValidate.value,
                style: normalStyleValidate.copyWith(
                    color: red_600,
                    fontSize: textValidate.value.isNotEmpty ? 11 : 0),
              ),
            )),
        const SizedBox(
          height: 16,
        ),
        // Obx(() => )
      ],
    );
  }

  Widget inputView() {
    return SizedBox(
      height: widget.maxLines == null ? 50 : widget.heightView,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: grayconst_100,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const SizedBox(
              width: 10,
            ),
            if (widget.icon != null)
              SvgPicture.asset(
                widget.icon ?? "",
                height: 18,
                width: 18,
              ),
            if (widget.icon != null)
              const SizedBox(
                width: 10,
              ),
            widget.obscureText == true
                ? textFilePass()
                : Expanded(
                    child: Center(
                      child: TextFormField(
                        enabled: widget.enabled,
                        controller: widget.controller,
                        // initialValue: widget.content,
                        maxLines: widget.maxLines,
                        keyboardType: widget.keyboardType,
                        style: normalStyle.copyWith(
                          fontSize: fontSizeNomal,
                          color: neutral_500,
                          fontWeight: FontWeight.w400,
                        ),
                        onChanged: (value) {
                          if (widget.callback != null) {
                            widget.callback!(value);
                          }
                        },
                        validator: (value) {
                          // if (value!.isEmpty) {
                          //   textValidate.value = widget.textValidate ?? "";
                          // }
                          return onValidate(value);
                        },
                        onFieldSubmitted: (value) {
                          FocusScope.of(context).unfocus();
                        },
                        decoration: InputDecoration(
                            isDense: true,
                            label: titleView(
                                title: (widget.title ?? "").tr,
                                isRequail: widget.isRequail,
                                titleFather: widget.titlefather),
                            // labelText: (widget.title ?? "").tr,
                            enabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                            contentPadding: const EdgeInsets.only(left: 5),
                            border: InputBorder.none,
                            labelStyle: normalStyle.copyWith(
                                color: neutral_400, fontSize: fontSize16),
                            focusColor: neutral_400,
                            errorMaxLines: 1,
                            errorStyle: const TextStyle(
                                fontSize: 0, height: 0.8, color: red_500),
                            errorBorder: InputBorder.none,
                            // decoration: InputDecoration(
                            //     isCollapsed: true,
                            //     border: InputBorder.none,
                            //     errorMaxLines: 1,
                            //     errorStyle: const TextStyle(
                            //         fontSize: 0, height: 0.8, color: red_500),
                            hintText: (widget.hintText).tr),
                      ),
                    ),
                  ),
            // const Icon(Icons.visibility_rounded, color: unselectedTextColor)

            if (widget.iconAction != null)
              InkWell(
                onTap: () {
                  if (widget.callbackAction != null) {
                    widget.callbackAction;
                  }
                },
                child: SvgPicture.asset(
                  widget.iconAction ?? "",
                  height: 18,
                  width: 18,
                ),
              ),

            if (widget.iconAction != null) const SizedBox(width: 10)
          ],
        ),
      ),
    );
  }

  Widget textFilePass() {
    return Expanded(
      child: Center(
        child: TextFormField(
          obscureText: widget.obscureText,
          controller: widget.controller,
          keyboardType: widget.keyboardType,
          style: normalStyle.copyWith(
            fontSize: fontSizeNomal,
            color: neutral_500,
            fontWeight: FontWeight.w400,
          ),
          onChanged: (value) {
            if (widget.callback != null) {
              widget.callback!(value);
            }
          },
          validator: (value) {
            // if (value!.isEmpty) {
            //   textValidate.value = widget.textValidate ?? "";
            // }
            return onValidate(value);
          },
          decoration: InputDecoration(
              isDense: true,
              label: titleView(
                  title: (widget.title ?? "").tr,
                  isRequail: widget.isRequail,
                  titleFather: widget.titlefather),
              // labelText: (widget.title ?? "").tr,
              enabledBorder: InputBorder.none,
              focusedBorder: InputBorder.none,
              contentPadding: const EdgeInsets.only(left: 5),
              border: InputBorder.none,
              labelStyle: normalStyle.copyWith(
                  color: neutral_400, fontSize: fontSize16),
              focusColor: neutral_400,
              errorMaxLines: 1,
              errorStyle:
                  const TextStyle(fontSize: 0, height: 0.8, color: red_500),
              errorBorder: InputBorder.none),
          // decoration: InputDecoration(
          //     isCollapsed: true,
          //     border: InputBorder.none,
          //     errorMaxLines: 1,
          //     errorStyle: const TextStyle(
          //         fontSize: 0, height: 0.8, color: red_500),
          //     hintText: (widget.hintText).tr),
        ),
      ),
    );
  }

  Widget titleView(
      {required String title, String? titleFather, bool isRequail = false}) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          if (titleFather != null)
            TextSpan(text: titleFather, style: widget.styleFather),
          TextSpan(
              text: title,
              style: normalStyle.copyWith(
                color: neutral_500,
                fontWeight: FontWeight.w400,
              )),
          TextSpan(
              text: isRequail ? '* ' : ' ',
              style: normalStyle.copyWith(
                color: red_600,
                fontWeight: FontWeight.w500,
              )),
        ],
      ),
    );
  }

  String? onValidate(value) {
    if (widget.isRequail) {
      if (widget.keyboardType == TextInputType.phone &&
          !value.toString().isValidPhone()) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }
      if (widget.keyboardType == TextInputType.emailAddress &&
          !value.toString().isValidEmail()) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }

      if (widget.obscureText == true &&
          !value.toString().validatePassword() &&
          widget.isFormatPass) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }
      if (value.toString().isEmpty) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }
      textValidate.value = "";
      return null;
    }
    if (widget.keyboardType == TextInputType.phone &&
        !value.toString().isValidPhone()) {
      textValidate.value = (widget.textValidate ?? "").tr;
      return "";
    }
    if (widget.keyboardType == TextInputType.emailAddress &&
        !value.toString().isValidEmail()) {
      textValidate.value = (widget.textValidate ?? "").tr;
      return "";
    }
    return null;
  }
}
