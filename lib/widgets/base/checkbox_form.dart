import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get_state_manager/src/rx_flutter/rx_obx_widget.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';

import '../../constants/colors.dart';
import '../../constants/icons.dart';
import '../../constants/size.dart';

class CheckBoxForm extends StatefulWidget implements FormObject {
  CheckBoxForm(
      {Key? key, this.onChangeCallback, this.control, this.onEditingComplete});

  OnChangeCallBack? onChangeCallback;
  OnChangeCallBack? onEditingComplete;

  @override
  Control? control;

  @override
  State<CheckBoxForm> createState() => _CheckBoxForm();

  @override
  void callReloadView() {}
}

class _CheckBoxForm extends State<CheckBoxForm> {
  Control? control;

  @override
  void initState() {
    super.initState();
    control = widget.control;
  }

  @override
  Widget build(BuildContext context) {
    // String widget2 = widget.title;
    return Obx(() => !widget.control!.visible.value
        ? Container(
            child: null,
          )
        : Expanded(
            flex: widget.control!.flex,
            child: Padding(
                padding: EdgeInsets.only(
                    left: widget.control!.indexRow > 0 ? 10 : 16,
                    top: 10,
                    right: widget.control!.isLastRow ? 16 : 0),
                child: Obx(
                  () => InkWell(
                      onTap: () {
                        if (widget.control!.enabled.value) {
                          widget.control!.value.value == "K"
                              ? widget.control!.value.value = "C"
                              : widget.control!.value.value = "K";
                        }
                        if (widget.onChangeCallback != null) {
                          widget.onChangeCallback!(widget);
                        }
                        if (widget.onEditingComplete != null) {
                          widget.onEditingComplete!(widget);
                        }
                      },
                      child: Row(
                        children: [
                          widget.control!.value.value == 'C'
                              ? SvgPicture.asset(iconCheckboxSelected)
                              : SvgPicture.asset(iconCheckbox),
                          Padding(
                            padding: const EdgeInsets.only(left: 7),
                            child: Text(
                              widget.control!.title.value,
                              textAlign: TextAlign.left,
                              style: normalStyle.copyWith(
                                color: neutral_500,
                                fontSize: fontSize,
                                // fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      )),
                ))));
    // decoration: InputDecoration(;
    //   labelText: widget.control!.title ?? "",
    // ),
    // initialValue: widget.control!.defaultValue,
    // onChanged: (String? value) {
    //   // debugprintDebug('"$value');
    //   control?.value = value!;

    //   if (widget.onChangeCallback != null) widget.onChangeCallback!(this);
    // },
    // onSaved: (String? value) {
    //   // printDebug('"$value"');
    //   // printDebug('"$value"');
    // },
    // onEditingComplete: () {
    //   if (widget.onEditingComplete != null) widget.onEditingComplete!(this);
    // },
    // );
  }
}
