import 'package:flutter/material.dart';

import '../../constants/colors.dart';

class ViewShadow extends StatelessWidget {
  ViewShadow(
      {Key? key,
      required this.views,
      this.offset = const Offset(-1, -2),
      this.radius = 0,
      this.blurRadius = 5,
      this.colorShadow = colorShadown,
      this.height = 80.0,
      this.width,
      this.padding = true})
      : super(key: key);
  List<Widget> views = [];
  Offset offset;
  double blurRadius = 5;
  double radius = 0;
  double height = 80.0;
  double? width;
  bool padding = true;
  Color colorShadow = colorShadown;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: height,
      width: width,
      padding: padding ? const EdgeInsets.only(left: 16, right: 16) : null,
      //Same as `blurRadius` i guess
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(radius),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: colorShadow,
            offset: offset, //(x,y)
            blurRadius: blurRadius,
            spreadRadius: 0.5,
          ),
        ],
      ),
      child: Row(children: views),
    );
  }
}
