import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/extensions/string_extension.dart';
import '../../constants/colors.dart';
import 'package:yourpti/extensions/date.dart';
import 'date_picker/flutter_datetime_picker.dart';

class TextFormBorderWidget extends StatefulWidget {
  TextFormBorderWidget(
      {Key? key,
      this.callback,
      this.hintText = "",
      this.maxLines,
      this.controller,
      this.callbackAction,
      this.isTimePicker = false,
      this.icon,
      this.iconAction,
      this.isRequail = false,
      this.obscureText = false,
      this.callbackDateTime,
      this.textValidate,
      this.initDate,
      this.enabled = true,
      this.isDate = false,
      this.isLogin = false,
      this.colorsView = grayconst_100,
      this.keyboardType = TextInputType.text,
      this.textAlign = TextAlign.left,
      this.height = 44});
  CallbackActionString? callback;
  VoidCallback? callbackAction;
  String? content;
  bool isLogin = false;
  TextEditingController? controller;
  CallbackActionDate? callbackDateTime;
  String hintText = "";
  int? maxLines;
  String? icon;
  bool enabled = true;

  String? iconAction;
  bool isRequail = false;
  String? textValidate;
  bool isTimePicker = false;
  bool isDate = false;
  String? initDate;
  TextInputType keyboardType;
  bool obscureText = false;
  Color colorsView = grayconst_100;
  TextAlign textAlign = TextAlign.left;
  double height = 44;
  @override
  State<TextFormBorderWidget> createState() => _TextFormBorderWidgetState();
}

class _TextFormBorderWidgetState extends State<TextFormBorderWidget> {
  bool obscureText = false;
  var textValidate = "".obs;
  DateTime initDate = DateTime.now();
  TextEditingController controller = TextEditingController();
  @override
  void initState() {
    super.initState();
    obscureText = widget.obscureText;
    if (widget.initDate != null) {
      controller.text = widget.initDate!;
      initDate =
          widget.initDate!.converStringToDate(dateFormat: "dd/MM/yyyy") ??
              DateTime.now();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        widget.isDate == false ? inputView() : dateTimeView(),
        if (textValidate.value.isNotEmpty)
          const SizedBox(
            height: 8,
          ),
        Obx(() => Text(
              textValidate.value,
              style: normalStyleValidate.copyWith(color: red_600),
            ))
      ],
    );
  }

  Widget inputView() {
    return SizedBox(
      height: widget.height,
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: widget.colorsView,
        ),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              width: widget.textAlign == TextAlign.center ? 0 : 17,
            ),
            if (widget.icon != null)
              SvgPicture.asset(
                widget.icon ?? "",
                height: 20,
                width: 20,
              ),
            if (widget.icon != null)
              const SizedBox(
                width: 10,
              ),
            Expanded(
              child: Center(
                child: TextFormField(
                  textAlign: widget.textAlign,
                  controller: widget.controller,
                  enabled: widget.enabled,
                  // initialValue: widget.content,
                  // maxLines: widget.obscureText ? null : widget.maxLines,
                  keyboardType: widget.keyboardType,
                  obscureText: obscureText,
                  style: normalStyle,
                  onChanged: (value) {
                    if (widget.callback != null) {
                      widget.callback!(value);
                    }
                  },
                  validator: (value) {
                    return onValidate(value ?? "");
                  },

                  decoration: InputDecoration(
                      isCollapsed: true,
                      border: InputBorder.none,
                      errorStyle: const TextStyle(
                          fontSize: 0, height: 0.8, color: red_500),
                      errorBorder: InputBorder.none,
                      hintText: (widget.hintText).tr),
                ),
              ),
            ),
            // const Icon(Icons.visibility_rounded, color: unselectedTextColor)
            if (widget.obscureText)
              InkWell(
                  onTap: () {
                    setState(() {
                      obscureText = !obscureText;
                    });
                  },
                  child: obscureText == false
                      ? const Icon(Icons.visibility_rounded, color: neutral_400)
                      : const Icon(Icons.visibility_off_rounded,
                          color: neutral_400)),

            if (widget.obscureText) const SizedBox(width: 17),
            if (widget.iconAction != null)
              InkWell(
                onTap: () {
                  if (widget.callbackAction != null) {
                    widget.callbackAction!();
                  }
                },
                child: SvgPicture.asset(
                  widget.iconAction ?? "",
                  height: 20,
                  width: 20,
                ),
              ),

            if (widget.iconAction != null) const SizedBox(width: 17)
          ],
        ),
      ),
    );
  }

  Widget dateTimeView() {
    return InkWell(
        onTap: () {
          showDatePicker();
        },
        child: SizedBox(
          height: 44,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: grayconst_100,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  width: 17,
                ),
                if (widget.icon != null)
                  SvgPicture.asset(
                    widget.icon ?? "",
                    height: 20,
                    width: 20,
                  ),
                if (widget.icon != null)
                  const SizedBox(
                    width: 10,
                  ),
                Expanded(
                  child: Center(
                    child: TextFormField(
                      controller: controller,
                      enabled: false,
                      keyboardType: widget.keyboardType,
                      obscureText: obscureText,
                      style: normalStyle,
                      onChanged: (value) {
                        if (widget.callback != null) {
                          widget.callback!(value);
                        }
                      },
                      validator: (value) {
                        return onValidate(value ?? "");
                      },
                      decoration: InputDecoration(
                          isCollapsed: true,
                          // hi: titleView(
                          //     title: (widget.hintText).tr,
                          //     isRequail: widget.isRequail),
                          border: InputBorder.none,
                          errorStyle: const TextStyle(
                              fontSize: 0, height: 0.8, color: red_500),
                          errorBorder: InputBorder.none,
                          hintText: (widget.hintText).tr),
                    ),
                  ),
                ),
                // const Icon(Icons.visibility_rounded, color: unselectedTextColor)
                if (widget.obscureText)
                  InkWell(
                      onTap: () {
                        setState(() {
                          obscureText = !obscureText;
                        });
                      },
                      child: obscureText == false
                          ? const Icon(Icons.visibility_rounded,
                              color: neutral_400)
                          : const Icon(Icons.visibility_off_rounded,
                              color: neutral_400)),

                if (widget.obscureText) const SizedBox(width: 17),
                if (widget.iconAction != null)
                  InkWell(
                    onTap: () {
                      if (widget.callbackAction != null) {
                        widget.callbackAction!();
                      }
                    },
                    child: SvgPicture.asset(
                      widget.iconAction ?? "",
                      height: 20,
                      width: 20,
                    ),
                  ),

                if (widget.iconAction != null) const SizedBox(width: 17)
              ],
            ),
          ),
        ));
  }

  Widget titleView({required String title, bool isRequail = false}) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
              text: title,
              style: normalStyle.copyWith(
                color: neutral_500,
                fontWeight: FontWeight.w400,
              )),
          TextSpan(
              text: isRequail ? '* ' : ' ',
              style: normalStyle.copyWith(
                color: red_600,
                fontWeight: FontWeight.w500,
              )),
        ],
      ),
    );
  }

  showDatePicker() {
    if (widget.isTimePicker) {
      DatePickerPTI.showTimePicker(context,
          showSecondsColumn: false,
          showTitleActions: true,
          currentTime: initDate, onChanged: (date) {
        // printDebug('change $date');
      }, onConfirm: (date) {
        // printDebug('confirm $date');

        var dateString = date.formatTimeToString();
        controller.text = dateString;
        initDate = date;

        if (widget.callbackDateTime != null) {
          widget.callbackDateTime!(date, dateString);
        }
      }, locale: LocaleType.vi);
    } else {
      DatePickerPTI.showDatePicker(context,
          showTitleActions: true, currentTime: initDate, onChanged: (date) {
        // printDebug('change $date');
      }, onConfirm: (date) {
        // printDebug('confirm $date');
        var dateString = date.formatDateToString();
        controller.text = dateString;
        initDate = date;
        if (widget.callbackDateTime != null) {
          widget.callbackDateTime!(date, dateString);
        }
      }, locale: LocaleType.vi);
    }
  }

  String? onValidate(String value) {
    if (widget.isRequail) {
      if (widget.keyboardType == TextInputType.phone &&
          !value.toString().isValidPhone()) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }
      if (widget.keyboardType == TextInputType.emailAddress &&
          !value.toString().isValidEmail()) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }
      // if (widget.isLogin && widget.obscureText == true) {
      //   textValidate.value = (widget.textValidate ?? "").tr;
      //   return "";
      // }

      if (widget.obscureText == true &&
          !value.toString().validatePassword() &&
          !widget.isLogin) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }
      if (value.toString().isEmpty) {
        textValidate.value = (widget.textValidate ?? "").tr;
        return "";
      }
      textValidate.value = "";
      return null;
    }
    return null;
  }
}
