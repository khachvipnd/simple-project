import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';
// import 'package:yourpti/widgets/dropdowns/drop_list_model.dart';

import '../../../constants/colors.dart';
import '../../../constants/styles.dart';
import '../../constants/icons.dart';

class ButtonUploadImageForm extends StatefulWidget implements FormObject {
  ButtonUploadImageForm(
      {Key? key,
      this.title = "",
      this.heightView,

      // required this.context,
      this.callback,
      this.control,
      this.onChangeCallback,
      this.onEditingComplete})
      : super(key: key);

  String title;
  // BuildContext context;
  OnChangeCallBack? onChangeCallback;
  OnChangeCallBack? onEditingComplete;
  CallbackData? callback;
  RxBool uploading = false.obs;
  double? heightView;
  TextEditingController controller = TextEditingController(text: "");
  @override
  Control? control;

  @override
  State<ButtonUploadImageForm> createState() => _ButtonUploadImageFormState();
  var textValidate = "".obs;
  @override
  void callReloadView() {}
}

class _ButtonUploadImageFormState extends State<ButtonUploadImageForm> {
  double widthView = (Get.width / 2) * 0.7;
  double? heightView;
  @override
  void initState() {
    super.initState();
    heightView = widthView * (9 / 16);
    widget.controller.text = widget.control!.value.value;
  }

  Widget inputView() {
    return InkWell(
        onTap: () {
          if (widget.onChangeCallback != null) {
            widget.onChangeCallback!(widget);
          }
          if (widget.onEditingComplete != null) {
            widget.onEditingComplete!(widget);
          }
        },
        child: Center(
          child: Container(
              height: heightView,
              width: widthView,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: widget.control!.enabled.value
                    ? grayconst_100
                    : secondary_300,
              ),
              child: widget.control!.value.value != ""
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(8),
                      child: CachedNetworkImage(
                        imageUrl: widget.control!.value.value,
                        fit: BoxFit.fill,
                      ))
                  : Center(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          SvgPicture.asset(iconCamera3),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            widget.control!.title.value,
                            style: normalStyle.copyWith(),
                          )
                        ],
                      ),
                    )),
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => !widget.control!.visible.value
        ? Container(
            child: null,
          )
        : Expanded(
            flex: widget.control!.flex,
            child: Padding(
                padding: EdgeInsets.only(
                    left: widget.control!.indexRow > 0 ? 10 : 16,
                    top: 10,
                    right: widget.control!.isLastRow ? 16 : 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // if (widget.title.isNotEmpty)
                    //   titleView(title: widget.title.tr, isRequail: widget.isRequail),
                    // if (widget.title.isNotEmpty)
                    //   const SizedBox(
                    //     height: 6,
                    //   ),
                    inputView(),
                    if (widget.textValidate.value.isNotEmpty)
                      const SizedBox(
                        height: 4,
                      ),
                    // if (textValidate.value.isNotEmpty)
                    Obx(() => Text(
                          widget.textValidate.value,
                          style: normalStyleValidate.copyWith(color: red_600),
                        ))
                  ],
                ))));
  }

  Widget titleView({required String title, bool isRequail = false}) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
              text: title,
              style: const TextStyle(
                  fontFamily: "Helveticaneue",
                  color: neutral_500,
                  fontWeight: FontWeight.w400,
                  fontSize: fontSizeNomal)),
          TextSpan(
              text: isRequail ? '* ' : ' ',
              style: const TextStyle(
                  fontFamily: "Helveticaneue",
                  fontWeight: FontWeight.w500,
                  fontSize: fontSizeNomal,
                  color: red_600)),
        ],
      ),
    );
  }
}
