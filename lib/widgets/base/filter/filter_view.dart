import 'package:flutter/material.dart';
import 'package:yourpti/widgets/base/dropdowns/drop_list_model.dart';

import '../../../constants/callbacks.dart';
import '../../../constants/colors.dart';
import '../../../constants/size.dart';
import '../../../constants/styles.dart';

class FilterView extends StatefulWidget {
  FilterView(
      {Key? key,
      // required this.scrollController,
      this.callback,
      required this.lsSearch,
      this.colorTabSelected,
      this.colorTextSelect,
      this.colorText,
      this.colorTab})
      : super(key: key);
  CallbackData? callback;
  List<OptionItem>? lsSearch;
  Color? colorTextSelect;
  Color? colorText;
  Color? colorTabSelected;
  Color? colorTab;
  @override
  State<FilterView> createState() => _FilterViewState();
}

class _FilterViewState extends State<FilterView> {
  ScrollController scrollController = ScrollController();
  @override
  void initState() {
    super.initState();
    scrollController = ScrollController();
    // scrollController!.animateTo(scrollController!.position.maxScrollExtent,
    //     duration: const Duration(milliseconds: 500), curve: Curves.ease);
  }

  Widget renderItemTab(OptionItem data, int index) {
    return SizedBox(
      height: 36,
      child: InkWell(
        onTap: () {
          List<OptionItem> ls = widget.lsSearch ?? [];
          OptionItem? tabSelected;
          for (var element in ls) {
            if (element.id == data.id) {
              element.isSelect = true;
              tabSelected = element;
            } else {
              element.isSelect = false;
            }
          }
          if (widget.callback != null) {
            widget.callback!(tabSelected);
          }

          if (GlobalObjectKey(index.toString()).currentContext != null) {
            Scrollable.ensureVisible(
                GlobalObjectKey(index.toString()).currentContext!,
                duration: const Duration(microseconds: 500),
                alignment: .5,
                curve: Curves.easeInOutCubic);
          }
          setState(() {});
        },
        child: Padding(
          padding: const EdgeInsets.only(right: 8),
          child: Container(
              key: GlobalObjectKey(index.toString()),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: data.isSelect
                      ? widget.colorTabSelected ?? primary
                      : widget.colorTab ?? Colors.white,
                  borderRadius: BorderRadius.circular(24)),
              child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
                  child: Center(
                    child: Text(
                      data.title,
                      textAlign: TextAlign.center,
                      style: normalStyle.copyWith(
                          fontSize: fontSize12,
                          fontWeight: FontWeight.w400,
                          color: data.isSelect
                              ? widget.colorTextSelect ?? Colors.white
                              : widget.colorText ?? primary),
                    ),
                  ))),
        ),
      ),
    );

    // return lsChild;
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Padding(
  //       padding: const EdgeInsets.only(left: 16),
  //       child: SingleChildScrollView(
  //         // controller: scrollController,
  //         scrollDirection: Axis.horizontal,
  //         child: Row(
  //             crossAxisAlignment: CrossAxisAlignment.start,
  //             mainAxisAlignment: MainAxisAlignment.start,
  //             children: [
  //               ...List.generate(
  //                 (widget.lsSearch ?? []).length,
  //                 (index) {
  //                   return renderItemTab(widget.lsSearch![index]);
  //                 },
  //               )
  //             ]),
  //       ));
  // }
  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.only(left: 16),
        child: SingleChildScrollView(
          controller: scrollController,
          scrollDirection: Axis.horizontal,
          child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                ...List.generate(
                  (widget.lsSearch ?? []).length,
                  (index) {
                    return renderItemTab(widget.lsSearch![index], index);
                  },
                )
              ]),
        ));
  }
}
