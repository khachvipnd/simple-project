import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';
import 'package:yourpti/widgets/base/dropdown_search_production/view_search_content_production.dart';
// import 'package:yourpti/widgets/dropdowns/drop_list_model.dart';

import '../../../constants/colors.dart';
import '../../../constants/size.dart';
import '../../../constants/styles.dart';

class DropdownSearchProduction extends StatefulWidget implements FormObject {
  DropdownSearchProduction(
      {Key? key,
      this.title = "",
      this.heightView,

      // required this.context,
      this.callback,
      this.control,
      this.onChangeCallback,
      this.onEditingComplete})
      : super(key: key);

  String title;
  // BuildContext context;
  OnChangeCallBack? onChangeCallback;
  OnChangeCallBack? onEditingComplete;
  CallbackData? callback;

  double? heightView;
  TextEditingController controller = TextEditingController(text: "");
  @override
  Control? control;

  @override
  State<DropdownSearchProduction> createState() =>
      _DropdownSearchProductionState();
  var textValidate = "".obs;
  @override
  void callReloadView() {}
}

class _DropdownSearchProductionState extends State<DropdownSearchProduction> {
  double heightView = Get.height - 100;

  @override
  void initState() {
    super.initState();
    if (widget.heightView == null) {
      heightView = Get.height - 100;
    }
    widget.controller.text = widget.control!.value.value;
  }

  Widget inputView() {
    return InkWell(
        onTap: () {
          showDropDown();
        },
        child: SizedBox(
          height: 50,
          child: Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color:
                  widget.control!.enabled.value ? grayconst_100 : secondary_300,
            ),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  width: 10,
                ),

                Expanded(
                  child: TextFormField(
                    controller: widget.controller,
                    // initialValue: item == null ? "" : (item!.title),
                    style: normalStyle.copyWith(
                      fontSize: fontSizeNomal,
                      color: neutral_500,
                      fontWeight: FontWeight.w400,
                    ),
                    onChanged: (value) {
                      if (widget.callback != null) {
                        widget.callback!(value);
                      }
                    },
                    validator: (value) {
                      // if (value!.isEmpty) {
                      //   textValidate.value = widget.textValidate ?? "";
                      // }
                      return onValidate(value);
                    },
                    enabled: false,
                    decoration: InputDecoration(
                        isDense: true,
                        enabled: false,
                        label: titleView(
                            title: widget.control!.title.value,
                            isRequail: widget.control!.required),
                        // labelText: (widget.title ?? "").tr,
                        contentPadding: const EdgeInsets.only(left: 5),
                        enabledBorder: InputBorder.none,
                        focusedBorder: InputBorder.none,
                        // hintText: (widget.hindText == null
                        //     ? "selectitem".tr
                        //     : widget.hindText!.tr),
                        labelStyle: normalStyle.copyWith(
                            color: neutral_400, fontSize: fontSize16),
                        focusColor: neutral_400,
                        border: InputBorder.none,
                        errorMaxLines: 1,
                        errorStyle: const TextStyle(
                            fontSize: 0, height: 0.8, color: red_500),
                        errorBorder: InputBorder.none),
                  ),
                ),
                // const Icon(Icons.visibility_rounded, color: unselectedTextColor)

                const Icon(
                  Icons.expand_more,
                  color: neutral_400,
                  size: 26,
                ),

                const SizedBox(width: 10)
              ],
            ),
          ),
        ));
  }

  void selectValue() {
    // cái này phục vụ set lại valueSelected = value

    var valueSelected = widget.control!.listData
        .firstWhere((element) => element.value == widget.control!.value);

    widget.control!.setValue(valueSelected[widget.control!.keyGetValueForShow]);
    widget.control!.setValueSelected(valueSelected);
    widget.controller.text = valueSelected[widget.control!.keyGetValueForShow];
  }

  void showDropDown() {
    if (!widget.control!.enabled.value) return; //-> tương đương disabled
    FocusScope.of(context).unfocus();
    var heightView = this.heightView;
    if (widget.control != null) {
      heightView = (widget.control!.listData.length * 42 + 150);
    }

    if (heightView <= 330) {
      heightView = 330;
    }
    if (heightView >= Get.height - 150) {
      heightView = Get.height - 150;
    }
    if (widget.control?.isSearch == true) {
      heightView = Get.height - 150;
    }
    showModalBottomSheet<void>(
        backgroundColor: Colors.transparent,
        isScrollControlled: true,
        isDismissible: true,
        context: context,
        builder: (BuildContext context) {
          return SizedBox(
            height: heightView,
            child: ViewSearchContentProduction(
                keyGetValueForShow: widget.control!.keyGetValueForShow,
                keyGetValueForID: widget.control!.keyGetValueForID,
                listData: widget.control!.listData,
                isSearch: widget.control?.isSearch ?? true,
                title: widget.control!.title.value,
                callback: (value) {
                  // printDebug(value);
                  widget.control!
                      .setValue(value[widget.control!.keyGetValueForShow]);
                  widget.control!.setValueSelected(value);
                  widget.controller.text =
                      value[widget.control!.keyGetValueForShow];
                  if (widget.onChangeCallback != null) {
                    widget.onChangeCallback!(widget);
                  }
                  if (widget.onEditingComplete != null) {
                    widget.onEditingComplete!(widget);
                  }
                  widget.textValidate.value = "";
                  setState(() {});
                }),
          );
        });
  }

  String? onValidate(value) {
    print(value);
    if (widget.control!.required &&
        widget.control!.valueSelected != null &&
        widget.control!.valueSelected[widget.control!.keyGetValueForID] == "") {
      widget.textValidate.value = "invalid_noti".tr;
      return widget.textValidate.value;
    } else {
      widget.textValidate.value = "";
    }

    if (widget.control!.required && widget.control!.value.value == "") {
      widget.control!.setInvalid("invalid_noti".tr);
      // isValidate = false;
      widget.textValidate.value = "invalid_noti".tr;
      return widget.textValidate.value;
    } else {
      widget.control!.setInvalid("");
      // isValidate = true;
      widget.textValidate.value = "";
    }

    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => !widget.control!.visible.value
        ? Container(
            child: null,
          )
        : Expanded(
            flex: widget.control!.flex,
            child: Padding(
                padding: EdgeInsets.only(
                    left: widget.control!.indexRow > 0 ? 10 : 16,
                    top: 10,
                    right: widget.control!.isLastRow ? 16 : 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // if (widget.title.isNotEmpty)
                    //   titleView(title: widget.title.tr, isRequail: widget.isRequail),
                    // if (widget.title.isNotEmpty)
                    //   const SizedBox(
                    //     height: 6,
                    //   ),
                    inputView(),
                    if (widget.textValidate.value.isNotEmpty)
                      const SizedBox(
                        height: 4,
                      ),
                    // if (textValidate.value.isNotEmpty)
                    Obx(() => Text(
                          widget.textValidate.value,
                          style: normalStyleValidate.copyWith(color: red_600),
                        ))
                  ],
                ))));

    // return Padding(
    //     padding: EdgeInsets.only(
    //        f left: widget.control!.indexRow > 0 ? 10 : 0, top: 10),
    //     child: Obx(() => Column(
    //           mainAxisAlignment: MainAxisAlignment.start,
    //           crossAxisAlignment: CrossAxisAlignment.start,
    //           children: [
    //             titleView(
    //                 title: widget.control!.title,
    //                 isRequail: widget.control!.required),
    //             const SizedBox(
    //               height: 6,
    //             ),
    //             InkWell(
    //               onTap: () {
    //                 if (!widget.control!.enabled.value)
    //                   return; //-> tương đương disabled
    //                 FocusScope.of(context).unfocus();
    //                 showModalBottomSheet<void>(
    //                     backgroundColor: Colors.transparent,
    //                     isScrollControlled: true,
    //                     isDismissible: true,
    //                     context: context,
    //                     builder: (BuildContext context) {
    //                       return SizedBox(
    //                         height: heightView,
    //                         child: ViewSearchContentProduction(
    //                             keyGetValueForShow:
    //                                 widget.control!.keyGetValueForShow,
    //                             keyGetValueForID:
    //                                 widget.control!.keyGetValueForID,
    //                             listData: widget.control!.listData,
    //                             callback: (value) {
    //                               printDebug(value);
    //                               widget.control!.setValue(value[
    //                                   widget.control!.keyGetValueForShow]);
    //                               widget.control!.setValueSelected(value);

    //                               if (widget.onChangeCallback != null) {
    //                                 widget.onChangeCallback!(widget);
    //                               }
    //                               setState(() {});
    //                             }),
    //                       );
    //                     });
    //               },
    //               child: Container(
    //                 height: 40,
    //                 decoration: BoxDecoration(
    //                   borderRadius: BorderRadius.circular(8),
    //                   color: widget.control!.enabled.value
    //                       ? grayconst_100
    //                       : secondary_300,
    //                 ),
    //                 child: Padding(
    //                   padding: const EdgeInsets.only(left: 12),
    //                   child: Row(
    //                     children: [
    //                       Text(
    //                           widget.control!.value.value == ""
    //                               ? "selectitem".tr
    //                               : widget.control!.value.value,
    //                           style: const TextStyle(fontSize: fontSizeNomal)),
    //                       const Spacer(),
    //                       const Icon(
    //                         Icons.arrow_drop_down,
    //                         color: neutral_400,
    //                         size: 20,
    //                       )
    //                     ],
    //                   ),
    //                 ),
    //               ),
    //             ),
    //             if (widget.control!.invalid.value != "")
    //               Text(widget.control!.invalid.value,
    //                   style: normalStyleValidate.copyWith(
    //                       color: red_600, fontSize: 11)),
    //           ],
    //         )));
  }

  Widget titleView({required String title, bool isRequail = false}) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
              text: title,
              style: const TextStyle(
                  fontFamily: "Helveticaneue",
                  color: neutral_500,
                  fontWeight: FontWeight.w400,
                  fontSize: fontSizeNomal)),
          TextSpan(
              text: isRequail ? '* ' : ' ',
              style: const TextStyle(
                  fontFamily: "Helveticaneue",
                  fontWeight: FontWeight.w500,
                  fontSize: fontSizeNomal,
                  color: red_600)),
        ],
      ),
    );
  }
}
