import 'package:flutter/material.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/models/FormObject.dart';

class DateTimePickerForm extends StatefulWidget implements FormObject {
  DateTimePickerForm({Key? key, this.onChangeCallback, this.control});

  OnChangeCallBack? onChangeCallback;
  OnChangeCallBack? onEditingComplete;

  @override
  Control? control;

  @override
  State<DateTimePickerForm> createState() => _DateTimePickerForm();

  @override
  void callReloadView() {
    // TODO: implement callReloadView
  }
}

class _DateTimePickerForm extends State<DateTimePickerForm> {
  Control? control;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    control = widget.control;
  }

  void openDateTimePicker() {
    // DatePicker.showDatePicker(context, showTitleActions: true,
    //     onChanged: (date) {
    //   printDebug('change $date in time zone ' +
    //       date.timeZoneOffset.inHours.toString());
    // }, onConfirm: (date) {
    //   printDebug('confirm $date');
    // }, currentTime: DateTime.now());
  }

  @override
  Widget build(BuildContext context) {
    // String widget2 = widget.title;

    // return
    return OutlinedButton(
      onPressed: () {
        openDateTimePicker(); // _restorableDatePickerRouteFuture.present();
      },
      child: const Text('Ngày'),
    );
  }
}
