import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/extensions/date.dart';

import '../../../constants/colors.dart';
import '../../../constants/icons.dart';
import '../../../constants/size.dart';
import '../../../constants/styles.dart';
import 'flutter_datetime_picker.dart';

class DatePickerView extends StatefulWidget {
  DatePickerView(
      {Key? key,
      this.title = "",
      this.initDate,
      this.minDate,
      this.maxDate,
      this.isTimePicker = false,
      this.isRequail = false,
      this.textValidate,
      this.initDateTime,
      this.formatDate = "yyyy-MM-ddTHH:MM:ss",
      this.callback})
      : super(key: key);
  CallbackActionDate? callback;
  // DateTime? initDate = DateTime.now();
  String? initDate;
  DateTime? minDate;
  DateTime? initDateTime = DateTime.now();
  DateTime? maxDate;
  String title;
  bool isTimePicker = false;
  bool isRequail = false;
  String? textValidate;
  String formatDate = "yyyy-MM-ddTHH:MM:ss";
  @override
  State<DatePickerView> createState() => _DatePickerViewState();
}

class _DatePickerViewState extends State<DatePickerView> {
  var initDate = DateTime.now();
  var textValidate = "".obs;
  TextEditingController controller = TextEditingController();
  @override
  void initState() {
    super.initState();
    if (widget.initDate != null) {
      try {
        initDate = DateFormat(widget.formatDate).parse(widget.initDate ?? "");
      } catch (e) {
        initDate = DateTime.now();
      }
    } else {
      if (widget.initDateTime != null) {
        initDate = widget.initDateTime!;
      } else {
        initDate = DateTime.now();
      }
    }

    controller = TextEditingController(
        text: widget.isTimePicker == true
            ? initDate.formatTimeToString()
            : initDate.formatDateToString());
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        // titleView(title: widget.title.tr, isRequail: widget.isRequail),
        // const SizedBox(
        //   height: 6,
        // ),
        InkWell(
          onTap: () {
            showDatePicker();
          },
          child: Container(
            height: 50,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              color: grayconst_100,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const SizedBox(
                  width: 10,
                ),
                Expanded(
                  child: Center(
                      child: TextFormField(
                          controller: controller,
                          validator: (value) {
                            // if (value!.isEmpty) {
                            //   textValidate.value = widget.textValidate ?? "";
                            // }
                            onValidate(value);
                            return null;
                          },
                          enabled: false,
                          decoration: InputDecoration(
                              isDense: true,
                              label: titleView(
                                  title: (widget.title).tr,
                                  isRequail: widget.isRequail),
                              // labelText: (widget.title ?? "").tr,
                              contentPadding: const EdgeInsets.only(left: 5),
                              enabledBorder: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              border: InputBorder.none,
                              labelStyle: normalStyle.copyWith(
                                  color: neutral_400, fontSize: fontSize16),
                              focusColor: neutral_400,
                              errorMaxLines: 1,
                              errorStyle: const TextStyle(
                                  fontSize: 0, height: 0.8, color: red_500),
                              errorBorder: InputBorder.none),
                          // decoration: const InputDecoration(
                          //     isCollapsed: true, border: InputBorder.none),
                          style: normalStyle)),
                ),
                SvgPicture.asset(iconCalendar, width: 20, height: 20),
                const SizedBox(
                  width: 10,
                ),
              ],
            ),
          ),
        ),
        // if (textValidate.value.isNotEmpty)
        //   const SizedBox(
        //     height: 8,
        //   ),
        // Obx(() => Text(
        //       textValidate.value,
        //       style: normalStyleValidate.copyWith(color: red_600),
        //     ))
        Obx(() => Container(
              padding:
                  EdgeInsets.only(top: textValidate.value.isNotEmpty ? 4 : 0),
              child: Text(
                textValidate.value,
                style: normalStyleValidate.copyWith(
                    color: red_600,
                    fontSize: textValidate.value.isNotEmpty ? 11 : 0),
              ),
            )),
        const SizedBox(
          height: 16,
        ),
      ],
    );
  }

  Widget titleView({required String title, bool isRequail = false}) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
              text: title,
              style: normalStyle.copyWith(
                color: neutral_500,
                fontWeight: FontWeight.w400,
              )),
          TextSpan(
              text: isRequail ? '* ' : ' ',
              style: normalStyle.copyWith(
                color: red_600,
                fontWeight: FontWeight.w500,
              )),
        ],
      ),
    );
  }

  onValidate(value) {}

  showDatePicker() {
    if (widget.isTimePicker) {
      DatePickerPTI.showTimePicker(context,
          showSecondsColumn: false,
          showTitleActions: true,
          minTime: widget.minDate,
          currentTime: initDate, onChanged: (date) {
        // printDebug('change $date');
      }, onConfirm: (date) {
        // printDebug('confirm $date');
        print(initDate.formatTimeToString());
        var dateString = date.formatTimeToString();
        controller.text = dateString;
        initDate = date;

        if (widget.callback != null) {
          widget.callback!(date, dateString);
        }
      }, locale: LocaleType.vi);
    } else {
      DatePickerPTI.showDatePicker(context,
          showTitleActions: true,
          minTime: widget.minDate,
          currentTime: initDate,
          maxTime: widget.maxDate, onChanged: (date) {
        // printDebug('change $date');
      }, onConfirm: (date) {
        // printDebug('confirm $date');
        var dateString = date.formatDateToString();
        controller.text = dateString;
        initDate = date;
        if (widget.callback != null) {
          widget.callback!(date, dateString);
        }
      }, locale: LocaleType.vi);
    }
  }
}
