import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:yourpti/constants/callbacks.dart';
import 'package:yourpti/extensions/date.dart';
import 'package:yourpti/extensions/string_extension.dart';
import 'package:yourpti/models/ControlObject.dart';
import 'package:yourpti/utils/common.dart';

import '../../../constants/colors.dart';
import '../../../constants/size.dart';
import '../../../constants/styles.dart';
import '../../../models/FormObject.dart';
import 'flutter_datetime_picker.dart';

class DatePickerProductionView extends StatefulWidget implements FormObject {
  DatePickerProductionView(
      {Key? key,
      this.title = "",
      this.initDate,
      this.minDate,
      this.maxDate,
      this.isRequail = false,
      this.context,
      this.callback,
      this.control,
      this.onChangeCallback,
      this.onEditingComplete})
      : super(key: key);
  CallbackActionDate? callback;

  // DateTime? initDate = DateTime.now();
  RxString? initDate;
  DateTime? minDate = DateFormat('dd/MM/yyy').parse("01/01/1900");
  DateTime? maxDate;
  String? dayForTime;
  String title;
  bool isRequail = false;
  BuildContext? context;
  OnChangeCallBack? onChangeCallback;
  OnChangeCallBack? onEditingComplete;
  TextEditingController controller = TextEditingController();
  var textValidate = "".obs;
  @override
  State<DatePickerProductionView> createState() => _DatePickerViewState();

  @override
  Control? control;

  @override
  void callReloadView() {}
}

class _DatePickerViewState extends State<DatePickerProductionView> {
  DateTime initDate = DateTime.now();

  @override
  void initState() {
    super.initState();
    print(initDate.toString());
    // initDate = DateFormat(

    //             )
    //     .parse(
    //         ? widget.control!.defaultValue
    //         : widget.control!.value.value != ""
    //             ? widget.control!.value.value
    //             : initDate.toString());
    var format = "dd/MM/yyyy";
    if (widget.control!.typeChildInput == TypeChildInputForm.timePicker) {
      format = "HH:mm";
    }

    if (widget.control!.value.value != "") {
      try {
        initDate = DateFormat(format).parse(widget.control!.value.value);
      } catch (e) {
        print('lỗi parse ngày tháng sml');
      }
    }

    // if (widget.initDate != null) {
    //   initDate = DateFormat("dd/MM/yyyy HH:mm").parse(widget.initDate ?? "");
    // }

    widget.controller = TextEditingController(
        text: widget.control!.typeChildInput == TypeChildInputForm.timePicker
            ? initDate.formatTimeToString()
            : initDate.formatDateToString());
  }

  @override
  Widget build(BuildContext context) {
    return Obx(() => !widget.control!.visible.value
        ? Container(
            child: null,
          )
        : Expanded(
            flex: widget.control!.flex,
            child: Padding(
              padding: EdgeInsets.only(
                  left: widget.control!.indexRow > 0 ? 10 : 16,
                  top: 10,
                  right: widget.control!.isLastRow ? 16 : 0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // titleView(title: widget.control!.title, isRequail: widget.isRequail),
                  // const SizedBox(
                  //   height: 6,
                  // ),
                  InkWell(
                    onTap: () {
                      if (widget.control!.enabled.value) {
                        showDatePicker();
                      }
                    },
                    child: Container(
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: widget.control!.enabled.value
                            ? grayconst_100
                            : secondary_300,
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const SizedBox(
                            width: 10,
                          ),
                          Expanded(
                            child: Center(
                                child: TextFormField(
                                    controller: widget.controller,
                                    validator: (value) {
                                      // if (value!.isEmpty) {
                                      //   textValidate.value = widget.textValidate ?? "";
                                      // }
                                      onValidate(value);
                                      return null;
                                    },
                                    enabled: false,
                                    decoration: InputDecoration(
                                        isDense: true,
                                        label: titleView(
                                            title:
                                                (widget.control!.title.value),
                                            isRequail:
                                                widget.control!.required),
                                        // labelText: (widget.title ?? "").tr,
                                        contentPadding:
                                            const EdgeInsets.only(left: 5),
                                        enabledBorder: InputBorder.none,
                                        focusedBorder: InputBorder.none,
                                        border: InputBorder.none,
                                        labelStyle: normalStyle.copyWith(
                                            color: neutral_400,
                                            fontSize: fontSize16),
                                        focusColor: neutral_400,
                                        errorMaxLines: 1,
                                        errorStyle: const TextStyle(
                                            fontSize: 0,
                                            height: 0.8,
                                            color: red_500),
                                        errorBorder: InputBorder.none),
                                    style: normalStyle)),
                          ),
                          SvgPicture.asset("assets/icons/calendar.svg",
                              width: 20, height: 20),
                          const SizedBox(
                            width: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                  if (widget.textValidate.value.isNotEmpty)
                    const SizedBox(
                      height: 8,
                    ),
                  Obx(() => Text(
                        widget.textValidate.value,
                        style: normalStyleValidate.copyWith(color: red_600),
                      ))
                ],
              ),
            )));
  }

  onValidate(value) {
    if (widget.control!.required && widget.control!.value.value == "") {
      widget.textValidate.value = "invalid_noti".tr;
      return widget.textValidate.value;
    } else {
      widget.textValidate.value = "";
    }
    return null;
  }

  Widget titleView({required String title, bool isRequail = false}) {
    return RichText(
      text: TextSpan(
        children: <TextSpan>[
          TextSpan(
              text: title,
              style: const TextStyle(
                  fontFamily: "Helveticaneue",
                  color: neutral_500,
                  fontWeight: FontWeight.w400,
                  fontSize: fontSizeNomal)),
          TextSpan(
              text: isRequail ? '* ' : ' ',
              style: const TextStyle(
                  fontFamily: "Helveticaneue",
                  fontWeight: FontWeight.w500,
                  fontSize: fontSizeNomal,
                  color: red_600)),
        ],
      ),
    );
  }

  showDatePicker() {
    if (widget.controller.text.isNotEmpty) {
      if (widget.control!.typeChildInput == TypeChildInputForm.timePicker) {
        if (widget.dayForTime != null && widget.dayForTime != '') {
          String time = widget.dayForTime! + ' ' + widget.controller.text;
          var datetime = DateFormat('dd/MM/yyyy HH:mm').parse(time);
          initDate = datetime;
        } else {
          initDate =
              widget.controller.text.converStringToDate(dateFormat: "HH:mm") ??
                  DateTime.now();
        }
      } else {
        initDate = widget.controller.text
                .converStringToDate(dateFormat: "dd/MM/yyyy") ??
            DateTime.now();
      }
    }
    if (widget.control!.typeChildInput == TypeChildInputForm.timePicker) {
      DatePickerPTI.showTimePicker(context,
          showSecondsColumn: false,
          showTitleActions: true,
          minTime: widget.minDate,
          currentTime: initDate, onChanged: (date) {
        // printDebug('change $date');
      }, onConfirm: (date) {
        widget.textValidate.value = "";
        // printDebug('confirm $date');
        // printDebug(initDate.formatTimeToString());
        var dateString = date.formatTimeToString();
        widget.controller.text = dateString;
        initDate = date;
        widget.control!.setValue(dateString);
        if (widget.onChangeCallback != null) {
          widget.onChangeCallback!(widget);
        }
        if (widget.onEditingComplete != null) {
          widget.onEditingComplete!(widget);
        }

        if (widget.callback != null) {
          widget.callback!(date, dateString);
        }
      }, locale: LocaleType.vi);
    } else {
      widget.minDate ??=
          "01/01/1900".converStringToDate(dateFormat: "dd/MM/yyyy");
      DatePickerPTI.showDatePicker(context,
          showTitleActions: true,
          minTime: widget.minDate,
          currentTime: initDate,
          maxTime: widget.maxDate, onChanged: (date) {
        // printDebug('change $date');
      }, onConfirm: (date) {
        widget.textValidate.value = "";
        // printDebug('confirm $date');
        var dateString = date.formatDateToString();
        widget.controller.text = dateString;
        initDate = date;
        widget.control!.setValue(dateString);
        if (widget.onChangeCallback != null) {
          widget.onChangeCallback!(widget);
        }
        if (widget.onEditingComplete != null) {
          widget.onEditingComplete!(widget);
        }

        if (widget.callback != null) {
          widget.callback!(date, dateString);
        }

        setState(() {});
      }, locale: LocaleType.vi);
    }
  }
}
