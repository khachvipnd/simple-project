import 'package:flutter/material.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:get/get.dart';
import 'package:yourpti/widgets/base/base.dart';
import '../../../constants/colors.dart';
import '../../../constants/size.dart';

class DialogView extends StatefulWidget {
  DialogView(
      {Key? key,
      this.title,
      this.content = "",
      this.maxHeight = 150,
      this.onDoneClick,
      this.onCancelClick,
      this.titleButtonCancel,
      this.titleButtonDone,
      this.forceHideXButton = false,
      this.timeForDismiss})
      : super(key: key);
  String? title;
  String content = "";
  double maxHeight = 150;
  VoidCallback? onDoneClick;
  VoidCallback? onCancelClick;
  String? titleButtonDone;
  String? titleButtonCancel;
  int? timeForDismiss;
  bool forceHideXButton;
  @override
  State<DialogView> createState() => _DialogViewState();
}

class _DialogViewState extends State<DialogView> {
  bool isContentCenter = true;
  @override
  void initState() {
    super.initState();
    if (widget.title != null ||
        widget.onCancelClick != null ||
        widget.onDoneClick != null) {
      isContentCenter = false;
    }

    if (widget.timeForDismiss != null) {
      Future.delayed(Duration(milliseconds: widget.timeForDismiss!), () {
        print('dismiss');
        Navigator.pop(context, false);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
      elevation: 0.0,
      insetPadding: EdgeInsets.zero,
      backgroundColor: Colors.transparent,
      child: dialogContent(context),
    );
  }

  Widget dialogContent(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(left: 0.0, right: 0.0),
      child: Stack(
        children: <Widget>[
          Container(
            width: double.infinity,
            constraints: const BoxConstraints(
              minHeight: 120,
            ),
            margin: const EdgeInsets.only(top: 8.0, right: 8.0),
            padding: EdgeInsets.only(
                top: isContentCenter == true ? 0 : 32,
                right: 20,
                left: 20,
                bottom: 20),
            // margin: const EdgeInsets.all(2),
            decoration: const BoxDecoration(
                color: white,
                borderRadius: BorderRadius.all(Radius.circular(20))),
            child: isContentCenter == true
                ? Column(
                    mainAxisSize: MainAxisSize.min,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                        Center(
                          child: Text(
                            widget.content.tr,
                            textAlign: TextAlign.center,
                            style: normalStyle.copyWith(
                                decoration: TextDecoration.none),
                          ),
                        )
                      ])
                : Column(mainAxisSize: MainAxisSize.min, children: [
                    if (widget.title != null)
                      Text(
                        widget.title!.tr,
                        textAlign: TextAlign.center,
                        style: boldStyle.copyWith(
                            decoration: TextDecoration.none,
                            fontSize: fontSize16,
                            color: neutral_500),
                      ),
                    if (widget.title != null)
                      const SizedBox(
                        height: 16,
                      ),
                    Text(
                      widget.content.tr,
                      textAlign: TextAlign.center,
                      style:
                          normalStyle.copyWith(decoration: TextDecoration.none),
                    ),
                    if (widget.onCancelClick != null ||
                        widget.onDoneClick != null)
                      const SizedBox(
                        height: 24,
                      ),
                    if (widget.onCancelClick != null ||
                        widget.onDoneClick != null)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          if (widget.onCancelClick != null)
                            Expanded(
                                flex: 1,
                                child: ButtonWidget(
                                  name: widget.titleButtonCancel ?? "cancel".tr,
                                  textColor: secondary,
                                  color: gray_200,
                                  textStyle: normalStyle,
                                  callback: () {
                                    if (widget.onCancelClick != null) {
                                      widget.onCancelClick!();
                                      Navigator.pop(context, false);
                                    }
                                  },
                                )),
                          if (widget.onDoneClick != null)
                            const SizedBox(
                              width: 10,
                            ),
                          if (widget.onDoneClick != null &&
                              widget.onCancelClick != null)
                            Expanded(
                                flex: 1,
                                child: ButtonWidget(
                                  name: widget.titleButtonDone ?? "accept".tr,
                                  color: secondary,
                                  textStyle: normalStyle,
                                  callback: () {
                                    if (widget.onDoneClick != null) {
                                      Navigator.pop(context, false);
                                      widget.onDoneClick!();
                                    }
                                  },
                                )),
                          if (widget.onDoneClick != null &&
                              widget.onCancelClick == null)
                            ButtonWidget(
                              name: "confirm".tr,
                              width: 150,
                              color: secondary,
                              textStyle: normalStyle,
                              callback: () {
                                if (widget.onDoneClick != null) {
                                  Navigator.pop(context, false);
                                  widget.onDoneClick!();
                                }
                              },
                            )
                        ],
                      ),
                  ]),
          ),

          Positioned(
              // top: 0,
              right: 0,
              child: widget.forceHideXButton
                  ? const SizedBox()
                  : InkWell(
                      onTap: () {
                        Navigator.pop(context, false);
                      },
                      child: const Align(
                        alignment: Alignment.topRight,
                        child: CircleAvatar(
                          radius: 14.0,
                          backgroundColor: Colors.white,
                          child: Icon(
                            Icons.close,
                            color: neutral_500,
                            size: 12,
                          ),
                        ),
                      ),
                    ))

          //   Container(
          //     width: 32,
          //     height: 32,
          //     decoration: const BoxDecoration(
          //         color: white,
          //         borderRadius: BorderRadius.all(Radius.circular(16))),
          //     child: const Icon(
          //       Icons.close,
          //       color: neutral_500,
          //       size: 12,
          //     ),
          //   ),
          // ))
        ],
      ),
    );
  }
}
