import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../../constants/colors.dart';
import '../../constants/icons.dart';
import '../../constants/styles.dart';

// ignore: must_be_immutable
class SupportWidget extends StatelessWidget {
  SupportWidget({Key? key, this.callback}) : super(key: key);
  VoidCallback? callback;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 44,
      child: InkWell(
        onTap: () {
          if (callback != null) {
            callback!();
          }
        },
        child: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
          SvgPicture.asset(
            headphone,
            height: 30,
            width: 30,
          ),
          const SizedBox(
            width: 6,
          ),
          Text(
            "support".tr,
            style: normalStyle.copyWith(
                fontWeight: FontWeight.w400, color: primary),
          )
        ]),
      ),
    );
  }
}
