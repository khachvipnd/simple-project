import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:yourpti/constants/styles.dart';
import 'package:yourpti/controllers/base/baseview.dart';
import '../../constants/icons.dart';

// ignore: must_be_immutable
class RequestSuccessView extends BaseScreen {
  RequestSuccessView(
      {Key? key, required this.title, required this.content, this.viewButton})
      : super(key: key);
  String title;
  String content;
  Widget? viewButton;
  @override
  State<RequestSuccessView> createState() =>
      _CreateCompensationSuccessViewState();
}

class _CreateCompensationSuccessViewState extends BaseState<RequestSuccessView>
    with BasicState {
  @override
  PreferredSizeWidget? createAppBar() {
    return null;
  }

  @override
  Widget createBody() {
    return SafeArea(
        child: Column(
      children: [
        Expanded(
            child: Container(
          padding: const EdgeInsets.only(left: 16, right: 16),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SvgPicture.asset(iconRequestSuccess),
              const SizedBox(
                height: 60,
              ),
              Text(
                widget.title,
                textAlign: TextAlign.center,
                style: boldStyle,
              ),
              const SizedBox(
                height: 15,
              ),
              Text(
                widget.content,
                textAlign: TextAlign.center,
                style: normalStyle,
              ),
            ],
          ),
        )),
        widget.viewButton == null ? Container() : widget.viewButton!
      ],
    ));
  }
}
