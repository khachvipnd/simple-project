import 'package:get/get.dart';
import 'package:yourpti/controllers/home/home_view.dart';

import 'package:yourpti/controllers/login/login_view.dart';
import 'package:yourpti/screens.dart';

import 'controllers/home/nhap/inputView.dart';

class RouterApp {
  static var route = [
    GetPage(name: loginView, page: () => const LoginView()),
    GetPage(name: homeView, page: () => const HomeView()),
    GetPage(name: inputView, page: () => const InputView()),
  ];
}
