import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yourpti/utils/common.dart';

import 'ControlObject.dart';

class PersonControl extends Control {
  RxBool isExpanded = false.obs;
  List<Control?> lsControls = [];
  dynamic data = {};

  PersonControl(
      {required RxString title,
      required List listData,
      required String id,
      required bool required,
      required TypeInputForm typeInput,
      required RxString value,
      required int flex,
      required RxBool enabled,
      required RxBool visible,
      required this.lsControls,
      required this.data,
      required this.isExpanded})
      : super(
            title: title,
            listData: listData,
            id: id,
            required: required,
            typeInput: typeInput,
            value: value,
            flex: flex,
            enabled: enabled,
            visible: visible);
  // PersonControl(
  //     {required RxString? title,
  //     required List? listData,
  //     required String? id,
  //     required bool? required,
  //     required TypeInputForm? typeInput,
  //     required RxString value,
  //     required int? flex,
  //     required RxBool? enabled,
  //     required RxBool? visible,
  //     required this.lsControls,
  //     required this.isExpanded,
  //     required this.data})
  //     : super(
  //           title: title,
  //           listData: listData,
  //           id: id,
  //           required: required,
  //           typeInput: typeInput,
  //           value: value,
  //           flex: flex,
  //           enabled: enabled,
  //           visible: visible);
  factory PersonControl.init(
      {required RxBool isExpanded, required dynamic data}) {
    return PersonControl(
        title: "".obs,
        listData: [],
        id: "",
        required: false,
        typeInput: TypeInputForm.hiddenInput,
        value: "".obs,
        flex: 0,
        enabled: false.obs,
        visible: true.obs,
        lsControls: [],
        isExpanded: isExpanded,
        data: data);
  }
  // PersonControl.init({required String id, required RxString value}) : super.init(id: id, value: value);

  // PersonControl.init({required String id, required RxString value}) : super._internal();

}
