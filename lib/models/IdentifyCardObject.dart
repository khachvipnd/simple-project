import 'dart:convert';
import 'dart:core';

class IdentifyCardObject {
  String? address;
  String? addressconf;
  String? birthday;
  String? birthdayconf;
  String? characteristics;
  String? characteristicsConf;
  String? cClass;
  String? copyright;
  String? country;
  String? district;
  String? document;
  String? ethnicity;
  String? ethnicityconf;
  String? expiry;
  String? expiryconf;
  String? hometown;
  String? hometownconf;
  String? id;
  String? idCheck;
  int? idFull;
  String? idLogic;
  String? idLogicMessage;
  String? idType;
  String? idconf;
  String? issueBy;
  String? issueByConf;
  String? issueDate;
  String? issueDateConf;
  String? name;
  String? nameconf;
  String? national;
  String? optionalData;
  String? passportType;
  String? precinct;
  String? province;
  String? religion;
  String? religionconf;
  int? resultCode;
  String? serverName;
  String? serverVer;
  String? sex;
  String? sexconf;
  String? street;
  String? streetName;

  IdentifyCardObject(
      {this.address,
      this.addressconf,
      this.birthday,
      this.birthdayconf,
      this.characteristics,
      this.characteristicsConf,
      this.cClass,
      this.copyright,
      this.country,
      this.district,
      this.document,
      this.ethnicity,
      this.ethnicityconf,
      this.expiry,
      this.expiryconf,
      this.hometown,
      this.hometownconf,
      this.id,
      this.idCheck,
      this.idFull,
      this.idLogic,
      this.idLogicMessage,
      this.idType,
      this.idconf,
      this.issueBy,
      this.issueByConf,
      this.issueDate,
      this.issueDateConf,
      this.name,
      this.nameconf,
      this.national,
      this.optionalData,
      this.passportType,
      this.precinct,
      this.province,
      this.religion,
      this.religionconf,
      this.resultCode,
      this.serverName,
      this.serverVer,
      this.sex,
      this.sexconf,
      this.street,
      this.streetName});

  IdentifyCardObject.fromMap(Map<String, dynamic> json) {
    address = json['address'];
    addressconf = json['addressconf'];
    birthday = json['birthday'];
    birthdayconf = json['birthdayconf'];
    characteristics = json['characteristics'];
    characteristicsConf = json['characteristics_conf'];
    cClass = json['class'];
    copyright = json['copyright'];
    country = json['country'];
    district = json['district'];
    document = json['document'];
    ethnicity = json['ethnicity'];
    ethnicityconf = json['ethnicityconf'];
    expiry = json['expiry'];
    expiryconf = json['expiryconf'];
    hometown = json['hometown'];
    hometownconf = json['hometownconf'];
    id = json['id'];
    idCheck = json['id_check'];
    if (json['id_full'] == "N/A"){
      idFull = 0;
    }else {
      idFull = json['id_full'];
    }
    idLogic = json['id_logic'];
    idLogicMessage = json['id_logic_message'];
    idType = json['id_type'];
    idconf = json['idconf'];
    issueBy = json['issue_by'];
    issueByConf = json['issue_by_conf'];
    issueDate = json['issue_date'];
    issueDateConf = json['issue_date_conf'];
    name = json['name'];
    nameconf = json['nameconf'];
    national = json['national'];
    optionalData = json['optional_data'];
    passportType = json['passport_type'];
    precinct = json['precinct'];
    province = json['province'];
    religion = json['religion'];
    religionconf = json['religionconf'];
    resultCode = json['result_code'];
    serverName = json['server_name'];
    serverVer = json['server_ver'];
    sex = json['sex'];
    sexconf = json['sexconf'];
    street = json['street'];
    streetName = json['street_name'];
  }
  factory IdentifyCardObject.fromJson(String source) =>
      IdentifyCardObject.fromMap(json.decode(source));

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['address'] = address;
    data['addressconf'] = addressconf;
    data['birthday'] = birthday;
    data['birthdayconf'] = birthdayconf;
    data['characteristics'] = characteristics;
    data['characteristics_conf'] = characteristicsConf;
    data['class'] = cClass;
    data['copyright'] = copyright;
    data['country'] = country;
    data['district'] = district;
    data['document'] = document;
    data['ethnicity'] = ethnicity;
    data['ethnicityconf'] = ethnicityconf;
    data['expiry'] = expiry;
    data['expiryconf'] = expiryconf;
    data['hometown'] = hometown;
    data['hometownconf'] = hometownconf;
    data['id'] = id;
    data['id_check'] = idCheck;
    data['id_full'] = idFull;
    data['id_logic'] = idLogic;
    data['id_logic_message'] = idLogicMessage;
    data['id_type'] = idType;
    data['idconf'] = idconf;
    data['issue_by'] = issueBy;
    data['issue_by_conf'] = issueByConf;
    data['issue_date'] = issueDate;
    data['issue_date_conf'] = issueDateConf;
    data['name'] = name;
    data['nameconf'] = nameconf;
    data['national'] = national;
    data['optional_data'] = optionalData;
    data['passport_type'] = passportType;
    data['precinct'] = precinct;
    data['province'] = province;
    data['religion'] = religion;
    data['religionconf'] = religionconf;
    data['result_code'] = resultCode;
    data['server_name'] = serverName;
    data['server_ver'] = serverVer;
    data['sex'] = sex;
    data['sexconf'] = sexconf;
    data['street'] = street;
    data['street_name'] = streetName;
    return data;
  }
}
