import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:yourpti/utils/common.dart';

class Control {
  RxString title = "".obs; // tilte + hint.
  List listData; // lsData cho các dropdown list

  String id; //
  bool required; // bắt buộc nhập
  // String defaultValue; // giá trị mặc định khi render
  // get getDefaultValue => this.defaultValue;

  // void setDefaultValue(final defaultValue) => this.defaultValue = defaultValue;

  TypeInputForm
      typeInput; // loại input: textfield, datetimepicker, checkbox....v..v
  TypeChildInputForm typeChildInput;
  TextInputType keyType; //keyboardType -> loại bàn phím hiển thị khi nhập
  FormatValuePresent formatShow; // thể hiện số tiền hay số thường....vv..
  int flex;
  BuildContext? context;
  var value = "".obs;
  void setValue(String value) => this.value = value.obs;

  int indexRow; // xác định vị trí của widget trong hàng, > 0 -> padding left
  bool isLastRow;
  bool notValidate; // lưu giá trị nếu k bắt buộc validate value;
  String icon;
  var enabled = true.obs;
  bool isDouble = false;
  String keyGetValueForShow;
  bool isSearch = true;
  String keyGetValueForID;
  dynamic valueSelected;
  bool isShowDK = true;
  bool isShowDKiem = true;
  bool isShowCCCD = true;
  String typeocr = "1";
  dynamic getValueSelected() {
    if (valueSelected != null && keyGetValueForID.isNotEmpty) {
      return valueSelected[keyGetValueForID];
    } else if (valueSelected == null && value.value.isNotEmpty) {
      return value.value;
    }
    return null;
  }

  void setValueSelected(dynamic valueSelected) =>
      this.valueSelected = valueSelected;
  var invalid = "".obs;
  String getInvalid() => invalid.value;
  RxBool visible = true.obs;
  bool isShowView = true;
  bool isMinDateCurrent = false;

  void setInvalid(var invalid) => this.invalid.value = invalid;

  dynamic style = {};
  String keyGetListDataFormStart = '';

  int? limitLength;

  bool forceShowUnitMoney = true; // check show unit ở màn overview
  factory Control.init(
      {required String id, required RxString value, String title = ""}) {
    return Control(
        title: title.obs,
        listData: [],
        id: id,
        required: false,
        typeInput: TypeInputForm.hiddenInput,
        value: value,
        flex: 0,
        enabled: false.obs,
        isShowCCCD: true,
        isShowDK: true,
        isShowDKiem: true,
        isShowView: true,
        typeocr: "1",
        visible: true.obs);
  }
  Control(
      {required this.title,
      required this.listData,
      required this.id,
      required this.required,
      // required this.defaultValue,
      required this.typeInput,
      this.keyType = TextInputType.text,
      required this.value,
      this.context,
      this.isSearch = true,
      this.formatShow = FormatValuePresent.Normal,
      required this.flex,
      this.indexRow = 0,
      this.notValidate = false,
      this.icon = "",
      required this.enabled,
      this.isShowView = true,
      this.typeocr = "1",
      this.isShowDK = true,
      this.isShowDKiem = true,
      this.isShowCCCD = true,
      this.isMinDateCurrent = false,
      this.keyGetValueForShow = "MA",
      this.keyGetValueForID = "MA",
      this.isDouble = false,
      this.typeChildInput = TypeChildInputForm.datePicker,
      required this.visible,
      this.isLastRow = false,
      this.keyGetListDataFormStart = ''});

  clone() => Control(
      title: title,
      listData: listData,
      id: id,
      required: required,
      keyType: keyType,
      value: value,
      enabled: enabled,
      flex: flex,
      typeInput: typeInput,
      visible: visible,
      keyGetValueForID: keyGetValueForID,
      keyGetValueForShow: keyGetValueForShow,
      isShowDK: isShowDK,
      isShowDKiem: isShowDKiem,
      typeocr: typeocr,
      isShowCCCD: isShowCCCD,
      indexRow: indexRow,
      isSearch: isSearch,
      context: context);
  get getListData => listData;

  void setListData(listData) => this.listData = listData ?? [];

  int get getIndexRow => indexRow;

  void setIndexRow(int indexRow) => this.indexRow = indexRow;

  // Map<String, dynamic> toMap() {
  //   return {
  //     'title': title,
  //     'listData': listData,
  //     'id': id,
  //     'required': required.toMap(),
  //     'defaultValue': defaultValue,
  //     'typeInput': typeInput,
  //   };
  // }

  factory Control.fromMap(Map<String, dynamic> map) {
    TextInputType keyType;
    switch (KeyboardType.values.byName(map['keyType'] ?? "Normal")) {
      case KeyboardType.Normal:
        keyType = TextInputType.text;
        break;
      case KeyboardType.Phone:
        keyType = TextInputType.phone;
        break;
      case KeyboardType.Number:
        keyType = TextInputType.number; //numberWithOptions(decimal: true);
        break;
      case KeyboardType.Email:
        keyType = TextInputType.emailAddress;
        break;
      default:
        keyType = TextInputType.text;
    }
    bool enabled = map['enabled'] ?? true;

    FormatValuePresent formatShow = FormatValuePresent.values
        .byName(map['formatShow'] ?? FormatValuePresent.Normal.name);
    String value =
        map['value'] ?? (formatShow == FormatValuePresent.Money ? "0" : "");
    RxBool visible = map['visible'] != null && map['visible'] == false
        ? false.obs
        : true.obs;
    bool isShowView =
        map['visible'] != null && map['visible'] == false ? false : true;
    String title = map['title'] ?? "";
    bool isShowDK = map["isShowDK"] ?? true;
    bool isShowDKiem = map["isShowDKiem"] ?? true;
    bool isShowCCCD = map["isShowCCCD"] ?? true;
    String typeocr = map["typeocr"] ?? "1";
    Control control = Control(
        title: title.obs,
        isShowDK: isShowDK,
        isShowDKiem: isShowDKiem,
        isShowCCCD: isShowCCCD,
        listData: map['listData'] ?? [],
        id: map['id'] ?? "",
        required: map['required'] ?? false,
        isSearch: map["search"] ?? true,
        isMinDateCurrent: map["mindate"] ?? false,
        typeocr: typeocr,
        // defaultValue: map['defaultValue'] ?? "",
        value: value.obs,
        typeInput: TypeInputForm.values
            .byName(map['typeInput'] ?? TypeInputForm.text.name),
        typeChildInput: TypeChildInputForm.values
            .byName(map['typeChildInput'] ?? TypeChildInputForm.undefined.name),
        keyType: keyType,
        formatShow: formatShow,
        isShowView: isShowView,
        flex: map['flex'] ?? 1,
        enabled: enabled.obs,
        isDouble: map["double"] ?? false,
        keyGetValueForShow: map['keyGetValueForShow'] ?? 'MA',
        keyGetValueForID: map['keyGetValueForID'] ?? 'MA',
        keyGetListDataFormStart: map['keyGetListDataFormStart'] ?? '',
        visible: visible);
    control.limitLength = map['limit'];
    control.style = map['style'] ?? {};

    return control;
  }
  String log() {
    return 'Title: ' +
        title.value +
        '\nid: ' +
        id +
        '\nvalue: ' +
        value.value +
        '\ntypeInput: ' +
        typeInput.name +
        '\nkeyType: ' +
        (keyType.toString());
  }
  // String toJson() => json.encode(toMap());

  factory Control.fromJson(String source) =>
      Control.fromMap(json.decode(source));
}
