import 'package:yourpti/models/ControlObject.dart';

abstract class FormObject {
  Control? control;
  void callReloadView() {}
}

class FormObjectDefault extends FormObject {
  bool isDefault = true;
}
