// ignore_for_file: file_names

import 'dart:convert';

import 'package:yourpti/models/ControlObject.dart';

class PageControl {
  final List<List<Control>> row;

  PageControl({
    required this.row,
  });
  factory PageControl.fromMap(Map<String, dynamic> map) {
    List<List<Control>> lsRow = [];
    map['page']?.forEach((row) {
      // printDebug(row);
      List<Control> lsControl = [];
      row.forEach((column) {
        Control control = Control.fromMap(column);
        lsControl.add(control);
      });

      lsRow.add(lsControl);
    });

    return PageControl(row: lsRow);
  }

  // String toJson() => json.encode(toMap());

  factory PageControl.fromJson(String source) =>
      PageControl.fromMap(json.decode(source));
}

class GroupControl extends PageControl {
  GroupControl({
    required row,
  }) : super(row: row);
  factory GroupControl.fromJson(Map<String, dynamic> map) {
    List<List<Control>> lsRow = [];
    map['group']?.forEach((row) {
      // printDebug(row);
      List<Control> lsControl = [];
      row.forEach((column) {
        Control control = Control.fromMap(column);
        lsControl.add(control);
      });

      lsRow.add(lsControl);
    });

    return GroupControl(row: lsRow);
  }
  factory GroupControl.fromJsonPage(Map<String, dynamic> map) {
    List<List<Control>> lsRow = [];
    map['page']?.forEach((row) {
      // printDebug(row);
      List<Control> lsControl = [];
      row.forEach((column) {
        Control control = Control.fromMap(column);
        lsControl.add(control);
      });

      lsRow.add(lsControl);
    });

    return GroupControl(row: lsRow);
  }
}
