const Map<String, String> ko = {
  "titlewelcome": "Xin chào",
  "welcome": "Chào mừng đến với ứng dụng của tôi",
  "usernamenotempty": "Tên người dùng không được để trống",
  "inforlogin": "Thông tin đăng nhập",
  "username": "Tên đăng nhập",
  "password": "Mật khẩu",
  "passnotempty": "Mật khẩu không được để trống",
  "savePass": "Lưu mật khẩu",
  "forgetpassword": "Quên mật khẩu",
  "questionRegister": "Bạn chưa có tài khoản?",
  "registernow": "Đăng ký ngay",
  "login": "Đăng nhập",
  "home": "Trang chủ",
  "input": "Nhập",
  'saveInfor': "Lưu thông tin",
  "information": "Thông tin",
};
