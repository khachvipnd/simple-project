// ignore_for_file: constant_identifier_names

enum FormatValuePresent { Normal, Money, Percent, Free }

enum KeyboardType {
  Normal,
  Number,
  Phone,
  Email,
}

enum TypeInputForm {
  textField,
  dropDownSearch,
  text,
  dateTimePicker,
  checkbox,
  hiddenInput,
  line,
  lineBox,
  buttonOCR,
  button,
  groupPerson,
  kyPhi,
  buttonUploadImage
}

enum TypeChildInputForm { undefined, timePicker, datePicker }

enum TypeOCR { cccd, dangKy, DangKiem }

enum KieuNhapLoaiXe { A, C, T }

enum TypeFrom { edit, createNew, reNew, copy }

List<Map<String, dynamic>> getYearData() {
  DateTime date = DateTime.now();
  var year = date.year;

  List<Map<String, dynamic>> yearData = [];

  for (var i = 0; i <= 50; i++) {
    yearData.add({"MA": (year - i).toString(), "TEN": (year - i).toString()});
  }

  return yearData;
}

bool validateCMT(String? str) {
  str = str?.replaceAll('/[^0-9]/g', "");
  if (str?.length == 7 ||
      str?.length == 14 ||
      str?.length == 12 ||
      str?.length == 11 ||
      str?.length == 10 || //for MST
      str?.length == 9) {
    return true;
  }

  return false;
}

/// return 1 là dateCompare > date;
/// reutrn = 0 là dateCompare = date;
/// reutrn = -1 là dateCompare < date;
int compareDateTime(DateTime date, DateTime dateCompare) {
  if (date.year > dateCompare.year) {
    return -1;
  } else if (date.year == dateCompare.year) {
    if (date.month > dateCompare.month) {
      return -1;
    } else if (date.month == dateCompare.month) {
      if (date.day > dateCompare.day) {
        return -1;
      } else if (date.day == dateCompare.day) {
        if (date.hour > dateCompare.hour) {
          return -1;
        } else if (date.hour == dateCompare.hour) {
          if (date.minute > dateCompare.minute) {
            return -1;
          } else if (date.minute == dateCompare.minute) {
            return 0;
          }
        }
      }
    }
  }
  return 1;
}
