import 'package:flutter/material.dart';
import "colors.dart";

const fontFamilyMain = "Helveticaneue";

const paddingLeftRight = EdgeInsets.only(left: 16, right: 16);

const fontSizeNomal = 15.0;
const fontSizeNomalvalidate = 12.0;
const fontSizeBold = 16.0;
const fontSizeBoldHeaderBar = 15.0;

const headerStyle = TextStyle(
  fontSize: fontSizeBoldHeaderBar,
  fontWeight: FontWeight.bold,
);

const normalStyleValidate = TextStyle(
    fontSize: fontSizeNomalvalidate,
    fontFamily: fontFamilyMain,
    fontWeight: FontWeight.w200,
    color: neutral_500);

const normalStyle = TextStyle(
    fontSize: fontSizeNomal,
    fontFamily: fontFamilyMain,
    fontWeight: FontWeight.w400,
    color: neutral_500);

const boldStyle = TextStyle(
  fontWeight: FontWeight.bold,
  color: neutral_500,
  fontFamily: fontFamilyMain,
  fontSize: fontSizeBold,
);

const nomalDecoration = BoxDecoration(
    color: gray_200, borderRadius: BorderRadius.all(Radius.circular(8)));
