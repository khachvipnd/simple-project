const username = "assets/icons/user.svg";
const password = "assets/icons/lock.svg";
const infor = "assets/icons/infor.svg";
const phone = "assets/icons/icon_phone.svg";
const logoPTI = "assets/icons/icon_register_success.svg";
const arrowleft = "assets/svgs/arrow_left.svg";
const arrowright = "assets/svgs/arrowright.svg";
const iconArrowStep = "assets/svgs/arrow_right.svg";
const eyeslash = "assets/icons/eye-slash.svg";
const headphone = "assets/icons/headphone.svg";
const vn = "assets/images/vn.png";
const hq = "assets/images/hq.png";
const english = "assets/images/english.png";
const check = "assets/images/check.png";
const welcome = "assets/icons/welcome.svg";
const takePicture = "assets/images/take_picture.png";
const identify = "assets/icons/identify.svg";
const registersuccess = "assets/icons/icon_register_success.svg";
const avatar = "assets/svgs/avatar_blank.svg";
const iconWelcome = "assets/svgs/icon_welcome.svg";
const logoPTIPrimaryNew = "assets/icons/icon_register_success.svg";
const vectorstep = "assets/icons/vector_step.svg";

const frametest1 = "assets/images/page1.png";
const frametest2 = "assets/images/page2.png";
const frametest3 = "assets/images/page3.png";
const frametest4 = "assets/images/page4.png";
const wellcome = "assets/images/wellcome.png";
const logoptistep = "assets/icons/icon_register_success.svg";

//support
const support = "assets/icons/support.svg";
const supportCall = "assets/icons/call_calling.svg";
const supportsms = "assets/icons/sms.svg";
const supportdocument = "assets/icons/document_text.svg";
const supportmesquestion = "assets/icons/mes_question.svg";
const supportArrow = "assets/icons/support_arrow.svg";

// home

const iconHomeSelect = "assets/icons/icon_home_select.svg";
const iconHome = "assets/icons/icon_home.svg";
const iconMyInsuranceSelect = "assets/icons/myinsurance_home_select.svg";
const iconMyInsurance = "assets/icons/myinsurance_home.svg";
const iconSupport = "assets/icons/support_home.svg";
const iconUserSelect = "assets/icons/user_home_select.svg";
const iconUser = "assets/icons/user_home.svg";
const iconSellerSelect = "assets/icons/icon_seller_select.svg";
const iconSeller = "assets/icons/icon_seller.svg";

// myinsurance
const iconHistoryIndemnify = "assets/icons/history_indemnify.svg";
const iconHistoryInsurance = "assets/icons/history_insurance.svg";
const iconCarInsurance = "assets/icons/icon_car_insurance.svg";
const iconUserInsurance = "assets/icons/icon_user_insurance.svg";
const iconPeopleInsurance = "assets/icons/people_insurance.svg";
const footermyinsurance = "assets/icons/footer_my_insurance.svg";
const iconDocument = "assets/icons/icon_document.svg";
const iconAdd = "assets/icons/icon_add.svg";
const iconCamera = "assets/icons/icon_camera.svg";
const iconCalendar = "assets/icons/calendar.svg";
const iconLocation2 = "assets/icons/icon_location2.svg";
const iconUser2 = "assets/icons/icon_user.svg";
const iconSMS = "assets/icons/icon_sms.svg";
// human compenstion
const iconSearch = "assets/icons/icon_search.svg";
const iconDownload = "assets/icons/icon_download.svg";
const iconListBank = "assets/icons/icon_list_bank.svg";
const iconRequestSuccess = "assets/icons/icon_request_success.svg";
// history compensation

const iconShowUpdate = "assets/icons/icon_show_update.svg";
const backgroundAccount = "assets/images/background_account.png";
const iconAvt = "assets/images/avt.png";
const iconDemoHistory = "assets/icons/icon_demo_history.svg";
const iconReuqetsEmpty = "assets/icons/icon_request_empty.svg";
// const iconPTIQr = "assets/icons/icon_pti_qr.png";
const iconEdit = "assets/icons/icon_edit.svg";

// account view
const iconarrow = "assets/svgs/icon_arrow.svg";
const iconLocation = "assets/icons/icon_location.svg";
const iconStartProcess = "assets/icons/icon_start_process.svg";

const iconCheckbox = "assets/svgs/icon_checkbox.svg";
const iconCheckboxSelected = "assets/svgs/icon_checkbox_selected.svg";

const iconEditPageOverview = "assets/svgs/icon_edit_overview.svg";
const iconLine = "assets/icons/icon_line.svg";
const notSelectStart = "assets/icons/not_start.svg";
const selectStart = "assets/icons/start_select.svg";

// news
const iconBannerNews = 'assets/svgs/banner_news.svg';

// address
const iconFilter = 'assets/svgs/icon_search_town.svg';
const iconClose = 'assets/svgs/icon_close.svg';
const iconUpdateAvt = 'assets/icons/icon_update_avt.png';

//notify
const iconCar = 'assets/svgs/icon_car.svg';
const iconBike = 'assets/svgs/icon_bike.svg';
const iconRectangle = 'assets/svgs/icon_rectangle.svg';
const iconSearchNotify = 'assets/icons/icon_search_notify.png';
const iconNoneResult = 'assets/images/no_value_search.png';
const iconEclipse = 'assets/icons/icon_eclipse.png';
const iconDot = 'assets/icons/icon_dot.png';
//person
const iconEditPerson = 'assets/svgs/icon_edit.svg';
const iconRecBin = 'assets/svgs/icon_recbin.svg';
const iconVertical = 'assets/icons/icon_vertical.svg';
const iconCallBorder = 'assets/svgs/icon_call.svg';
const iconMore = 'assets/icons/icon_more.svg';

//bank
const tpbank = 'assets/icons/bank.svg';
const vector = 'assets/icons/Vector.svg';
const iconBchh = 'assets/icons/bchh.svg';

const bgStatisHomeView = "assets/images/bg_statis_home_view.png";
const btnNextCircle = "assets/images/next.png";
const btnBackCircle = "assets/images/back.png";
const iconNoDemnify = "assets/images/no_demnify.png";

const iconBgHome = "assets/images/home_bg_top.png";
const iconBgHomeBottomTop = "assets/images/home_bg_bottom_top.png";
const iconOto = "assets/images/oto.png";
const iconCamera2 = "assets/images/icon_camera2.png";
const iconFingerPrint = "assets/images/fingerprint.png";
const iconFaceID = "assets/images/face_id.png";
const iconCoppy = "assets/icons/coppy.svg";
const iconCamera3 = "assets/svgs/icon_camera.svg";
