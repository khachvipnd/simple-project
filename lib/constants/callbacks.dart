import 'package:yourpti/models/FormObject.dart';

import '../widgets/base/dropdowns/drop_list_model.dart';

typedef CallbackActionBool = void Function(bool isCheck);

typedef CallbackActionString = void Function(String content);

typedef CallbackActionDate = void Function(DateTime date, String dateString);
typedef CallbackActionOptionItem = void Function(OptionItem object);

typedef CallbackData = void Function(dynamic data);

typedef OnChangeCallBack = void Function(FormObject obj);

typedef CallbackActionInt = void Function(int content);