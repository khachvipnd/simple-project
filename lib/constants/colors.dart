import 'package:flutter/material.dart';

const backgroundColorAtsplash = Color(0xFF1E3458);
const primaryColor = Color(0xFF007CC4);
const accentColor = Color(0xFFF58220);
const appColor = Color(0xFFF58220);
const backgroundColor = Color(0xFFEAEAEA);
const subTextColor = Color(0xFF333333);
const subTextColor1 = Color(0xFF999999);
const borderColor = Color(0xffD3D3D3);
const textColor = Color(0xff000000);
const unselectedTextColor = Color(0xff777777);
const unselectedBorderColor = Color(0xffe6e6e6);
const errorBorderColor = Color(0xffEE3226);
const colorgray60 = Color(0xffF8F8F8);
const colorGray = Color(0xffF5F5F5);

const primary = Color.fromRGBO(0, 55, 123, 1);
const secondary = Color(0xffFCBA3F); // Color.fromRGBO(246, 134, 50, 1);
const white = Color.fromRGBO(255, 255, 255, 1);
const neutral_500 = Color.fromRGBO(38, 38, 40, 1);
const neutral_400 = Color.fromRGBO(100, 116, 139, 1);
const neutral_300 = Color.fromRGBO(203, 212, 225, 1);
const neutral_200 = Color.fromRGBO(241, 244, 249, 1);
const neutral_100 = Color.fromRGBO(246, 248, 252, 1);
const green_600 = Color.fromRGBO(85, 155, 16, 1);
const green_500 = Color.fromRGBO(119, 175, 64, 1);
const green_400 = Color.fromRGBO(153, 195, 112, 1);
const green_300 = Color.fromRGBO(187, 215, 159, 1);
const green_200 = Color.fromRGBO(221, 235, 207, 1);
const green_100 = Color.fromRGBO(238, 250, 226, 1);
const yellow_600 = Color.fromRGBO(245, 205, 63, 1);
const yellow_500 = Color.fromRGBO(245, 213, 99, 1);
const yellow_400 = Color.fromRGBO(245, 221, 136, 1);
const yellow_300 = Color.fromRGBO(245, 229, 172, 1);
const yellow_200 = Color.fromRGBO(245, 237, 209, 1);
const yellow_100 = Color.fromRGBO(255, 252, 238, 1);
const red_600 = Color.fromRGBO(229, 24, 14, 1);
const red_500 = Color.fromRGBO(234, 70, 62, 1);
const red_400 = Color.fromRGBO(239, 116, 110, 1);
const red_300 = Color.fromRGBO(245, 163, 159, 1);
const red_200 = Color.fromRGBO(250, 209, 207, 1);
const red_100 = Color.fromRGBO(254, 243, 243, 1);
const primary_500 = Color.fromRGBO(51, 95, 149, 1);
const primary_400 = Color.fromRGBO(102, 135, 176, 1);
const primary_300 = Color.fromRGBO(153, 175, 202, 1);
const primary_200 = Color.fromRGBO(204, 215, 229, 1);
const primary_100 = Color.fromRGBO(242, 245, 248, 1);
const secondaryconst_500 = Color.fromRGBO(248, 158, 91, 1);
const secondary_400 = Color.fromRGBO(250, 182, 132, 1);
const secondary_300 = Color.fromRGBO(251, 207, 173, 1);
const secondary_200 = Color.fromRGBO(253, 231, 214, 1);
const secondary_100 = Color.fromRGBO(255, 249, 245, 1);
const gray_600 = Color.fromRGBO(160, 160, 160, 1);
const gray_500 = Color.fromRGBO(217, 217, 217, 1);
const gray_400 = Color.fromRGBO(240, 240, 240, 1);
const gray_300 = Color.fromRGBO(242, 242, 242, 1);
const gray_200 = Color.fromRGBO(246, 246, 246, 1);
const grayconst_100 = Color.fromRGBO(249, 249, 249, 1);
const blue_600 = Color.fromRGBO(25, 113, 255, 1);
const blue_500 = Color.fromRGBO(71, 141, 255, 1);
const blue_400 = Color.fromRGBO(117, 170, 255, 1);
const blue_300 = Color.fromRGBO(163, 198, 255, 1);
const blue_200 = Color.fromRGBO(209, 227, 255, 1);
const blue_100 = Color.fromRGBO(243, 248, 255, 1);
const blueprimary = Color.fromRGBO(242, 245, 248, 1);
const colorShadown = Color.fromRGBO(0, 0, 0, 0.06);

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}
